package controllers

import ( 
	"fmt"
	"time"
	"new_sosmed/models"
	profiles "new_sosmed/models/profiles"
	common "new_sosmed/models/common"
	adminWeb "new_sosmed/models/admin_web"
	products "new_sosmed/models/product"
	qrcode "github.com/skip2/go-qrcode"
	"github.com/astaxie/beego"
)

type AdminWeb struct {
	BaseController
}

func (c *AdminWeb) LoginRockStar() {
	form := adminWeb.RockstarLoginForm{}
	if err := c.ParseForm(&form); err != nil {
		beego.Debug("ParseLoginForm : ", err)
		c.Data["json"] = common.NewErrorInfo(ErrInputData)
		c.ServeJSON()
		return
	}

	beego.Debug("ParseLoginForm : ", &form)

	adminRockstar := adminWeb.AdminWebStruct{}
	if code, err := adminRockstar.FindAdminWebByID(form.Email); err != nil {
		beego.Error("FindAdminWebByID : ", err) 
		if code == common.ErrNotFound {
			c.Data["json"] = common.NewErrorInfo(ErrNoUser)
		} else {
			c.Data["json"] = common.NewErrorInfo(ErrDatabase)
		}
		c.ServeJSON()
		return
	}
	beego.Debug("UserInfo : ", &adminRockstar)

	if ok, err := adminRockstar.CheckPass(form.Password); err != nil {
		beego.Error("CheckWebAdminPass : ", err)
		c.Data["json"] = common.NewErrorInfo(ErrSystem)
		c.ServeJSON()
		return
	} else if !ok {
		c.Data["json"] = common.NewErrorInfo(ErrPass)
		c.ServeJSON()
		return
	}
	adminRockstar.ClearPass()

	c.SetSession("user_id", form.Email)

	c.Data["json"] = &adminWeb.RockstarLoginInfo{Code: 0, WebAdminInfo: &adminRockstar}
	c.ServeJSON()
}

func (c *AdminWeb) CreateAdminUser() {
	form_profile := profiles.CreateProfileForm{}
	if err := c.ParseForm(&form_profile); err != nil {
		beego.Debug("ParseCreateProfileForm : ", err)
		c.Data["json"] = common.NewErrorInfo(ErrInputData)
		c.ServeJSON()
		return
	}

	profile, err := profiles.NewProfile(&form_profile)
	if err != nil {
		beego.Error("CreateProfile : ", err)
		c.Data["json"] = common.NewErrorInfo(ErrSystem)
		c.ServeJSON()
		return
	}

	if code, err := profile.Insert(); err != nil {
		beego.Error("InsertProfile : ", err)
		if code == common.ErrDupRows {
			c.Data["json"] = common.NewErrorInfo(ErrDupUser)
		} else {
			c.Data["json"] = common.NewErrorInfo(ErrDatabase)
		}
		c.ServeJSON()
		return
	}

	form := adminWeb.CreateAdminUserForm{}
	if err := c.ParseForm(&form); err != nil {
		beego.Debug("ParseCreateAdminUserForm : ", err)
		c.Data["json"] = common.NewErrorInfo(ErrInputData)
		c.ServeJSON()
		return
	}
	beego.Debug("ParseCreateAdminUserForm : ", &form)

	regDate := time.Now()
	Adminuser, err := adminWeb.NewAdminUser(&form, regDate)
	if err != nil {
		beego.Error("NewAdminUser:", err)
		c.Data["json"] = common.NewErrorInfo(ErrSystem)
		c.ServeJSON()
		return
	}
	beego.Debug("NewAdminUser:", Adminuser)

	if code, err := Adminuser.InsertAdminUser(); err != nil {
		beego.Error("InsertUser:", err)
		if code == common.ErrDupRows {
			c.Data["json"] = common.NewErrorInfo(ErrDupUser)
		} else {
			c.Data["json"] = common.NewErrorInfo(ErrDatabase)
		}
		c.ServeJSON()
		return
	}

	go models.IncTotalUserCount(regDate)

	// Generate QrCode By Email
	var pathQrCode = "./static/qrcode/" // Path of image qrCode

	q, err := qrcode.New(form.Email, qrcode.Medium) // Eksekusi QrCode, format Medium
	if err != nil {
		panic(err)
	}

	q.DisableBorder = true // Set Border

	err = q.WriteFile(256, pathQrCode+"ADMIN"+form.Email+".png") // Create File PNG.
	if err != nil {
		panic(err)
	}

	c.Data["json"] = common.NewNormalInfo("Succes")
	c.ServeJSON()
}

func (c *AdminWeb) DeleteAdminUser() {
	form := adminWeb.DeleteAdminUserByIdForm{}
	if err := c.ParseForm(&form); err != nil {
		beego.Debug("DeleteAdminUserByIdForm : ", err)
		c.Data["json"] = common.NewErrorInfo(ErrInputData)
		c.ServeJSON()
		return
	}
	beego.Debug("ParseFormDeleteTimelineById : ", &form)

	delete_admin_user_by_id := adminWeb.AdminWebStruct{}

	if getAdminUser, err := delete_admin_user_by_id.FindAdminWebByID(form.Id); err != nil {
		beego.Error("GetAdminUserById : ", err) 
		if getAdminUser == common.ErrNotFound {
			c.Data["json"] = common.NewErrorInfo(ErrDupUser)
		} else {
			c.Data["json"] = common.NewErrorInfo(ErrDatabase)
		}
		c.ServeJSON()
		return
	}

	code, err := delete_admin_user_by_id.DeleteAdminUser(form.Id)

	if err != nil {
		beego.Error("DeleteAdminUserById : ", err) 
		if code == common.ErrNotFound {
			c.Data["json"] = common.NewErrorInfo(ErrDupUser)
		} else {
			c.Data["json"] = common.NewErrorInfo(ErrDatabase)
		}
		c.ServeJSON()
		return
	}

	delete_profile_user_id := profiles.Profile{} 
	delete_profile, err := delete_profile_user_id.DeleteAccount(form.Id)

	if err != nil {
		beego.Error("DeleteTimelineById : ", err) 
		if delete_profile == common.ErrNotFound {
			c.Data["json"] = common.NewErrorInfo(ErrDupUser)
		} else {
			c.Data["json"] = common.NewErrorInfo(ErrDatabase)
		}
		c.ServeJSON()
		return
	}

	c.Data["json"] = common.NewNormalInfo("Success")
	c.ServeJSON()
}

func (c *AdminWeb) ActivationAdminUserWithCode() {
	form := adminWeb.ActivationUserAdminWithCodeForm{}

	if err := c.ParseForm(&form); err != nil {
		beego.Debug("ParseActivateAdminUser : ", err)
		c.Data["json"] = common.NewErrorInfo(ErrInputData)
		c.ServeJSON()
		return
	}
	beego.Debug("ParseActivateAdminUser : ", &form)

	code, err := adminWeb.ActivationAdminUserWithCode(form.Email, form.Activation_code)
	if err != nil {
		beego.Error("ActivateAdminUser : ", err)

		if code == common.ErrNotFound {
			c.Data["json"] = common.NewErrorInfo(ErrNoUser)
		} else if code == common.ErrDatabase {
			c.Data["json"] = common.NewErrorInfo(ErrDatabase)
		} else {
			c.Data["json"] = common.NewErrorInfo(ErrSystem)
		}
		c.ServeJSON()
		return
	} 
	c.Data["json"] = common.NewNormalInfo("Success")
	c.ServeJSON()
}

func (c *AdminWeb) ActivateAdminUser() {
	form := adminWeb.ActivateUserAdminForm{}

	if err := c.ParseForm(&form); err != nil {
		beego.Debug("ParseActivateAdminUser : ", err)
		c.Data["json"] = common.NewErrorInfo(ErrInputData)
		c.ServeJSON()
		return
	}
	beego.Debug("ParseActivateAdminUser : ", &form)

	code, err := adminWeb.ActivateUser(form.Email)
	if err != nil {
		beego.Error("ActivateAdminUser : ", err)

		if code == common.ErrNotFound {
			c.Data["json"] = common.NewErrorInfo(ErrNoUser)
		} else if code == common.ErrDatabase {
			c.Data["json"] = common.NewErrorInfo(ErrDatabase)
		} else {
			c.Data["json"] = common.NewErrorInfo(ErrSystem)
		}
		c.ServeJSON()
		return
	} 
	c.Data["json"] = common.NewNormalInfo("Success")
	c.ServeJSON()
}

func (c *AdminWeb) DeactiveAdminUser() {
	form := adminWeb.DeactiveUserAdminForm{}

	if err := c.ParseForm(&form); err != nil {
		beego.Debug("ParseDeactivateAdminUser : ", err)
		c.Data["json"] = common.NewErrorInfo(ErrInputData)
		c.ServeJSON()
		return
	}
	beego.Debug("ParseDeactivateAdminUser : ", &form)

	code, err := adminWeb.DeactivationAdminUser(form.Email)
	if err != nil {
		beego.Error("ParseDeactivateAdminUser : ", err)

		if code == common.ErrNotFound {
			c.Data["json"] = common.NewErrorInfo(ErrNoUser)
		} else if code == common.ErrDatabase {
			c.Data["json"] = common.NewErrorInfo(ErrDatabase)
		} else {
			c.Data["json"] = common.NewErrorInfo(ErrSystem)
		}
		c.ServeJSON()
		return
	} 
	c.Data["json"] = common.NewNormalInfo("Success")
	c.ServeJSON()
}

// func (c *AdminWeb) UpdateAdminUser() {
// 	form := adminWeb.UpdateEducationHighSchoolForm{}
// 	if err := c.ParseForm(&form); err != nil {
// 		beego.Debug("ParseUpdateEducationHighSchool : ", err)
// 		c.Data["json"] = common.NewErrorInfo(ErrInputData)
// 		c.ServeJSON()
// 		return
// 	}
// 	beego.Debug("ParseUpdateEducationHighSchool : ", &form)
// }

func (c *AdminWeb) AddProduct() {
	form := products.AddProductForm{}

	if err := c.ParseForm(&form); err != nil {
		beego.Debug("ParseDeactivateAdminUser : ", err)
		c.Data["json"] = common.NewErrorInfo(ErrInputData)
		c.ServeJSON()
		return
	}
	beego.Debug("ParseDeactivateAdminUser : ", &form)

	// file 
	f, h, _ := c.GetFile("image_product")
	path 	:= "./static/product_file/" + h.Filename // inisialiasi path

	defer f.Close()
	c.SaveToFile("file", path)
	product, err := products.NewProductPost(&form, h.Filename)
	if err != nil {
		beego.Error("NewTimeline:", err)
		c.Data["json"] = common.NewErrorInfo(ErrSystem)
		c.ServeJSON()
		return
	}
	beego.Debug("NewTimeline:", product)

	if code, err := product.InsertProduct(); err != nil {
		beego.Error("InsertUser:", err)
		if code == common.ErrDupRows {
			c.Data["json"] = common.NewErrorInfo(ErrDupUser)
		} else {
			c.Data["json"] = common.NewErrorInfo(ErrDatabase)
		}
		c.ServeJSON()
		return
	}
	fmt.Println(path)
	c.Data["json"] = common.NewNormalInfo("Succes")
	c.ServeJSON()
}

func (c *AdminWeb) EditProduct() {
	form := products.EditProductForm{}
	if err := c.ParseForm(&form); err != nil {
		beego.Debug("ParseEditProduct : ", err)
		c.Data["json"] = common.NewErrorInfo(ErrInputData)
		c.ServeJSON()
		return
	}
	beego.Debug("ParseEditProduct :", &form)
}

func (c *AdminWeb) DeleteProduct() {
	form := products.DeleteProductForm{}
	if err := c.ParseForm(&form); err != nil {
		beego.Debug("ParseDeleteProduct : ", err)
		c.Data["json"] = common.NewErrorInfo(ErrInputData)
		c.ServeJSON()
		return
	}
}