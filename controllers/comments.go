package controllers

import (
	common "new_sosmed/models/common"
	// timelines "new_sosmed/models/timelines"
	comments "new_sosmed/models/comments"
	// profiles "new_sosmed/models/profiles"
	"github.com/astaxie/beego"
)

type CommentsControllers struct {
	BaseController
}

func (c *CommentsControllers) PostComment() {
	form := comments.CommentsPostForm{}
	if err := c.ParseForm(&form); err != nil {
		beego.Debug("ParseCreateProfileForm : ", err)
		c.Data["json"] = common.NewErrorInfo(ErrInputData)
		c.ServeJSON()
		return
	}

	beego.Debug("ParseCommentPostForm:", &form)

	// comment, err := comments.NewCommentsPost(&form)
	// if err != nil {
	// 	beego.Error("NewWorkPlace : ", err)
	// 	c.Data["json"] = common.NewErrorInfo(ErrSystem)
	// 	c.ServeJSON()
	// 	return
	// }
	// beego.Debug("NewWorkPlace : ", &form)

	// if code, err := comment.InsertComment(); err != nil {
	// 	beego.Error("InsertWorkplace : ", err)
	// 	if code == common.ErrDupRows {
	// 		c.Data["json"] = common.NewErrorInfo(ErrDupUser)
	// 	} else {
	// 		c.Data["json"] = common.NewErrorInfo(ErrDatabase)
	// 	}
	// 	c.ServeJSON()
	// 	return
	// }
	// c.Data["json"] = company.CompanyInfo{Code: 200, Message:"Success"}
	// c.ServeJSON()
}