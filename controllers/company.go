package controllers

import (
	"time"
	common "new_sosmed/models/common"
	company "new_sosmed/models/company"
	task_board "new_sosmed/models/task_board"
	division "new_sosmed/models/division_company"
	allowance "new_sosmed/models/allowance_company"
	task_board_list "new_sosmed/models/task_board_list"
	"github.com/astaxie/beego"
	// "gopkg.in/mgo.v2/bson"
)

type CompanyController struct {
	BaseController
}

func (c *CompanyController) CreateCompany() {
	form := company.CreateCompanyPostForm{}
	if err := c.ParseForm(&form); err != nil {
		beego.Debug("ParseCreateCompanyPostForm:", err)
		c.Data["json"] = common.NewErrorInfo(ErrInputData)
		c.ServeJSON()
		return
	}
	beego.Debug("ParseCreateCompanyPostForm:", &form)

	create_company, err := company.NewCompanyPost(&form)
	if err != nil {
		beego.Error("NewCompanyPost : ", err)
		c.Data["json"] = common.NewErrorInfo(ErrSystem)
		c.ServeJSON()
		return
	}
	beego.Debug("NewCompanyPost : ", &form)

	if code, err := create_company.InsertCompany(); err != nil {
		beego.Error("InsertCompany : ", err)
		if code == common.ErrDupRows {
			c.Data["json"] = common.NewErrorInfo(ErrDupUser)
		} else {
			c.Data["json"] = common.NewErrorInfo(ErrDatabase)
		}
		c.ServeJSON()
		return
	}
	c.Data["json"] = company.CompanyInfo{Code: 200, Message:"Success"}
	c.ServeJSON()
}

func (c *CompanyController) GetCompanyById() {
	form := company.GetCompanyByIdForm{}
	if err := c.ParseForm(&form); err != nil {
		beego.Debug("ParseGetCompanyById : ", err)
		c.Data["json"] = common.NewErrorInfo(ErrInputData)
		c.ServeJSON()
		return
	}
	beego.Debug("ParseGetCompanyById : ", &form)

	company_ := company.Company{}
	if code, err := company_.GetCompanyById(form.Id); err != nil {
		beego.Error("ParseGetCompanyById : ", err) 
		if code == common.ErrNotFound {
			c.Data["json"] = common.NewErrorInfo(ErrNoUser)
		} else {
			c.Data["json"] = common.NewErrorInfo(ErrDatabase)
		}
		c.ServeJSON()
		return
	}

	c.Data["json"] = company.CompanySingleInfo{Code: 200, Message:"Success", Data: &company_}
	c.ServeJSON()
}

func (c *CompanyController) Update_company() {
	form := company.UpdateSettingCompanyForm{}
	if err := c.ParseForm(&form); err != nil {
		beego.Debug("ParseUpdateCompanyForm : ", &form)
		c.Data["json"] = common.NewErrorInfo(ErrInputData)
		c.ServeJSON()
		return
	}

	companies := company.Company{}
	dateUpdated := time.Now().UnixNano() / int64(time.Millisecond) // Convert time to milisec
	code, err := companies.UpdateCompany(form.Id, form.NameCompany, form.FormatDate, form.FormatTime, form.ReportRange, form.DeductEvery, dateUpdated)
	if err != nil {
		beego.Error("UpdateCompany : ", err) 

		if code == common.ErrNotFound {
			c.Data["json"] = common.NewErrorInfo(ErrNoUser)
		} else if code == common.ErrDatabase {
			c.Data["json"] = common.NewErrorInfo(ErrDatabase)
		} else {
			c.Data["json"] = common.NewErrorInfo(ErrSystem) 
		}
		c.ServeJSON()
		return
	}
	beego.Debug(&form)
	c.Data["json"] = company.CompanyInfo{Code: 200, Message: "Success", Data: &companies}
	c.ServeJSON()
}

func (c *CompanyController) Post_division() {
	form := division.CreateDivisinPostForm{}
	if err := c.ParseForm(&form); err != nil {
		beego.Debug("ParsePostDivisionForm : ", &form)
		c.Data["json"] = common.NewErrorInfo(ErrInputData)
		c.ServeJSON()
		return
	}

	division_company, err := division.NewDivisionPost(&form)
	if err != nil {
		beego.Error("NewCompanyPost : ", err)
		c.Data["json"] = common.NewErrorInfo(ErrSystem)
		c.ServeJSON()
		return
	}
	beego.Debug("NewCompanyPost : ", &form)

	if code, err := division_company.InsertDevision(); err != nil {
		beego.Error("InsertDevision : ", err)
		if code == common.ErrDupRows {
			c.Data["json"] = common.NewErrorInfo(ErrDupUser)
		} else {
			c.Data["json"] = common.NewErrorInfo(ErrDatabase)
		}
		c.ServeJSON()
		return
	}
	c.Data["json"] = division.DivisionInfo{Code: 200, Message:"Success"}
	c.ServeJSON()
}

func (c *CompanyController) Get_devision() {
	form := division.GetDevisionByIdForm{}
	if err := c.ParseForm(&form); err != nil {
		beego.Debug("ParseGetDivisionForm : ", &form)
		c.Data["json"] = common.NewErrorInfo(ErrInputData)
		c.ServeJSON()
		return
	}

	division_company := division.Devision{}
	if code, err := division_company.GetDevisionById(form.Id); err != nil {
		beego.Error("ParseGetCompanyById : ", err) 
		if code == common.ErrNotFound {
			c.Data["json"] = common.NewErrorInfo(ErrNoUser)
		} else {
			c.Data["json"] = common.NewErrorInfo(ErrDatabase)
		}
		c.ServeJSON()
		return
	}

	c.Data["json"] = division.DivisionInfo{Code: 200, Message:"Success", Data: &division_company}
	c.ServeJSON()
}

func (c *CompanyController) Get_devision_by_company_id() {
	form := division.GetDevisionByCompanyIdForm{}
	if err := c.ParseForm(&form); err != nil {
		beego.Debug("ParseGetDivisionForm : ", &form)
		c.Data["json"] = common.NewErrorInfo(ErrInputData)
		c.ServeJSON()
		return
	}

	division_company := division.Devision{}
	code, _ := division_company.GetDevisionByCompanyId(form.CompanyId)

	c.Data["json"] = division.DivisionByCompanyIDInfo{Code: 200, Message:"Success", Data: &code}
	c.ServeJSON()
}

func (c *CompanyController) Post_allowance() {
	form := allowance.CreateAllowancePostForm{}
	if err := c.ParseForm(&form); err != nil {
		beego.Debug("ParsePostAllowanceForm : ", &form)
		c.Data["json"] = common.NewErrorInfo(ErrInputData)
		c.ServeJSON()
		return
	}

	allowance_company, err := allowance.NewAllowancePost(&form)
	if err != nil {
		beego.Error("NewAllowancePost : ", err)
		c.Data["json"] = common.NewErrorInfo(ErrSystem)
		c.ServeJSON()
		return
	}
	beego.Debug("NewAllowancePost : ", &form)

	if code, err := allowance_company.InsertAllowance(); err != nil {
		beego.Error("InsertAllowance : ", err)
		if code == common.ErrDupRows {
			c.Data["json"] = common.NewErrorInfo(ErrDupUser)
		} else {
			c.Data["json"] = common.NewErrorInfo(ErrDatabase)
		}
		c.ServeJSON()
		return
	}
	c.Data["json"] = allowance.AllowanceInfo{Code: 200, Message:"Success"}
	c.ServeJSON()
}

func (c *CompanyController) Get_allowance() {
	form := allowance.GetAllowanceByIdForm{}
	if err := c.ParseForm(&form); err != nil {
		beego.Debug("ParseGetAllowanceform : ", &form)
		c.Data["json"] = common.NewErrorInfo(ErrInputData)
		c.ServeJSON()
		return
	}

	allowance_company := allowance.Allowance{}
	if code, err := allowance_company.GetAllowanceById(form.Id); err != nil {
		beego.Error("ParseGetCompanyById : ", err) 
		if code == common.ErrNotFound {
			c.Data["json"] = common.NewErrorInfo(ErrNoUser)
		} else {
			c.Data["json"] = common.NewErrorInfo(ErrDatabase)
		}
		c.ServeJSON()
		return
	}

	c.Data["json"] = allowance.AllowanceInfo{Code: 200, Message:"Success", Data: &allowance_company}
	c.ServeJSON()
}

func (c *CompanyController) Get_allowance_by_company_id() {
	form := allowance.GetAllowanceByCompanyId{}
	if err := c.ParseForm(&form); err != nil {
		beego.Debug("ParseGetAllowanceByCompanyIdForm : ", &form)
		c.Data["json"] = common.NewErrorInfo(ErrInputData)
		c.ServeJSON()
		return
	}

	allowance_company := allowance.Allowance{}
	code, _ := allowance_company.GetAllowanceByCompanyId(form.CompanyId)

	c.Data["json"] = allowance.AllowanceByCompanyIDInfo{Code: 200, Message:"Success", Data: &code}
	c.ServeJSON()
}

func (c *CompanyController) Update_allowance() {
	form := allowance.UpdateAllowanceCompanyForm{}
	if err := c.ParseForm(&form); err != nil {
		beego.Debug("ParseUpdateAllowanceform : ", &form)
		c.Data["json"] = common.NewErrorInfo(ErrInputData)
		c.ServeJSON()
		return
	}
}

func (c *CompanyController) Post_job() {
	form := task_board.TaskBoardPostForm{}
	if err := c.ParseForm(&form); err != nil {
		beego.Debug("ParsePostJob : ", &form)
		c.Data["json"] = common.NewErrorInfo(ErrInputData)
		c.ServeJSON()
		return
	}

	task, err := task_board.NewTaskBoardPost(&form)
	if err != nil {
		beego.Error("NewTaskBoardPost : ", err)
		c.Data["json"] = common.NewErrorInfo(ErrSystem)
		c.ServeJSON()
		return
	}
	beego.Debug("NewTaskBoardPost : ", &form)

	if code, err := task.InsertTaskBoard(); err != nil {
		beego.Error("InsertTaskBoard : ", err)
		if code == common.ErrDupRows {
			c.Data["json"] = common.NewErrorInfo(ErrDupUser)
		} else {
			c.Data["json"] = common.NewErrorInfo(ErrDatabase)
		}
		c.ServeJSON()
		return
	}
	c.Data["json"] = task_board.TaskBoardInfo{Code: 200, Message:"Success"}
	c.ServeJSON()
}

func (c *CompanyController) Get_job_by_id() {
	form := task_board.TaskBoardGetForm{}
	if err := c.ParseForm(&form); err != nil {
		beego.Debug("ParseGetJob : ", &form)
		c.Data["json"] = common.NewErrorInfo(ErrInputData)
		c.ServeJSON()
		return
	}

	task_job := task_board.TaskBoard{}
	if code, err := task_job.FindTaskBoardByID(form.Id); err != nil {
		beego.Error("ParseGetCompanyById : ", err) 
		if code == common.ErrNotFound {
			c.Data["json"] = common.NewErrorInfo(ErrNoUser)
		} else {
			c.Data["json"] = common.NewErrorInfo(ErrDatabase)
		}
		c.ServeJSON()
		return
	}

	c.Data["json"] = task_board.TaskBoardInfo{Code: 200, Message:"Success", Data: &task_job}
	c.ServeJSON()
}

func (c *CompanyController) Get_job_by_company_id() {
	form := task_board.TaskBoardGetByCompanyIdForm{}
	if err := c.ParseForm(&form); err != nil {
		beego.Debug("ParseGetJobByCompanyId : ", &form)
		c.Data["json"] = common.NewErrorInfo(ErrInputData)
		c.ServeJSON()
		return
	}

	task_job := task_board.TaskBoard{}
	code, _ := task_job.FindByCompanyId(form.Company_id)

	c.Data["json"] = task_board.TaskBoardByCompanyIDInfo{Code: 200, Message:"Success", Data: &code}
	c.ServeJSON()
}

func (c *CompanyController) Post_job_module() {
	form := task_board_list.TaskBoardListPostForm{}
	if err := c.ParseForm(&form); err != nil {
		beego.Debug("ParsePostJobModule : ", &form)
		c.Data["json"] = common.NewErrorInfo(ErrInputData)
		c.ServeJSON()
		return
	}

	task_list, err := task_board_list.NewTaskBoardListPost(&form)
	if err != nil {
		beego.Error("NewTaskBoardListPost : ", err)
		c.Data["json"] = common.NewErrorInfo(ErrSystem)
		c.ServeJSON()
		return
	}
	beego.Debug("NewTaskBoardPost : ", &form)

	if code, err := task_list.InsertTaskBoardList(); err != nil {
		beego.Error("InsertTaskBoard : ", err)
		if code == common.ErrDupRows {
			c.Data["json"] = common.NewErrorInfo(ErrDupUser)
		} else {
			c.Data["json"] = common.NewErrorInfo(ErrDatabase)
		}
		c.ServeJSON()
		return
	}
	c.Data["json"] = task_board_list.TaskBoardListInfo{Code: 200, Message:"Success"}
	c.ServeJSON()
}

func (c *CompanyController) Get_job_module_by_id() {
	form := task_board_list.TaskBoardListGetForm{}
	if err := c.ParseForm(&form); err != nil {
		beego.Debug("ParseGetJobModule : ", &form)
		c.Data["json"] = common.NewErrorInfo(ErrInputData)
		c.ServeJSON()
		return
	}

	task_job_list := task_board_list.TaskBoardList{}
	if code, err := task_job_list.FindTaskBoardListByID(form.Id); err != nil {
		beego.Error("ParseGetJobModuleById : ", err) 
		if code == common.ErrNotFound {
			c.Data["json"] = common.NewErrorInfo(ErrNoUser)
		} else {
			c.Data["json"] = common.NewErrorInfo(ErrDatabase)
		}
		c.ServeJSON()
		return
	}

	c.Data["json"] = task_board_list.TaskBoardListInfo{Code: 200, Message:"Success", Data: &task_job_list}
	c.ServeJSON()
}

func (c *CompanyController) Update_job_module() {
	form := task_board_list.TaskBoardListGetForm{}
	if err := c.ParseForm(&form); err != nil {
		beego.Debug("ParseGetJobModule : ", &form)
		c.Data["json"] = common.NewErrorInfo(ErrInputData)
		c.ServeJSON()
		return
	}
}