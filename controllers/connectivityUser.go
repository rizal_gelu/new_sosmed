package controllers

import (
	"fmt"
	// "time"
	// "os"
	common "new_sosmed/models/common"
	connectivityUser "new_sosmed/models/connectivityuser"
	// timelines "new_sosmed/models/timelines"
	// comments "new_sosmed/models/comments"
	profiles "new_sosmed/models/profiles"
	"github.com/astaxie/beego"
	// "gopkg.in/mgo.v2/bson"
)

type ConnectivityUserController struct {
	BaseController
}

func (c *ConnectivityUserController) FollowPost() {
	form := connectivityUser.FollowsPostForm{} // Inisialisasi Form
	if err := c.ParseForm(&form); err != nil { // Checking form 
		beego.Debug("ParseFollowPostForm : ", err)
		c.Data["json"] = common.NewErrorInfo(ErrInputData) // Print with json
		c.ServeJSON() // json
		return // mengembalikan ke rel sebelumnya
	}
	beego.Debug("ParseFollowPostForm : ", &form) // Print Form

	follow, err := connectivityUser.NewFollowPost(&form) // Validasi 
	if err != nil { 
		beego.Error("NewFollow : ", err)
		c.Data["json"] = common.NewErrorInfo(ErrSystem)
		c.ServeJSON()
		return
	}
	beego.Debug("NewFollow : ", follow)

	checkingStatus, err := follow.CheckingStatusFollow(form.EmailForm, form.EmailTo) // Checking 

	if checkingStatus < 0 { // 
		if code, err := follow.Insert(); err != nil { 
			beego.Error("InsertFollow : ", err)
			if code == common.ErrDupRows {
				c.Data["json"] = common.NewErrorInfo(ErrDupUser)
			} else {
				c.Data["json"] = common.NewErrorInfo(ErrDatabase)
			}

			c.ServeJSON()
			return
		}

		c.Data["json"] = &connectivityUser.ResponseFollow{Code:200, Message: "Success"}
		c.ServeJSON()
	} else {
		c.Data["json"] = &connectivityUser.ResponseFollow{Code:404, Message: "Cant Double Rows"}
		c.ServeJSON()
	}
}

func (c *ConnectivityUserController) UnFollowPost() {
	form := connectivityUser.UnfollowPostForm{}
	if err := c.ParseForm(&form); err != nil {
		beego.Debug("ParseUnFollowForm : ", err)
		c.Data["json"] = common.NewErrorInfo(ErrInputData)
		c.ServeJSON()
		return
	}
	beego.Debug("ParseUnFollowForm : ", &form)

	connectivityuser := connectivityUser.Follows{}

	unfollow, err := connectivityuser.UnFollow(form.EmailForm, form.EmailTo)
	if err != nil {
		beego.Error("UnFollow : ", err)
		if unfollow == common.ErrDupRows {
			c.Data["json"] = common.NewErrorInfo(ErrDupUser)
		} else {
			c.Data["json"] = common.NewErrorInfo(ErrDatabase)
		}
		c.ServeJSON()
		return
	}

	c.Data["json"] = &connectivityUser.ResponseUnFollow{Code: 200, Message: "Success"}
	c.ServeJSON()
}

func (c *ConnectivityUserController) ListFollowing() {
	form := connectivityUser.CountFollowingPostForm{}
	if err := c.ParseForm(&form); err != nil {
		beego.Debug("ParseCountFollowingForm : ", err)
		c.Data["json"] = common.NewErrorInfo(ErrInputData)
		c.ServeJSON()
		return
	}
	beego.Debug("ParseCountFollowingForm : ", &form)

	getFollowing := connectivityUser.Follows{}
	code, err := getFollowing.GetListFollowing(form.EmailForm)
	if err != nil {
		fmt.Println("Error GetListFollowing", code)
	}

	printRes := make([]map[string]interface{}, 0)

	for _, v := range code {

		profiles_account := profiles.Profile{}
		if profile, err := profiles_account.FindByEmail(v.EmailTo); err != nil {
			beego.Error("FindByEmailTo : ", err)
			if profile == common.ErrNotFound {
				c.Data["json"] = common.NewErrorInfo(ErrNoUser)
			} else {
				c.Data["json"] = common.NewErrorInfo(ErrDatabase)
			}
			c.ServeJSON()
			return
		}

		res := make(map[string]interface{})
		res["emailform"] 		= v.EmailFrom
		res["emailto"] 			= v.EmailTo
		res["first_name_to"]	= profiles_account.First_name
		res["last_name_to"]		= profiles_account.Last_name
		res["avatar_to"]		= profiles_account.Avatar
		res["status"]			= v.Status
		res["date_created"]		= v.DateCreated

		printRes = append(printRes, res) 
	}

	c.Data["json"] = &connectivityUser.GetListFollowingResponse{Code: 200, Message: "Success", Data: printRes}
	c.ServeJSON()
}

func (c *ConnectivityUserController) ListFollowers() {
	form := connectivityUser.ListFollowersForm{}
	if err := c.ParseForm(&form); err != nil {
		beego.Debug("ParseListFollowers : ", err) 
		c.Data["json"] = common.NewErrorInfo(ErrInputData)
		c.ServeJSON()
		return
	}
	beego.Debug("ParseListFollowers : ", &form)

	getFollowers := connectivityUser.Follows{}
	code, err := getFollowers.GetListFollowers(form.EmailTo)
	if err != nil {
		fmt.Println("Error GetListFollowers", code)
	}

	fmt.Println(form.EmailTo)

	printRes := make([]map[string]interface{}, 0)
	for _, v := range code {
		profiles_account := profiles.Profile{}

		if profile, err := profiles_account.FindByEmail(v.EmailTo); err != nil {
			beego.Error("FindByEmailTo : ", err)
			if profile == common.ErrNotFound {
				c.Data["json"] = common.NewErrorInfo(ErrNoUser)
			} else {
				c.Data["json"] = common.NewErrorInfo(ErrDatabase)
			}
			c.ServeJSON()
			return
		}

		res := make(map[string]interface{})
		res["emailform"] 		= v.EmailFrom
		res["emailto"]			= v.EmailTo
		res["first_name_to"]	= profiles_account.First_name
		res["last_name_to"] 	= profiles_account.Last_name
		res["avatar_to"]		= profiles_account.Avatar
		res["status"]			= v.Status
		res["date_created"] 	= v.DateCreated

		printRes = append(printRes, res)
	}

	c.Data["json"] = &connectivityUser.GetListFollowersResponse{Code: 200, Message: "Success", Data : printRes}
	c.ServeJSON()
}

func (c *ConnectivityUserController) BlockedUser() {
	form := connectivityUser.BlockedUserForm{}
	if err := c.ParseForm(&form); err != nil {
		beego.Debug("ParseBlockedUser : ", err)
		c.Data["json"] = common.NewErrorInfo(ErrInputData)
		c.ServeJSON()
		return
	}
	beego.Debug("ParseBlockedUser : ", &form)

	blocked := connectivityUser.Follows{}
	if code, err := blocked.BlockedUser(); err != nil {
		beego.Error("Blocked : ", err)
		if code == common.ErrNotFound {
			c.Data["json"] = common.NewErrorInfo(ErrNoUser)
		} else {
			c.Data["json"] = common.NewErrorInfo(ErrDatabase)
		}
		c.ServeJSON()
		return
	}

	c.Data["json"] = &connectivityUser.ResponseBlockedUser{Code:200, Message: "Success"}
	c.ServeJSON()
}

func (c *ConnectivityUserController) UnBlockedUser() {
	form := connectivityUser.UnBlockedUserForm{}
	if err := c.ParseForm(&form); err != nil {
		beego.Debug("ParseUnBlockUser : ", err)
		c.Data["json"] = common.NewErrorInfo(ErrInputData)
		c.ServeJSON()
		return
	}
	beego.Debug("ParseUnBlockUser : ", &form)

	unblock := connectivityUser.Follows{}
	if code, err := unblock.UnBlockUser(form.EmailFrom, form.EmailTo); err != nil {
		beego.Error("UnBlocked : ", err)
		if code == common.ErrNotFound {
			c.Data["json"] = common.NewErrorInfo(ErrNoUser)
		} else {
			c.Data["json"] = common.NewErrorInfo(ErrDatabase)
		}
		c.ServeJSON()
		return
	}

	c.Data["json"] = &connectivityUser.ResponseUnBlockedUser{Code:200, Message: "Success"}
}
