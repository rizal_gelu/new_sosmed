package controllers

import (
	common "new_sosmed/models/common"
	order "new_sosmed/models/order"
	// timelines "new_sosmed/models/timelines"
	// comments "new_sosmed/models/comments"
	// profiles "new_sosmed/models/profiles"
	"github.com/astaxie/beego"
)

type OrderControllers struct {
	BaseController
}

func (c *OrderControllers) Ordering() {
	form := order.OrderPostForm{}
	if err := c.ParseForm(&form); err != nil {
		beego.Debug("ParseOrderForm : ", err)
		c.Data["json"] = common.NewErrorInfo(ErrInputData)
		c.ServeJSON()
		return
	}

	beego.Debug("ParseOrderForm:", &form)

	ordering, err := order.NewOrderPost(&form)
	if err != nil {
		beego.Error("NewOrderPost : ", err)
		c.Data["json"] = common.NewErrorInfo(ErrSystem)
		c.ServeJSON()
		return
	}
	beego.Debug("NewOrderPost : ", &form)

	if code, err := ordering.InsertOrder(); err != nil {
		beego.Error("InsertOrder : ", err)
		if code == common.ErrDupRows {
			c.Data["json"] = common.NewErrorInfo(ErrDupUser)
		} else {
			c.Data["json"] = common.NewErrorInfo(ErrDatabase)
		}
		c.ServeJSON()
		return
	}
	c.Data["json"] = order.OrderInfo{Code: 200, Message:"Success"}
	c.ServeJSON()
}

func (c *OrderControllers) Cart() {
	form := order.CartPostForm {}
	if err := c.ParseForm(&form); err != nil {
		beego.Debug("ParseCartForm : ", err)
		c.Data["json"] = common.NewErrorInfo(ErrInputData)
		c.ServeJSON()
		return
	}
	beego.Debug("ParseCartForm : ", &form)
}

