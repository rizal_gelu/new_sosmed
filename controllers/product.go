package controllers

import (
	"fmt"
	// common "new_sosmed/models/common"
	products "new_sosmed/models/product"
	// "github.com/astaxie/beego"
	// "gopkg.in/mgo.v2/bson"
)
type ProductController struct {
	BaseController
}

func (c *ProductController) GetAllProduct() {
	code, err := products.GetAllProductList() 
	if err != nil {
		fmt.Println("goblok controller")
	}

	fmt.Println(code)

	c.Data["json"] = products.GetAllProductForm{Code: 200, Message:"Success", Data: &code}
	c.ServeJSON()
}