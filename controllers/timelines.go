package controllers

import (
	"fmt"
	"time"
	"os"
	// libraries "new_sosmed/library" 
	common "new_sosmed/models/common"
	timelines "new_sosmed/models/timelines"
	comments "new_sosmed/models/comments"
	profiles "new_sosmed/models/profiles"
	likes "new_sosmed/models/like"
	"github.com/astaxie/beego"
	"gopkg.in/mgo.v2/bson"
)

type TimelineController struct {
	BaseController
}

func (c *TimelineController) PostTimeline() {
	form := timelines.TimelinePostForm{}
	if err := c.ParseForm(&form); err != nil {
		beego.Debug("ParseTimelinePostForm:", err)
		c.Data["json"] = common.NewErrorInfo(ErrInputData)
		c.ServeJSON()
		return
	}
	beego.Debug("ParseTimelinePostForm:", &form)

	// file 
	f, h, _ := c.GetFile("file")
	path 	:= "./static/timeline_file/" + h.Filename // inisialiasi path

	if h.Filename != "" {
		c.SaveToFile("file", path)
		timeline, err := timelines.NewTimelinePost(&form, h.Filename)
		if err != nil {
			beego.Error("NewTimeline:", err)
			c.Data["json"] = common.NewErrorInfo(ErrSystem)
			c.ServeJSON()
			return
		}
		beego.Debug("NewTimeline:", timeline)

		if code, err := timeline.Insert(); err != nil {
			beego.Error("InsertUser:", err)
			if code == common.ErrDupRows {
				c.Data["json"] = common.NewErrorInfo(ErrDupUser)
			} else {
				c.Data["json"] = common.NewErrorInfo(ErrDatabase)
			}
			c.ServeJSON()
			return
		}
		fmt.Println(path)
		c.Data["json"] = common.NewNormalInfo("Succes")
		c.ServeJSON()
	} else {
		c.Data["json"] = "fail"
		c.ServeJSON()
		return
	}

	defer f.Close()
}

func (c *TimelineController) GetAll() {
	code, err := timelines.GetAllTimelines() 

	if err != nil {
		fmt.Println("goblok controller")
	}

	response := make([]map[string]interface{}, 0) // Inisialisasi Response dengan panjang nilai 0

	for _, v := range code { // Looping code kombinasi dengan range. Yang di ambil hanya value (v) makanya ada tanda _

		profile_account := profiles.Profile{} // Inisialiasi cetakan Profile. 

		// Menurut saya golang selain clean code, golang juga bahasa yang konsisten dalam penggunaan variable
		// ataupun menurunkan sifat2 orang tua ke anak - anaknya. OOP (Object Orientation Programming)
		if profile, err := profile_account.FindByEmail(v.Email); err != nil { // profile_account berasal dari inisialisasi cetakan
			beego.Error("FindByEmail : ", err) 
			if profile == common.ErrNotFound {
				c.Data["json"] = common.NewErrorInfo(ErrNoUser)
			} else {
				c.Data["json"] = common.NewErrorInfo(ErrDatabase)
			}
			c.ServeJSON()
			return
		}

		convertObjectID := bson.ObjectId(v.Id).Hex()  // Convert Back ObjectId to String
		
		// Sesi Like dibawah
		likesStruct := likes.Like{}
		likes, err_like := likesStruct.GetLikeTLByIdTL(convertObjectID)

		if err_like != nil {
			fmt.Println("tolol like")
		}

		setLikes := make([]map[string]interface{}, 0)
		for _, like_v := range likes {
			convertTimeLike := time.Unix(0, like_v.DateCreated * int64(time.Millisecond))
			fmt.Println(convertTimeLike)

			like := make(map[string]interface{})
			like["id"] 			= bson.ObjectId(like_v.Id)
			like["id_timeline"]	= like_v.Id_timeline
			like["email"]		= like_v.Email
			like["date_create"] = like_v.DateCreated

			setLikes = append(setLikes, like)
		}

		// Sesi komentar dibawah
		commentsStuct := comments.Comments{}
		comments, err := commentsStuct.GetCommentByIdTimeline(convertObjectID)

		if err != nil {
			if err != nil {
				fmt.Println("goblok controller")
			}
		}

		setComments := make([]map[string]interface{}, 0) 
		for _, comments_v := range comments {
			// fmt.Println(comments_v.Id)
			convertTimeComment := time.Unix(0, comments_v.DateCreated * int64(time.Millisecond))
			fmt.Println(convertTimeComment)
			comment := make(map[string]interface{})
			comment["id"] 				= bson.ObjectId(comments_v.Id)
			comment["email_comment"]	= comments_v.Email
			comment["id_timeline"]		= comments_v.Id_timeline
			comment["date_create"] 		= comments_v.DateCreated
			comment["date_update"] 		= comments_v.DateUpdated

			setComments = append(setComments, comment)
		}

		res := make(map[string]interface{}) // res diinisialiasi lagi untuk cetakan output
		res["id"]				= bson.ObjectId(v.Id) 
		res["email"]			= v.Email
		res["description"]		= v.Description
		res["file"]				= v.File
		res["date_created"]		= v.DateCreated
		res["date_updated"]		= v.DateUpdated
		res["avatar"]			= profile_account.Avatar
		res["first_name"]		= profile_account.First_name
		res["last_name"]		= profile_account.Last_name
		res["comments"]			= setComments
		res["like"] 			= setLikes

		response = append(response, res) // var response yg sudah di inisialiasi sebelumnya, di append atau di gabung, atau diisi
		// value nya dengan res.
	}

	// response di parse ke json dan jadi output bagi API. 
	c.Data["json"] = &timelines.TimelineInfo{Code: 200, Message: "Succes", TimelineInfo: response}
	c.ServeJSON()
}

func (c *TimelineController) GetTimelineById() {
	form := timelines.TimelineGetSingleForm{}

	if err := c.ParseForm(&form); err != nil {
		beego.Debug("TimelineGetSingle : ", err)
		c.Data["json"] = common.NewErrorInfo(ErrInputData)
		c.ServeJSON()
		return
	}
	beego.Debug("ParseFormTimelineSingle : ", &form)

	timeline_single := timelines.Timeline{}

	if code, err := timeline_single.FindByID(form.Id); err != nil {
		beego.Error("GetTimelineById : ", err) 
		if code == common.ErrNotFound {
			c.Data["json"] = common.NewErrorInfo(ErrDupUser)
		} else {
			c.Data["json"] = common.NewErrorInfo(ErrDatabase)
		}
		c.ServeJSON()
		return
	}
	beego.Debug("TimelineSingleInfo : ", &timeline_single)

	profile_account := profiles.Profile{}

	if profile, err := profile_account.FindByEmail(timeline_single.Email); err != nil {
		beego.Error("FindByEmail : ", err) 
		if profile == common.ErrNotFound {
			c.Data["json"] = common.NewErrorInfo(ErrNoUser)
		} else {
			c.Data["json"] = common.NewErrorInfo(ErrDatabase)
		}
		c.ServeJSON()
		return
	}

	convertObjectID := bson.ObjectId(timeline_single.Id).Hex()  // Convert Back ObjectId to String
	commentsStuct := comments.Comments{}
	comments, err := commentsStuct.GetCommentByIdTimeline(convertObjectID)

	if err != nil {
		if err != nil {
			fmt.Println("goblok controller")
		}
	}

	setComments := make([]map[string]interface{}, 0) 
	for _, comments_v := range comments {
		// fmt.Println(comments_v.Id)
		// convertTimeCreateComment := time.Unix(0, comments_v.DateCreated * int64(time.Millisecond))
		// convertTimeUpdateComment := time.Unix(0, comments_v.DateUpdated * int64(time.Millisecond))
		// fmt.Println(convertTimeComment)
		comment := make(map[string]interface{})
		comment["id"] 				= bson.ObjectId(comments_v.Id)
		comment["email_comment"]	= comments_v.Email
		comment["id_timeline"]		= comments_v.Id_timeline
		comment["date_create"] 		= comments_v.DateCreated
		comment["date_update"] 		= comments_v.DateUpdated

		setComments = append(setComments, comment)
	}

	// convertTimeCreateTimeline := time.Unix(0, timeline_single.DateCreated * int64(time.Millisecond))
	// convertTimeUpdateTimeline := time.Unix(0, timeline_single.DateUpdated * int64(time.Millisecond))

	var response = map[string]interface{}{
		"id": 				bson.ObjectId(timeline_single.Id),
		"email":			timeline_single.Email,
		"description":		timeline_single.Description,
		"date_created": 	timeline_single.DateCreated,
		"date_updated": 	timeline_single.DateUpdated,
		"file": 			timeline_single.File,
		"first_name": 		profile_account.First_name,
		"last_name": 		profile_account.Last_name,
		"avatar":			profile_account.Avatar,
		"comments": 		setComments,
	}


	c.Data["json"] = timelines.GetTimelineByIdInfo{Code: 200, Message: "Succes", SingleTimeline: response}
	c.ServeJSON()
}

func (c *TimelineController) GetTimelineByEmail() {
	form := timelines.GetTimelineAllByEmailForm{}

	if err := c.ParseForm(&form); err != nil {
		beego.Debug("GetTimelineByEmail : ", err)
		c.Data["json"] = common.NewErrorInfo(ErrInputData)
		c.ServeJSON()
		return
	}
	beego.Debug("ParseFormTimelineSingle : ", &form) 

	timeline_get_all_by_email := timelines.Timeline{}
	code, err := timeline_get_all_by_email.FindByEmail(form.Email) // Code adalah variable yg menampung hasil query

	if err != nil {
		fmt.Println("salah tolol")
	}

	response := make([]map[string]interface{}, 0) // Inisialisasi Response dengan panjang nilai 0

	for _, v := range code { // Looping code kombinasi dengan range. Yang di ambil hanya value (v) makanya ada tanda _

		profile_account := profiles.Profile{} // Inisialiasi cetakan Profile. 

		// Menurut saya golang selain clean code, golang juga bahasa yang konsisten dalam penggunaan variable
		// ataupun menurunkan sifat2 orang tua ke anak - anaknya. OOP (Object Orientation Programming)
		if profile, err := profile_account.FindByEmail(v.Email); err != nil { // profile_account berasal dari inisialisasi cetakan
			beego.Error("FindByEmail : ", err) 
			if profile == common.ErrNotFound {
				c.Data["json"] = common.NewErrorInfo(ErrNoUser)
			} else {
				c.Data["json"] = common.NewErrorInfo(ErrDatabase)
			}
			c.ServeJSON()
			return
		}

		convertObjectID := bson.ObjectId(v.Id).Hex()  // Convert Back ObjectId to String
		commentsStuct := comments.Comments{}
		comments, err := commentsStuct.GetCommentByIdTimeline(convertObjectID)

		if err != nil {
			if err != nil {
				fmt.Println("goblok controller")
			}
		}

		setComments := make([]map[string]interface{}, 0) 
		for _, comments_v := range comments {
			// fmt.Println(comments_v.Id)
			convertTimeCreateComment := time.Unix(0, comments_v.DateCreated * int64(time.Millisecond))
			convertTimeUpdateComment := time.Unix(0, comments_v.DateUpdated * int64(time.Millisecond))
			// fmt.Println(convertTimeComment)
			comment := make(map[string]interface{})
			comment["id"] 				= bson.ObjectId(comments_v.Id)
			comment["email_comment"]	= comments_v.Email
			comment["id_timeline"]		= comments_v.Id_timeline
			comment["date_create"] 		= convertTimeCreateComment
			comment["date_update"] 		= convertTimeUpdateComment

			setComments = append(setComments, comment)
		}

		convertTimeCreateTimeline := time.Unix(0, v.DateCreated * int64(time.Millisecond))
		convertTimeUpdateTimeline := time.Unix(0, v.DateUpdated * int64(time.Millisecond))
		res := make(map[string]interface{}) // res diinisialiasi lagi untuk cetakan output
		res["id"]				= bson.ObjectId(v.Id) 
		res["email"]			= v.Email
		res["description"]		= v.Description
		res["file"]				= v.File
		res["date_created"]		= convertTimeCreateTimeline
		res["date_updated"]		= convertTimeUpdateTimeline
		res["avatar"]			= profile_account.Avatar
		res["first_name"]		= profile_account.First_name
		res["last_name"]		= profile_account.Last_name
		res["comments"]			= setComments

		response = append(response, res) // var response yg sudah di inisialiasi sebelumnya, di append atau di gabung, atau diisi
		// value nya dengan res.
	}

	// response di parse ke json dan jadi output bagi API. 
	c.Data["json"] = &timelines.TimelineGetAllByEmailInfo{Code: 200, Message: "Succes", GetAllTimelineByEmail: response}
	c.ServeJSON()
}

func (c *TimelineController) TimelineUpdate() {
	form := timelines.TimelineUpdateForm{} // Inisialiasi Form update. Nyambung ke Models. 

	if err := c.ParseForm(&form); err != nil { // Check Error form, dan kesesuaian data yang di minta form
		// Perhatikan ini sudah berbeda block
		// Jika terjadi error, pada form maka block ini akan dijalankan sistem.
		beego.Debug("UpdateTimelineCheckParse : ", err) // Print di CommadnPromt, jika terjadi err. Lihat Block code.
		c.Data["json"] = common.NewErrorInfo(ErrInputData) // set Print Error diresponse json (API).
		c.ServeJSON() // Eksekusi Print
		return // Mengambalikan kelajur (Keluar dari block code)
	}
	beego.Debug("ParseFormDeleteTimeline : ", &form) // Print di CommandPromt

	update_timeline := timelines.Timeline{} // Inisialisasi struct, nyambung ke timelines Model

	if getTimeline, err := update_timeline.FindByID(form.Id); err != nil {
		beego.Error("GetTimelineById : ", err) 
		if getTimeline == common.ErrNotFound {
			c.Data["json"] = common.NewErrorInfo(ErrDupUser)
		} else {
			c.Data["json"] = common.NewErrorInfo(ErrDatabase)
		}
		c.ServeJSON()
		return
	}

	// file 
	f, h, _ := c.GetFile("file") // inisialiasi form 
	path 	:= "./static/timeline_file/" + h.Filename // inisialiasi path
	
	dateUpdated := time.Now().UnixNano() / int64(time.Millisecond) // Convert time to milisec
	if h.Filename != "" { // check form
		//Deleting File 
		var url = "./static/timeline_file/" + update_timeline.File // Folder + Name File
		os.Remove(url) // Remove File.

		// Sudah berbeda block, jika kondisi h terpenuhi (tidak kosong), maka block code ini akan di eksekusi.
		code, err := update_timeline.UpdateTimeline(form.Id, form.Description, h.Filename, dateUpdated) //Eksekusi ke Database

		if err != nil { // var err, jika terpenuhi (tidak kosong)
			beego.Error("UpdateTimeline : ", err) 

			if code == common.ErrNotFound { // Check nilai var code. Jika sama dengan code ErrNotFound, nyambung ke Folder Common (Models)
				c.Data["json"] = common.NewErrorInfo(ErrNoUser)
			} else if code == common.ErrDatabase {
				c.Data["json"] = common.NewErrorInfo(ErrDatabase)
			} else {
				c.Data["json"] = common.NewErrorInfo(ErrSystem) 
			}

			c.ServeJSON()
			return
		}

		c.SaveToFile("file", path) // Inisialisasi post file
		c.Data["json"] = timelines.TimelineSingleInfo{Code: 200, Message: "Success", SingleTimeline: &update_timeline}
	} else {
		// Sudah berbeda block, jika kondisi h tidak terpenuhi (kosong), maka block code ini akan di eksekusi.
		code, err := update_timeline.UpdateTimeline(form.Id, form.Description, update_timeline.File, dateUpdated)

		if err != nil {
			beego.Error("UpdateTimeline: ", err)
			if code == common.ErrNotFound {
				c.Data["json"] = common.NewErrorInfo(ErrNoUser)
			} else if code == common.ErrDatabase {
				c.Data["json"] = common.NewErrorInfo(ErrDatabase)
			} else {
				c.Data["json"] = common.NewErrorInfo(ErrSystem)
			}

			c.ServeJSON()
			return
		}

		c.Data["json"] = timelines.TimelineSingleInfo{Code: 200, Message: "Success", SingleTimeline: &update_timeline}
	}
	f.Close() // Close Form
	c.ServeJSON()
}

// func (c *TimelineController) RepostFeed() {
// 	form := timelines.RepostTiemlineform{}

// 	if err := c.ParseForm(&form); err != nil {
// 		beego.Debug("RepostTiemlineform : ", err)
// 		c.Data["json"] = common.NewErrorInfo(ErrInputData)
// 		c.ServeJSON()
// 		return
// 	} 

// 	beego.Debug("ParseFormRepostTimeline : ", &form)
	
// }

func (c * TimelineController) DeleteTimelineById() {
	form := timelines.DeleteTimelineByIdForm{}
	if err := c.ParseForm(&form); err != nil {
		beego.Debug("DeleteTimelineById : ", err)
		c.Data["json"] = common.NewErrorInfo(ErrInputData)
		c.ServeJSON()
		return
	}

	beego.Debug("ParseFormDeleteTimelineById : ", &form)

	delete_timeline_by_id := timelines.Timeline{}

	if getTimeline, err := delete_timeline_by_id.FindByID(form.Id); err != nil {
		beego.Error("GetTimelineById : ", err) 
		if getTimeline == common.ErrNotFound {
			c.Data["json"] = common.NewErrorInfo(ErrDupUser)
		} else {
			c.Data["json"] = common.NewErrorInfo(ErrDatabase)
		}
		c.ServeJSON()
		return
	}

	var path = "./static/timeline_file/" + delete_timeline_by_id.File
	os.Remove(path)

	code, err := delete_timeline_by_id.DeleteTimeline(form.Id)

	if err != nil {
		beego.Error("DeleteTimelineById : ", err) 
		if code == common.ErrNotFound {
			c.Data["json"] = common.NewErrorInfo(ErrDupUser)
		} else {
			c.Data["json"] = common.NewErrorInfo(ErrDatabase)
		}
		c.ServeJSON()
		return
	}

	c.Data["json"] = common.NewNormalInfo("Success")
	c.ServeJSON()
}

func (c *TimelineController) PostComment() {
	form := comments.CommentsPostForm{}

	if err := c.ParseForm(&form); err != nil {
		beego.Debug("PostComment : ", err)
		c.Data["json"] = common.NewErrorInfo(ErrInputData)
		c.ServeJSON()
		return
	}

	beego.Debug("PostCommentForm : ", &form)

	// file 
	f, h, _ := c.GetFile("file")
	path 	:= "./static/timeline_file/" + h.Filename // inisialiasi path
	f.Close()
	c.SaveToFile("file", path)

	post_comment, err := comments.NewCommentsPost(&form, h.Filename)
	if err != nil {
		beego.Error("NewCommentPost:", err)
		c.Data["json"] = common.NewErrorInfo(ErrSystem)
		c.ServeJSON()
		return
	}
	beego.Debug("NewCommentPost:", &post_comment)

	if code, err := post_comment.InsertComment(); err != nil {
		beego.Error("InsertComment:", err)
		if code == common.ErrDupRows {
			c.Data["json"] = common.NewErrorInfo(ErrDupUser)
		} else {
			c.Data["json"] = common.NewErrorInfo(ErrDatabase)
		}
		c.ServeJSON()
		return
	}

	c.Data["json"] = common.NewNormalInfo("Succes")
	c.ServeJSON()
}

func (c *TimelineController) UpdateComment() {
	form := comments.UpdateCommentForm{}

	if err := c.ParseForm(&form); err != nil {
		beego.Debug("UpdateComment : ", err)
		c.Data["json"] = common.NewErrorInfo(ErrInputData)
		c.ServeJSON()
		return
	}
	beego.Debug("PostCommentForm : ", &form)

	update_comment := comments.Comments{}

	if get_comments, err := update_comment.GetCommentById(form.Id); err != nil {
		beego.Error("GetCommentById :", err)
		if get_comments == common.ErrNotFound {
			c.Data["json"] = common.NewErrorInfo(ErrDupUser)
		} else {
			c.Data["json"] = common.NewErrorInfo(ErrDatabase)
		}
		c.ServeJSON()
		return
	}

	// file 
	f, h, errUpload := c.GetFile("file") // inisialiasi form 
	path 	:= "./static/timeline_file/" + h.Filename // inisialiasi path

	dateUpdated := time.Now().UnixNano() / int64(time.Millisecond) // Convert time to milisec
	if errUpload != nil { // check form
		//Deleting File 
		var url = "./static/timeline_file/" + update_comment.File // Folder + Name File
		os.Remove(url) // Remove File.

		code, err := update_comment.UpdateComment(form.Id, form.Comment, update_comment.Id_timeline, form.File, dateUpdated)
		if err != nil { // var err, jika terpenuhi (tidak kosong)
			beego.Error("UpdateTimeline : ", err) 

			if code == common.ErrNotFound { // Check nilai var code. Jika sama dengan code ErrNotFound, nyambung ke Folder Common (Models)
				c.Data["json"] = common.NewErrorInfo(ErrNoUser)
			} else if code == common.ErrDatabase {
				c.Data["json"] = common.NewErrorInfo(ErrDatabase)
			} else {
				c.Data["json"] = common.NewErrorInfo(ErrSystem) 
			}

			c.ServeJSON()
			return
		}

		c.SaveToFile("file", path) // Inisialisasi post file
		c.Data["json"] = comments.ComentsReturnResponse{Code: 200, Message: "Success", CommentInfo: &update_comment}
	} else {
		code, err := update_comment.UpdateComment(form.Id, form.Comment, update_comment.Id_timeline, update_comment.File, dateUpdated)
		
		if err != nil { // var err, jika terpenuhi (tidak kosong)
			beego.Error("UpdateTimeline : ", err) 

			if code == common.ErrNotFound { // Check nilai var code. Jika sama dengan code ErrNotFound, nyambung ke Folder Common (Models)
				c.Data["json"] = common.NewErrorInfo(ErrNoUser)
			} else if code == common.ErrDatabase {
				c.Data["json"] = common.NewErrorInfo(ErrDatabase)
			} else {
				c.Data["json"] = common.NewErrorInfo(ErrSystem) 
			}

			c.ServeJSON()
			return
		}

		c.Data["json"] = comments.ComentsReturnResponse{Code: 200, Message: "Success", CommentInfo: &update_comment}
	}
	defer f.Close() // Close Form
	c.ServeJSON()
}

func (c *TimelineController) DeleteComment() {
	form := comments.DeleteCommentForm{}

	if err := c.ParseForm(&form); err != nil {
		beego.Debug("DeleteComment : ", err)
		c.Data["json"] = common.NewErrorInfo(ErrInputData)
		c.ServeJSON()
		return
	}
	beego.Debug("DeleteCommentCheckForm : ", &form)

	update_comment := comments.Comments{}

	if get_comments, err := update_comment.GetCommentById(form.Id); err != nil {
		beego.Error("GetCommentById :", err)
		if get_comments == common.ErrNotFound {
			c.Data["json"] = common.NewErrorInfo(ErrDupUser)
		} else {
			c.Data["json"] = common.NewErrorInfo(ErrDatabase)
		}
		c.ServeJSON()
		return
	}

	var url = "./static/timeline_file/" + update_comment.File // Folder + Name File
	os.Remove(url) // Remove File.

	code, err := update_comment.DeleteComment(form.Id)
	if err != nil {
		beego.Error("DeleteTimelineById : ", err) 
		if code == common.ErrNotFound {
			c.Data["json"] = common.NewErrorInfo(ErrDupUser)
		} else {
			c.Data["json"] = common.NewErrorInfo(ErrDatabase)
		}
		c.ServeJSON()
		return
	}

	c.Data["json"] = common.NewNormalInfo("Success")
	c.ServeJSON()
}

func (c *TimelineController) PostLikeTL() {
	form := likes.PostLikeTLForm{}
	if err := c.ParseForm(&form); err != nil {
		beego.Debug("ParseLike : ", err)
		c.Data["json"] = common.NewErrorInfo(ErrInputData)
		c.ServeJSON()
		return
	}
	beego.Debug("ParseLikeForm : ", &form)

	your_like, err := likes.NewLikesPost(&form)
	if err != nil {
		beego.Error("NewLike : ", err)
		c.Data["json"] = common.NewErrorInfo(ErrSystem)
		c.ServeJSON()
		return
	}
	beego.Debug("NewLike : ", &form)

	if code, err := your_like.InsertLike(); err != nil {
		beego.Error("InsertLike : ", err)
		if code == common.ErrDupRows {
			c.Data["json"] = common.NewErrorInfo(ErrDupUser)
		} else {
			c.Data["json"] = common.NewErrorInfo(ErrDatabase)
		}
		c.ServeJSON()
		return
	}
	c.Data["json"] = likes.LikeInfo{Code: 200, Message:"Success"}
	c.ServeJSON()
}

func (c *TimelineController) UnlikeTL() {
	form := likes.UnLikeTLform{}
	if err := c.ParseForm(&form); err != nil {
		beego.Debug("ParseUnlike : ", err)
		c.Data["json"] = common.NewErrorInfo(ErrInputData)
		c.ServeJSON()
		return
	}

	beego.Debug("ParseUnlikeForm : ", &form)

	unlike := likes.Like{}

	code, err := unlike.DeleteLike(form.Id_timeline, form.Email)

	if err != nil {
		beego.Error("DeleteEmployment : ", err)
		if code == common.ErrNotFound {
			c.Data["json"] = common.NewErrorInfo(ErrNoUser)
		} else {
			c.Data["json"] = common.NewErrorInfo(ErrDatabase)
		}
		c.ServeJSON()
		return
	}
	c.Data["json"] = likes.LikeInfo{Code: 200, Message: "Success"}
	c.ServeJSON()
}

func (c *TimelineController) GetLikeTL() {
	form := likes.GetLikeForm{}
	if err := c.ParseForm(&form); err != nil {
		beego.Debug("ParseGetLike : ", err)
		c.Data["json"] = common.NewErrorInfo(ErrInputData)
		c.ServeJSON()
		return
	}

	like_single_tl := likes.Like{}

	code, _ := like_single_tl.GetLikeTLByIdTL(form.Id_timeline)

	c.Data["json"] = likes.LikeInfo{Code: 200, Message: "Success", LikeInfo: &code}
	c.ServeJSON()
}