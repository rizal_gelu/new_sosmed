package controllers

import (
	"fmt"
	"time"
	"new_sosmed/models"
	common "new_sosmed/models/common"
	users "new_sosmed/models/users"
	profiles "new_sosmed/models/profiles"
	employmentUser "new_sosmed/models/profile_employment"
	educationHighSchool "new_sosmed/models/profile_education_high_school"
	educationCollege "new_sosmed/models/profile_education_college"
	residence "new_sosmed/models/profile_residence"
	qrcode "github.com/skip2/go-qrcode"
	bio "new_sosmed/models/bio"
	workplace "new_sosmed/models/workplace"
	social_links "new_sosmed/models/social_link"
	phone "new_sosmed/models/phone"
	address "new_sosmed/models/address"
	"github.com/astaxie/beego"
)

type UserController struct {
	BaseController
}

func (c *UserController) Register() {
	form_profile := profiles.CreateProfileForm{}
	if err := c.ParseForm(&form_profile); err != nil {
		beego.Debug("ParseCreateProfileForm : ", err)
		c.Data["json"] = common.NewErrorInfo(ErrInputData)
		c.ServeJSON()
		return
	}

	profile, err := profiles.NewProfile(&form_profile)
	if err != nil {
		beego.Error("CreateProfile : ", err)
		c.Data["json"] = common.NewErrorInfo(ErrSystem)
		c.ServeJSON()
		return
	}

	if code, err := profile.Insert(); err != nil {
		beego.Error("InsertProfile : ", err)
		if code == common.ErrDupRows {
			c.Data["json"] = common.NewErrorInfo(ErrDupUser)
		} else {
			c.Data["json"] = common.NewErrorInfo(ErrDatabase)
		}
		c.ServeJSON()
		return
	}

	form := users.RegisterForm{}
	if err := c.ParseForm(&form); err != nil {
		beego.Debug("ParseRegsiterForm:", err)
		c.Data["json"] = common.NewErrorInfo(ErrInputData)
		c.ServeJSON()
		return
	}
	beego.Debug("ParseRegsiterForm:", &form)

	regDate := time.Now()
	user, err := users.NewUser(&form, regDate)
	if err != nil {
		beego.Error("NewUser:", err)
		c.Data["json"] = common.NewErrorInfo(ErrSystem)
		c.ServeJSON()
		return
	}
	beego.Debug("NewUser:", user)

	if code, err := user.Insert(); err != nil {
		beego.Error("InsertUser:", err)
		if code == common.ErrDupRows {
			c.Data["json"] = common.NewErrorInfo(ErrDupUser)
		} else {
			c.Data["json"] = common.NewErrorInfo(ErrDatabase)
		}
		c.ServeJSON()
		return
	}

	go models.IncTotalUserCount(regDate)

	// Generate QrCode By Email
	var pathQrCode = "./static/qrcode/" // Path of image qrCode

	q, err := qrcode.New(form.Email, qrcode.Medium) // Eksekusi QrCode, format Medium
	if err != nil {
		panic(err)
	}

	q.DisableBorder = true // Set Border

	err = q.WriteFile(256, pathQrCode+form.Email+".png") // Create File PNG.
	if err != nil {
		panic(err)
	}

	c.Data["json"] = common.NewNormalInfo("Succes")
	c.ServeJSON()
}

func (c *UserController) Login() {
	form := users.LoginForm{}
	if err := c.ParseForm(&form); err != nil {
		beego.Debug("ParseLoginForm : ", err)
		c.Data["json"] = common.NewErrorInfo(ErrInputData)
		c.ServeJSON()
		return
	}
	beego.Debug("ParseLoginForm : ", &form)

	if err := c.VerifyForm(&form); err != nil {
		beego.Debug("ValidLoginForm:", err)
		c.Data["json"] = common.NewErrorInfo(ErrInputData)
		c.ServeJSON()
		return
	}

	user := users.User{}
	if code, err := user.FindByID(form.Email); err != nil {
		beego.Error("FindUserById:", err)
		if code == common.ErrNotFound {
			c.Data["json"] = common.NewErrorInfo(ErrNoUser)
		} else {
			c.Data["json"] = common.NewErrorInfo(ErrDatabase)
		}
		c.ServeJSON()
		return
	}
	beego.Debug("UserInfo:", &user)

	if ok, err := user.CheckPass(form.Password); err != nil {
		beego.Error("CheckUserPass:", err)
		c.Data["json"] = common.NewErrorInfo(ErrSystem)
		c.ServeJSON()
		return
	} else if !ok {
		c.Data["json"] = common.NewErrorInfo(ErrPass)
		c.ServeJSON()
		return
	}
	user.ClearPass()

	c.SetSession("user_id", form.Email)

	c.Data["json"] = &users.LoginInfo{Code: 200, Data: &user}
	c.ServeJSON()
}

func (c *UserController) Logout() {
	form := users.LogoutForm{}

	if err := c.ParseForm(&form); err != nil {
		beego.Debug("ParseLogoutForm : ", err)
		c.Data["json"] = common.NewErrorInfo(ErrInputData)
		c.ServeJSON()
		return
	}
	beego.Debug("ParseLogoutForm : ", &form)

	if err := c.VerifyForm(&form); err != nil {
		beego.Debug("ValidLogoutForm:", err)
		c.Data["json"] = common.NewErrorInfo(ErrInputData)
		c.ServeJSON()
		return
	}

	if c.GetSession("user_id") != form.Email {
		c.Data["json"] = common.NewErrorInfo(ErrInvalidUser)
		c.ServeJSON()
		return
	}

	c.DelSession("user_id")

	c.Data["json"] = common.NewNormalInfo("Succes")
	c.ServeJSON()
}

func (c *UserController) Change_Password() {
	form := users.Change_PasswordForm{}

	if err := c.ParseForm(&form); err != nil {
		beego.Debug("ParsePasswdForm : ", err)
		c.Data["json"] = common.NewErrorInfo(ErrInputData)
		c.ServeJSON()
		return
	}
	beego.Debug("ParsePasswdForm : ", &form)

	if err := c.VerifyForm(&form); err != nil {
		beego.Debug("ValidPasswdForm:", err)
		c.Data["json"] = common.NewErrorInfo(ErrInputData)
		c.ServeJSON()
		return
	}

	if c.GetSession("user_id") != form.Email {
		c.Data["json"] = common.NewErrorInfo(ErrInvalidUser)
		c.ServeJSON()
		return
	}

	code, err := users.ChangePass(form.Email, form.OldPass, form.NewPass)
	if err != nil {
		beego.Error("ChangeUserPass:", err)
		if code == common.ErrNotFound {
			c.Data["json"] = common.NewErrorInfo(ErrNoUserPass)
		} else if code == common.ErrDatabase {
			c.Data["json"] = common.NewErrorInfo(ErrDatabase)
		} else {
			c.Data["json"] = common.NewErrorInfo(ErrSystem)
		}
		c.ServeJSON()
		return
	}

	c.Data["json"] = common.NewNormalInfo("Succes")
	c.ServeJSON()
}

func (c *UserController) Activation_user() {
	form := users.ActivationUserForm{}

	if err := c.ParseForm(&form); err != nil {
		beego.Debug("ParseActivation : ", err)
		c.Data["json"] = common.NewErrorInfo(ErrInputData)
		c.ServeJSON()
		return
	}
	beego.Debug("ParseActivation : ", &form)
	fmt.Println(form.Email)

	code, err := users.ActivateUser(form.Email, form.Activation_code)
	if err != nil {
		beego.Error("Activation_user : ", err)

		if code == common.ErrNotFound {
			c.Data["json"] = common.NewErrorInfo(ErrNoUser)
		} else if code == common.ErrDatabase {
			c.Data["json"] = common.NewErrorInfo(ErrDatabase)
		} else {
			c.Data["json"] = common.NewErrorInfo(ErrSystem)
		}
		c.ServeJSON()
		return
	} 
	c.Data["json"] = common.NewNormalInfo("Success")
	c.ServeJSON()
}

func (c *UserController) Forgot_password() {
	form := users.Forgot_password{}

	if err := c.ParseForm(&form);  err != nil {
		beego.Debug("ParseForgotPassword : ", err)
		c.Data["json"] = common.NewErrorInfo(ErrInputData) 
		c.ServeJSON()
		return
	}
	beego.Debug("ParseForgotPassword : ", &form)

	code, err := users.ForgotPassword(form.Email)
	if err != nil {
		beego.Error("ForgotPassword : ", err)

		if code == common.ErrNotFound {
			c.Data["json"] = common.NewErrorInfo(ErrNoUser)
		} else if code == common.ErrDatabase {
			c.Data["json"] = common.NewErrorInfo(ErrDatabase)
		} else if code == common.ErrUserNotActive {
			c.Data["json"] = common.NewErrorInfo(ErrUserNotActive)
		} else {
			c.Data["json"] = common.NewErrorInfo(ErrSystem)
		}
		c.ServeJSON()
		return
	}

	c.Data["json"] = common.NewNormalInfo("Success")
	c.ServeJSON()
}

func (c*UserController) ForgotPasswordConfirm() {
	form := users.Forgot_pwd_confirm{}

	if err := c.ParseForm(&form); err != nil {
		beego.Debug("ForgotPasswordConfirm : ", err)
		c.Data["json"] = common.NewErrorInfo(ErrInputData)
		c.ServeJSON()
		return
	}
	beego.Debug("ForgotPasswordConfirm : ", &form)
	fmt.Println(form.Email)

	code, err := users.ForgotPasswordConfirm(form.Email, form.Forgot_pwd_code)
	if err != nil {
		beego.Error("ForgotPasswordConfirm : ", err)

		if code == common.ErrNotFound {
			c.Data["json"] = common.NewErrorInfo(ErrNoUser)
		} else if code == common.ErrDatabase {
			c.Data["json"] = common.NewErrorInfo(ErrDatabase)
		} else if code == common.ErrUserNotActive {
			c.Data["json"] = common.NewErrorInfo(ErrUserNotActive)
		} else {
			c.Data["json"] = common.NewErrorInfo(ErrSystem)
		}
		c.ServeJSON()
		return
	}
	c.Data["json"] = common.NewNormalInfo("Success")
	c.ServeJSON()
}

func (c *UserController) Reset_password() {
	form := users.Reset_password{}

	if err := c.ParseForm(&form); err != nil {
		beego.Debug("ParseResetPassword : ", err)
		c.Data["json"] = common.NewErrorInfo(ErrInputData)
		c.ServeJSON()
		return
	}
	beego.Debug("ParseResetPassword : ", &form)

	code, err := users.ResetPassword(form.Email, form.Forgot_password_code, form.NewPass)
	if err != nil {
		beego.Error("ResetPassword : ", err)

		if code == common.ErrNotFound {
			c.Data["json"] = common.NewErrorInfo(ErrNoUser)
		} else if code == common.ErrDatabase {
			c.Data["json"] = common.NewErrorInfo(ErrDatabase)
		} else {
			c.Data["json"] = common.NewErrorInfo(ErrSystem)
		}
		c.ServeJSON()
		return
	}

	c.Data["json"] = common.NewNormalInfo("Success")
	c.ServeJSON()
}

func (c *UserController) Find_Profile() {
	form := profiles.FindProfileByEmailForm{}

	if err := c.ParseForm(&form); err != nil {
		beego.Debug("ParseProfileForm : ", err)
		c.Data["json"] = common.NewNormalInfo(ErrInputData)
		c.ServeJSON()
		return
	}
	beego.Debug("ParseProfileForm : ", &form)

	profile_account := profiles.Profile{}

	if code, err := profile_account.FindByEmail(form.Email); err != nil {
		beego.Error("FindByEmail : ", err) 
		if code == common.ErrNotFound {
			c.Data["json"] = common.NewErrorInfo(ErrNoUser)
		} else {
			c.Data["json"] = common.NewErrorInfo(ErrDatabase)
		}
		c.ServeJSON()
		return
	}
	beego.Debug("ProfileInfo : ", &profile_account)

	c.Data["json"] = profiles.SingleProfileInfo{Code: 200, Message:"Success", UserInfo: &profile_account}
	c.ServeJSON()
}

func (c *UserController) Update_Profile() {
	form := profiles.UpdateProfileForm{}

	if err := c.ParseForm(&form); err != nil {
		beego.Debug("ParseUpdateProfile : ", err)
		c.Data["json"] = common.NewErrorInfo(ErrInputData)
		c.ServeJSON()
		return
	}
	beego.Debug("ParseResetProfile : ", &form)

	// file 
	f, h, _ := c.GetFile("avatar")
	path 	:= "./static/photo_profile/" + h.Filename // inisialiasi path
	f.Close()
	c.SaveToFile("avatar", path)
	
	profile_update := profiles.Profile{}
	code, err := profile_update.Update(form.Email, form.First_name, form.Last_name, form.Date_of_birth, h.Filename)
	if err != nil {
		beego.Error("UpdateProfile : ", err) 

		if code == common.ErrNotFound {
			c.Data["json"] = common.NewErrorInfo(ErrNoUser)
		} else if code == common.ErrDatabase {
			c.Data["json"] = common.NewErrorInfo(ErrDatabase)
		} else {
			c.Data["json"] = common.NewErrorInfo(ErrSystem) 
		}
		c.ServeJSON()
		return
	}

	c.Data["json"] = profiles.SingleProfileInfo{Code: 200, UserInfo: &profile_update}
	c.ServeJSON()
}

func (c *UserController) PrivateAccountActive() {
	form := profiles.PrivateAccountActiveForm{}

	if err := c.ParseForm(&form); err != nil {
		beego.Debug("ParsePrivateAccount : ", err)
		c.Data["json"] = common.NewErrorInfo(ErrInputData)
		c.ServeJSON()
		return
	}
	beego.Debug("ParsePrivateAccountForm : ", &form)

	profile_update := profiles.Profile{}
	code, err := profile_update.PrivateAccountActive(form.Email)
	if err != nil {
		beego.Error("PrivateAccountActive : ", err)
		if code == common.ErrNotFound {
			c.Data["json"] = common.NewErrorInfo(ErrNoUser)
		} else if code == common.ErrDatabase {
			c.Data["json"] = common.NewErrorInfo(ErrDatabase)
		} else {
			c.Data["json"] = common.NewErrorInfo(ErrSystem)
		}
		c.ServeJSON()
		return
	}

	c.Data["json"] = profiles.PrivateAccountActiveResponse{Code: 200, Message: "Success"}
	c.ServeJSON()
}

func (c *UserController) PrivateAccountDeActive() {
	form := profiles.PrivateAccountDeActiveForm{}
	if err := c.ParseForm(&form); err != nil {
		beego.Debug("ParsePrivateAccountDeActive : ", err)
		c.Data["json"] = common.NewErrorInfo(ErrInputData)
		c.ServeJSON()
		return
	}

	beego.Debug("ParsePrivateAccountForm : ", &form)

	profile_update := profiles.Profile{}

	code, err := profile_update.PrivateAccountDeActive(form.Email)
	if err != nil {
		beego.Error("PrivateAccountDeActive : ", err) 
		if code == common.ErrNotFound{
			c.Data["json"] = common.NewErrorInfo(ErrNoUser)
		} else if code == common.ErrDatabase{
			c.Data["json"] = common.NewErrorInfo(ErrDatabase)
		} else {
			c.Data["json"] = common.NewErrorInfo(ErrSystem)
		}
		c.ServeJSON()
		return
	}

	c.Data["json"] = profiles.PrivateAccountDeActiveResponse{Code: 200, Message: "Success"}
	c.ServeJSON()
}

func (c *UserController) PostEmployment() {
	form := employmentUser.CreateEmploymentProfileForm{}
	if err := c.ParseForm(&form); err != nil {
		beego.Debug("ParseEmploymentUserForm : ", err)
		c.Data["json"] = common.NewErrorInfo(ErrInputData)
		c.ServeJSON()
		return
	}

	beego.Debug("ParseEmploymentUserForm : ", &form)

	employment, err := employmentUser.NewEmploymentUser(&form)
	if err != nil {
		beego.Error("NewEmploymentUser : ", err)
		c.Data["json"] = common.NewErrorInfo(ErrSystem)
		c.ServeJSON()
		return
	}
	beego.Debug("NewEmploymentUser : ", employment)

	if code, err := employment.Insert(); err != nil {
		beego.Error("InsertEmployment : ", err)
		if code == common.ErrDupRows {
			c.Data["json"] = common.NewErrorInfo(ErrDupUser)
		} else {
			c.Data["json"] = common.NewErrorInfo(ErrDatabase)
		}
		c.ServeJSON()
		return
	}
	c.Data["json"] = common.NewNormalInfo("Success")
	c.ServeJSON()
}

func (c *UserController) GetEmploymentByEmail() {
	form := employmentUser.GetEmploymentByEmailForm{}
	if err := c.ParseForm(&form); err != nil {
		beego.Debug("ParseEmploymentByEmailForm : ", err)
		c.Data["json"] = common.NewErrorInfo(ErrInputData)
		c.ServeJSON()
		return
	}

	beego.Debug("ParseEmploymentUserForm : ", &form)

	employment := employmentUser.EmploymentUser{}

	if code, err := employment.FindByEmail(form.Email); err != nil {
		beego.Error("GetEmploymentByEmail : ", err)
		if code == common.ErrDupRows {
			c.Data["json"] = common.NewErrorInfo(ErrSystem)
		} else {
			c.Data["json"] = common.NewErrorInfo(ErrDatabase)
		}
		c.ServeJSON()
		return
	}
	c.Data["json"] = employmentUser.ResponseGetEmploymentInfo{Code: 200, Message: "Success", Data: &employment}
	c.ServeJSON()
}

func (c *UserController) UpdateEmployment() {
	form := employmentUser.UpdateEmploymentForm{}
	if err := c.ParseForm(&form); err != nil {
		beego.Debug("UpdateEmployment : ", err) 
		c.Data["json"] = common.NewErrorInfo(ErrInputData)
		c.ServeJSON()
		return
	}
	beego.Debug("UpdateEmployment : ", &form)

	update_employment := employmentUser.EmploymentUser{}

	if getEmploymentUser, err := update_employment.FindByEmail(form.Email); err != nil {
		beego.Error("GetEmploymentUSer : ", err)
		if getEmploymentUser == common.ErrNotFound{
			c.Data["json"] = common.NewErrorInfo(ErrDupUser)
		} else {
			c.Data["json"] = common.NewErrorInfo(ErrDatabase)
		}
		c.ServeJSON()
		return
	}

	code, err := update_employment.Update(form.Email, form.Company, form.Position, form.City, form.Date_start, form.Date_end, form.Project_name, form.Description)
	if err != nil {
		beego.Error("UpdateEmployment : ", err)
		if code == common.ErrNotFound {
			c.Data["json"] = common.NewErrorInfo(ErrNoUser)
		} else if code == common.ErrDatabase {
			c.Data["json"] = common.NewErrorInfo(ErrDatabase)
		} else {
			c.Data["json"] = common.NewErrorInfo(ErrSystem)
		}

		c.ServeJSON()
		return
	}

	c.Data["json"] = employmentUser.ResponseUpdateEmploymentInfo{Code: 200, Message: "Success", Data: &update_employment}
	c.ServeJSON()
}

func (c *UserController) DeleteEmployment() {
	form := employmentUser.DeleteEmploymentForm{}
	if err := c.ParseForm(&form); err != nil {
		beego.Debug("DeleteEmploymentForm : ", err)
		c.Data["json"] = common.NewErrorInfo(ErrInputData)
		c.ServeJSON()
		return
	}

	beego.Debug("ParseFormDeleteEmployment : ", &form)

	deleteEmploymentUser := employmentUser.EmploymentUser{}

	code, err := deleteEmploymentUser.DeleteEploymentUser(form.Email)

	if err != nil {
		beego.Error("DeleteEmployment : ", err)
		if code == common.ErrNotFound {
			c.Data["json"] = common.NewErrorInfo(ErrNoUser)
		} else {
			c.Data["json"] = common.NewErrorInfo(ErrDatabase)
		}
		c.ServeJSON()
		return
	}
	c.Data["json"] = employmentUser.ResponseDeleteEmployeeInfo{Code: 200, Message: "Success"}
	c.ServeJSON()
}

func (c *UserController) PostEducationHighSchool() {
	form := educationHighSchool.CreateEducationHighSchoolProfileForm{}
	if err := c.ParseForm(&form); err != nil {
		beego.Debug("ParseEducationHighSchool : ", err)
		c.Data["json"] = common.NewErrorInfo(ErrInputData)
		c.ServeJSON()
		return
	}
	beego.Debug("ParseEducationHighSchool : ", &form)

	high_school, err := educationHighSchool.NewEducationHighSchool(&form)
	if err != nil {
		beego.Error("NewHighSchool : ", err)
		c.Data["json"] = common.NewErrorInfo(ErrSystem)
		c.ServeJSON()
		return
	}

	beego.Debug("NewHighSchool : ", high_school)

	if code, err := high_school.Insert(); err != nil {
		beego.Error("HighSchool : ", err)
		if code == common.ErrDupRows {
			c.Data["json"] = common.NewErrorInfo(ErrDupUser)
		} else {
			c.Data["json"] = common.NewErrorInfo(ErrDatabase)
		}
		c.ServeJSON()
		return
	}
	c.Data["json"] = common.NewNormalInfo("Success")
	c.ServeJSON()
}

func (c *UserController) GetEducationHighSchoolByEmail() {
	form := educationHighSchool.GetEducationHighSchoolForm{}
	if err := c.ParseForm(&form); err != nil {
		beego.Debug("ParseEducationHighSchool : ", err)
		c.Data["json"] = common.NewErrorInfo(ErrInputData)
		c.ServeJSON()
		return
	}
	beego.Debug("ParseEducationHighSchool : ", &form)

	high_school := educationHighSchool.EducationHighSchool{}
	if edu_high_school, err := high_school.FindEducationHighSchoolByEmail(form.Email); err != nil {
		beego.Error("FindEducationHighSchoolByEmail : ", err)
		if edu_high_school == common.ErrNotFound {
			c.Data["json"] = common.NewErrorInfo(ErrNoUser)
		} else {
			c.Data["json"] = common.NewErrorInfo(ErrDatabase)
		}
		c.ServeJSON()
		return
	}

	c.Data["json"] = educationHighSchool.ResponseGetEducationHighSchoolInfo{Code: 200, Message: "Success", Data: &high_school}
	c.ServeJSON()
}

func (c *UserController) UpdateEducationHighSchool() {
	form := educationHighSchool.UpdateEducationHighSchoolForm{}
	if err := c.ParseForm(&form); err != nil {
		beego.Debug("ParseUpdateEducationHighSchool : ", err)
		c.Data["json"] = common.NewErrorInfo(ErrInputData)
		c.ServeJSON()
		return
	}
	beego.Debug("ParseUpdateEducationHighSchool : ", &form)

	update_high_school := educationHighSchool.EducationHighSchool{}

	if getHighSchool, err := update_high_school.FindEducationHighSchoolByEmail(form.Email); err != nil {
		beego.Error("GetEducationHigShool : ", &form)
		if getHighSchool == common.ErrNotFound {
			c.Data["json"] = common.NewErrorInfo(ErrDupUser)
		} else {
			c.Data["json"] = common.NewErrorInfo(ErrDatabase)
		}
		c.ServeJSON()
		return
	}

	code, err := update_high_school.UpdateEducationHighSchool(form.Email, form.High_school_name, form.Year_graduate, form.Graduated, form.Description)
	if err != nil {
		beego.Error("UpdateHighSchool : ", err)
		if code == common.ErrNotFound {
			c.Data["json"] = common.NewErrorInfo(ErrNoUser)
		} else if code == common.ErrDatabase {
			c.Data["json"] = common.NewErrorInfo(ErrDatabase)
		} else {
			c.Data["json"] = common.NewErrorInfo(ErrSystem)
		}

		c.ServeJSON()
		return
	}

	c.Data["json"] = educationHighSchool.ResponseUpdateEducationHighSchoolInfo{Code: 200, Message: "Success", Data: &update_high_school}
	c.ServeJSON()
}

func (c *UserController) DeleteEducationHighSchool() {
	form := educationHighSchool.DeleteEducationHighSchoolForm{}
	if err := c.ParseForm(&form); err != nil {
		beego.Debug("ParseDeleteEducationHighSchool : ", err)
		c.Data["json"] = common.NewErrorInfo(ErrInputData)
		c.ServeJSON()
		return
	}
	beego.Debug("ParseUpdateEducationHighSchool : ", &form)

	delete_high_school := educationHighSchool.EducationHighSchool{}
	if getHigh_school, err := delete_high_school.FindEducationHighSchoolByEmail(form.Email); err != nil {
		beego.Error("GetHighSchool : ", err)
		if getHigh_school == common.ErrNotFound {
			c.Data["json"] = common.NewErrorInfo(ErrDupUser)
		} else {
			c.Data["json"] = common.NewErrorInfo(ErrDatabase)
		}

		c.ServeJSON()
		return
	}

	code, err := delete_high_school.DeleteEducationHighSchool(form.Email)
	if err != nil {
		beego.Error("DeleteHighSchool : ", err)
		if code == common.ErrNotFound {
			c.Data["json"] = common.NewErrorInfo(ErrDupUser)
		} else {
			c.Data["json"] = common.NewErrorInfo(ErrDatabase)
		}
		c.ServeJSON()
		return
	}

	c.Data["json"] = educationHighSchool.ResponseDeleteEducationHighSchool{Code: 200, Message: "Success"}
	c.ServeJSON()
}

func (c *UserController) PostEducationCollege() {
	form := educationCollege.CreateEducationCollegeForm{}
	if err := c.ParseForm(&form); err != nil {
		beego.Debug("ParseCreateEducationCollege : ", err)
		c.Data["json"] = common.NewErrorInfo(ErrInputData)
		c.ServeJSON()
		return
	}
	beego.Debug("ParseCreateEducationCollege : ", &form)

	education_college, err := educationCollege.NewEducationCollege(&form)
	if err != nil {
		beego.Error("CreateEducationCollege : ", err)
		c.Data["json"] = common.NewErrorInfo(ErrSystem)
		c.ServeJSON()
		return
	}
	beego.Debug("NewEducationCollege : ", education_college)

	if code, err := education_college.Insert(); err != nil {
		beego.Error("InsertCollege : ", err)
		if code == common.ErrDupRows {
			c.Data["json"] = common.NewErrorInfo(ErrDupUser)
		} else {
			c.Data["json"] = common.NewErrorInfo(ErrDatabase)
		}
		c.ServeJSON()
		return
	}

	c.Data["json"] = educationCollege.ResponsePostEducationCollegeInfo{Code: 200, Message: "Success"}
	c.ServeJSON()
}

func (c *UserController) GetEducationCollegeByEmail() {
	form := educationCollege.GetEducationCollegeByEmailForm{}
	if err := c.ParseForm(&form); err != nil {
		beego.Debug("ParseGetEducationCollageByEmail : ", err)
		c.Data["json"] = common.NewErrorInfo(ErrInputData)
		c.ServeJSON()
		return
	}
	beego.Debug("ParseGetEducationCollageByEmail : ", &form)

	getEducationByEmail := educationCollege.EducationCollege{}

	if code, err := getEducationByEmail.FindEducationCollegeByEmail(form.Email); err != nil {
		beego.Error("GetEducationByEmail : ", err)
		if code == common.ErrNotFound {
			c.Data["json"] = common.NewErrorInfo(ErrNoUser)
		} else {
			c.Data["json"] = common.NewErrorInfo(ErrDatabase)
		}
		c.ServeJSON()
		return
	}

	c.Data["json"] = educationCollege.ResponseGetEducationCollegeInfo{Code: 200, Message: "Success", Data: &getEducationByEmail}
	c.ServeJSON()
}

func (c *UserController) UpdateEducationCollege() {
	form := educationCollege.UpdateEducationCollegeForm{}
	if err := c.ParseForm(&form); err != nil {
		beego.Debug("ParseUpdateEducationCollege : ", err)
		c.Data["json"] = common.NewErrorInfo(ErrInputData)
		c.ServeJSON()
		return
	}
	beego.Debug("ParseUpdateEducationCollege : ", &form)

	updateEducationCollege := educationCollege.EducationCollege{}

	if getEducationCollege, err := updateEducationCollege.FindEducationCollegeByEmail(form.Email); err != nil {
		beego.Error("UpdateEducationCollege : ", err)
		if getEducationCollege == common.ErrNotFound {
			c.Data["json"] = common.NewErrorInfo(ErrDupUser)
		} else {
			c.Data["json"] = common.NewErrorInfo(ErrDatabase)
		}
		c.ServeJSON()
		return
	}

	code, err := updateEducationCollege.UpdateEducationCollege(form.Email, form.College_name, form.Faculty_major, form.Graduated, form.Year_graduate, form.Description)
	if err != nil {
		beego.Error("UpdateEducationCollege : ", err)
		if code == common.ErrNotFound {
			c.Data["json"] = common.NewErrorInfo(ErrNoUser)
		} else if code == common.ErrDatabase {
			c.Data["json"] = common.NewErrorInfo(ErrDatabase)
		} else {
			c.Data["json"] = common.NewErrorInfo(ErrSystem)
		}

		c.ServeJSON()
		return
	}

	c.Data["json"] = educationCollege.ResponseUpdateEducationCollegeInfo{Code: 200, Message: "Success", Data: &updateEducationCollege}
	c.ServeJSON()
}

func (c *UserController) DeleteEducationCollege() {
	form := educationCollege.DeleteEducationCollegeForm{}
	if err := c.ParseForm(&form); err != nil {
		beego.Debug("ParseDeleteEducationCollege : ", err)
		c.Data["json"] = common.NewErrorInfo(ErrDupUser)
		c.ServeJSON()
		return
	}
	beego.Debug("ParseDeleteEducationCollege : ", &form)

	deleteEducationCollege := educationCollege.EducationCollege{}

	if getEducationCollage, err := deleteEducationCollege.FindEducationCollegeByEmail(form.Email); err != nil {
		beego.Error("GetEducationCollage : ", err)
		if getEducationCollage == common.ErrNotFound {
			c.Data["json"] = common.NewErrorInfo(ErrDupUser)
		} else {
			c.Data["json"] = common.NewErrorInfo(ErrDatabase)
		}
		c.ServeJSON()
		return
	}

	code, err := deleteEducationCollege.DeleteEducationCollage(form.Email)
	if err != nil {
		beego.Error("DeleteEducationCollage : ", err)
		if code == common.ErrNotFound {
			c.Data["json"] = common.NewErrorInfo(ErrDupUser)
		} else {
			c.Data["json"] = common.NewErrorInfo(ErrDatabase)
		}
		c.ServeJSON()
		return
	} 

	c.Data["json"] = educationCollege.ResponseDeleteEducationCollegeForm{Code: 200, Message:"Success"}
	c.ServeJSON()
}

func (c *UserController) PostResidence() {
	form := residence.CreateResidenceForm{}
	if err := c.ParseForm(&form); err != nil {
		beego.Debug("ParseResidence : ", err)
		c.Data["json"] = common.NewErrorInfo(ErrInputData)
		c.ServeJSON()
		return
	}
	beego.Debug("ParseResidence : ", &form)

	your_residence, err := residence.NewResidence(&form)
	if err != nil {
		beego.Error("NewResidence : ", err)
		c.Data["json"] = common.NewErrorInfo(ErrSystem)
		c.ServeJSON()
		return
	}
	beego.Debug("NewResidence : ", &form)

	if code, err := your_residence.Insert(); err != nil {
		beego.Error("InsertResidence : ", err)
		if code == common.ErrDupRows {
			c.Data["json"] = common.NewErrorInfo(ErrDupUser)
		} else {
			c.Data["json"] = common.NewErrorInfo(ErrDatabase)
		}
		c.ServeJSON()
		return
	}
	c.Data["json"] = residence.ResponsePostResidence{Code: 200, Message:"Success"}
	c.ServeJSON()
}

func (c *UserController) GetResidenceByEmail() {
	form := residence.GetResidenceByEmailForm{}
	if err := c.ParseForm(&form); err != nil {
		beego.Debug("ParseGetResidenceForm : ", err)
		c.Data["json"] = common.NewErrorInfo(ErrInputData)
		c.ServeJSON()
		return
	}
	beego.Debug("ParseGetResidenceForm : ", &form)

	residence_struct := residence.Residence{}

	if code, err := residence_struct.FindResidenceByEmail(form.Email); err != nil {
		beego.Error("GetResidenceByEmail : ", err)
		if code == common.ErrNotFound {
			c.Data["json"] = common.NewErrorInfo(ErrDupUser)
		} else {
			c.Data["json"] = common.NewErrorInfo(ErrDatabase)
		}
		c.ServeJSON()
		return
	}

	c.Data["json"] = residence.ResponseGetResidenceInfo{Code: 200, Message: "Success", Data: &residence_struct}
	c.ServeJSON()
}

func (c *UserController) UpdateResidence() {
	form := residence.UpdateResidenceForm{}
	if err := c.ParseForm(&form); err != nil {
		beego.Debug("ParseUpdateResidence : ", err )
		c.Data["json"] = common.NewErrorInfo(ErrInputData)
		c.ServeJSON()
		return
	}
	beego.Debug("ParseUpdateResidence : ", &form)

	residence_struct := residence.Residence{}

	if getResidence, err := residence_struct.FindResidenceByEmail(form.Email); err != nil {
		beego.Error("UpdateResidence : ", err)
		if getResidence == common.ErrNotFound {
			c.Data["json"] = common.NewErrorInfo(ErrDupUser)
		} else {
			c.Data["json"] = common.NewErrorInfo(ErrDatabase)
		}
		c.ServeJSON()
		return
	}

	code, err := residence_struct.UpdateResidence(form.Email, form.City, form.Address)
	if err != nil {
		beego.Error("UpdateResidence : ", err)

		if code == common.ErrNotFound {
			c.Data["json"] = common.NewErrorInfo(ErrNoUser)
		} else if code == common.ErrDatabase {
			c.Data["json"] = common.NewErrorInfo(ErrDatabase)
		} else {
			c.Data["json"] = common.NewErrorInfo(ErrSystem)
		}
		c.ServeJSON()
		return
	}

	c.Data["json"] = residence.ResponseUpdateResidenceInfo{Code: 200, Message: "Success", Data: &residence_struct}
	c.ServeJSON()
}

func (c *UserController) DeleteResidence() {
	form := residence.DeleteResidenceForm{}
	if err := c.ParseForm(&form); err != nil {
		beego.Debug("ParseDeleteResidence : ", err)
		c.Data["json"] = common.NewErrorInfo(ErrInputData)
		c.ServeJSON()
		return
	}
	beego.Debug("DeleteResidenceForm: ", &form)

	delete_residence := residence.Residence{}
	if getResidence, err := delete_residence.FindResidenceByEmail(form.Email); err != nil {
		beego.Error("DeleteResidence : ", err)
		if getResidence == common.ErrNotFound {
			c.Data["json"] = common.NewErrorInfo(ErrDupUser)
		} else {
			c.Data["json"] = common.NewErrorInfo(ErrDatabase)
		}
		c.ServeJSON()
		return
	}

	code, err := delete_residence.DeleteResidence(form.Email)
	if err != nil {
		beego.Error("DeleteResidence : ", err)
		if code == common.ErrNotFound {
			c.Data["json"] = common.NewErrorInfo(ErrDupUser)
		} else {
			c.Data["json"] = common.NewErrorInfo(ErrDatabase)
		}
		c.ServeJSON()
		return
	}

	c.Data["json"] = residence.ResponseDeleteResidenceInfo{Code: 200, Message: "Success"}
	c.ServeJSON()
}

func (c *UserController) Post_bio() {
	form := bio.BioPostForm{}
	if err := c.ParseForm(&form); err != nil {
		beego.Debug("ParseBio : ", err)
		c.Data["json"] = common.NewErrorInfo(ErrInputData)
		c.ServeJSON()
		return
	}
	beego.Debug("ParseBioForm : ", &form)

	your_bio, err := bio.NewBioPost(&form)
	if err != nil {
		beego.Error("NewBio : ", err)
		c.Data["json"] = common.NewErrorInfo(ErrSystem)
		c.ServeJSON()
		return
	}
	beego.Debug("NewBio : ", &form)

	if code, err := your_bio.Insert(); err != nil {
		beego.Error("InsertResidence : ", err)
		if code == common.ErrDupRows {
			c.Data["json"] = common.NewErrorInfo(ErrDupUser)
		} else {
			c.Data["json"] = common.NewErrorInfo(ErrDatabase)
		}
		c.ServeJSON()
		return
	}
	c.Data["json"] = bio.BioInfo{Code: 200, Message:"Success"}
	c.ServeJSON()
}

func (c *UserController) Get_bio() {
	form := bio.BioGetForm{}
	if err := c.ParseForm(&form); err != nil {
		beego.Debug("ParseBio : ", err)
		c.Data["json"] = common.NewErrorInfo(ErrInputData)
		c.ServeJSON()
		return
	}
	beego.Debug("ParseBioForm : ", &form)

	your_bio := bio.Bio{}

	if code, err := your_bio.FindByID(form.Email); err != nil {
		beego.Error("GetBioByEmail : ", err)
		if code == common.ErrNotFound {
			c.Data["json"] = common.NewErrorInfo(ErrDupUser)
		} else {
			c.Data["json"] = common.NewErrorInfo(ErrDatabase)
		}
		c.ServeJSON()
		return
	}

	c.Data["json"] = bio.BioInfo{Code: 200, Message: "Success", Bio: &your_bio}
	c.ServeJSON()
}

func (c *UserController) Add_workplace() {
	form := workplace.AddWorkplaceForm{}
	if err := c.ParseForm(&form); err != nil {
		beego.Debug("ParseWorkplace :", err)
		c.Data["json"] = common.NewErrorInfo(ErrInputData)
		c.ServeJSON()
		return
	}
	beego.Debug("ParseWorkplaceForm : ", &form)

	u_workplace, err := workplace.NewWorkPlace(&form)
	if err != nil {
		beego.Error("NewWorkPlace : ", err)
		c.Data["json"] = common.NewErrorInfo(ErrSystem)
		c.ServeJSON()
		return
	}
	beego.Debug("NewWorkPlace : ", &form)

	if code, err := u_workplace.Insert(); err != nil {
		beego.Error("InsertWorkplace : ", err)
		if code == common.ErrDupRows {
			c.Data["json"] = common.NewErrorInfo(ErrDupUser)
		} else {
			c.Data["json"] = common.NewErrorInfo(ErrDatabase)
		}
		c.ServeJSON()
		return
	}
	c.Data["json"] = workplace.WorkplaceInfo{Code: 200, Message:"Success"}
	c.ServeJSON()
}

func (c *UserController) Get_Workplace() {
	form := workplace.GetWorkplaceForm{}
	if err := c.ParseForm(&form); err != nil {
		beego.Debug("ParseGetWorkplace :", err)
		c.Data["json"] = common.NewErrorInfo(ErrInputData)
		c.ServeJSON()
		return
	}
	beego.Debug("ParseGetWorkplaceForm : ", &form)

	u_workplace := workplace.WorkPlace{}

	if code, err := u_workplace.FindByID(form.Email); err != nil {
		beego.Error("GetWorkplaceByEmail : ", err)
		if code == common.ErrNotFound {
			c.Data["json"] = common.NewErrorInfo(ErrDupUser)
		} else {
			c.Data["json"] = common.NewErrorInfo(ErrDatabase)
		}
		c.ServeJSON()
		return
	}

	c.Data["json"] = workplace.WorkplaceInfo{Code: 200, Message: "Success", Data: &u_workplace}
	c.ServeJSON()
}

func (c *UserController) Post_social_link() {
	form := social_links.SocialLinkPostForm{}
	if err := c.ParseForm(&form); err != nil {
		beego.Debug("ParseSocialLink : ", err)
		c.Data["json"] = common.NewErrorInfo(ErrInputData)
		c.ServeJSON()
		return
	}
	beego.Debug("ParseSocialLink : ", &form)

	sosmed, err := social_links.NewSocialLinkPost(&form)
	if err != nil {
		beego.Error("NewSocialLink : ", err)
		c.Data["json"] = common.NewErrorInfo(ErrSystem)
		c.ServeJSON()
		return
	}
	beego.Debug("NewSocialLink : ", &form)

	if code, err := sosmed.Insert(); err != nil {
		beego.Error("InsertNewSocialLink : ", err)
		if code == common.ErrDupRows {
			c.Data["json"] = common.NewErrorInfo(ErrDupUser)
		} else {
			c.Data["json"] = common.NewErrorInfo(ErrDatabase)
		}
		c.ServeJSON()
		return
	}
	c.Data["json"] = workplace.WorkplaceInfo{Code: 200, Message:"Success"}
	c.ServeJSON()
}

func (c *UserController) Get_social_link() {
	form := social_links.SocialLinkGetForm{}
	if err := c.ParseForm(&form); err != nil {
		beego.Debug("ParseGetSocialLink :", err)
		c.Data["json"] = common.NewErrorInfo(ErrInputData)
		c.ServeJSON()
		return
	}
	beego.Debug("ParseGetSocialLinkForm : ", &form)

	// u_sosmed := social_links.SocialLink{}

	// code, _ := u_sosmed.FindByEmail(form.Email)


	// c.Data["json"] = social_links.SocialLinkUser{Code: 200, Message: "Success", Data: &code}
	// c.ServeJSON()
}

func (c *UserController) Add_phone() {
	form := phone.PhonePostForm{}
	if err := c.ParseForm(&form); err != nil {
		beego.Debug("ParsePhone : ", err)
		c.Data["json"] = common.NewErrorInfo(ErrInputData)
		c.ServeJSON()
		return
	}
	beego.Debug("ParsePhone : ", &form)

	phone_cell, err := phone.NewPhonePost(&form)
	if err != nil {
		beego.Error("NewPhone : ", err)
		c.Data["json"] = common.NewErrorInfo(ErrSystem)
		c.ServeJSON()
		return
	}
	beego.Debug("NewPhone : ", &form)

	if code, err := phone_cell.InsertPhone(); err != nil {
		beego.Error("InsertPhone : ", err)
		if code == common.ErrDupRows {
			c.Data["json"] = common.NewErrorInfo(ErrDupUser)
		} else {
			c.Data["json"] = common.NewErrorInfo(ErrDatabase)
		}
		c.ServeJSON()
		return
	}
	c.Data["json"] = phone.PhoneInfo{Code: 200, Message:"Success"}
	c.ServeJSON()
}

func (c *UserController) Get_phone() {
	form := phone.PhoneGetForm{}
	if err := c.ParseForm(&form); err != nil {
		beego.Debug("ParseGetPhone :", err)
		c.Data["json"] = common.NewErrorInfo(ErrInputData)
		c.ServeJSON()
		return
	}
	beego.Debug("ParseGetPhone : ", &form)

	phone_cell := phone.Phone{}

	if code, err := phone_cell.FindPhoneByEmail(form.Email); err != nil {
		beego.Error("ParseGetPhoneByEmail : ", err)
		if code == common.ErrNotFound {
			c.Data["json"] = common.NewErrorInfo(ErrDupUser)
		} else {
			c.Data["json"] = common.NewErrorInfo(ErrDatabase)
		}
		c.ServeJSON()
		return
	}

	c.Data["json"] = phone.PhoneInfo{Code: 200, Message: "Success", Data: &phone_cell}
	c.ServeJSON()
}

func (c *UserController) Post_address() {
	form := address.AddressPostForm{}
	if err := c.ParseForm(&form); err != nil {
		beego.Debug("ParseAddress :", err)
		c.Data["json"] = common.NewErrorInfo(ErrInputData)
		c.ServeJSON()
		return
	}
	beego.Debug("ParseAddress : ", &form)

	u_address, err := address.NewAddressPost(&form)
	if err != nil {
		beego.Error("NewAddress : ", err)
		c.Data["json"] = common.NewErrorInfo(ErrSystem)
		c.ServeJSON()
		return
	}
	beego.Debug("NewAddress : ", &form)

	if code, err := u_address.InsertAddress(); err != nil {
		beego.Error("InsertAddress : ", err)
		if code == common.ErrDupRows {
			c.Data["json"] = common.NewErrorInfo(ErrDupUser)
		} else {
			c.Data["json"] = common.NewErrorInfo(ErrDatabase)
		}
		c.ServeJSON()
		return
	}
	c.Data["json"] = address.AddressInfo{Code: 200, Message:"Success"}
	c.ServeJSON()
}

func (c *UserController) Get_address() {
	form := address.AddressGetForm{}
	if err := c.ParseForm(&form); err != nil {
		beego.Debug("ParseAddress : ", err)
		c.Data["json"] = common.NewErrorInfo(ErrSystem)
		c.ServeJSON()
		return
	}

	u_address := address.Address{}

	if code, err := u_address.FindByEmail(form.Email); err != nil {
		beego.Error("ParseGetPhoneByEmail : ", err)
		if code == common.ErrNotFound {
			c.Data["json"] = common.NewErrorInfo(ErrDupUser)
		} else {
			c.Data["json"] = common.NewErrorInfo(ErrDatabase)
		}
		c.ServeJSON()
		return
	}

	c.Data["json"] = address.AddressInfo{Code: 200, Message: "Success", Data: &u_address}
	c.ServeJSON()
}