package models

import(
	// "gopkg.in/mgo.v2/bson"
)

type AddressPostForm struct {
	Email    				string `form:"email"					valid:"Required;Mobile"`				
	NameCity 				string `form:"name_city"`
	TypeCity 				string `form:"type_city"`
}

type AddressGetForm struct {
	Email 					string `form:"email"`
}

type AddressInfo struct {
	Code 					int 						`json:"code"`
	Message 				string 						`json:"message"`
	Data 		 			*Address 					`json:"data"`
}