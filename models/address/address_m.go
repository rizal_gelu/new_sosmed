package models

import(
	"time"
	"new_sosmed/models/mymongo"
	common "new_sosmed/models/common"
	"gopkg.in/mgo.v2"
	"gopkg.in/mgo.v2/bson"
)

type Address struct {
	Id 						bson.ObjectId		`bson:"_id,omitempty"`
	Email 					string 				`bson:"email" json:"email"`
	NameCity 		 		string 				`json:"name_city"`
	TypeCity 				string 				`json:"type_city"`
	DateCreated				int64 				`json:"date_created"`
	DateUpdated				int64 				`json:"date_updated"`
}

func NewAddressPost(r *AddressPostForm) (u *Address, err error) {
	date_created := time.Now().UnixNano() / int64(time.Millisecond)

	address := Address{
		Email: r.Email,
		NameCity: r.NameCity,
		TypeCity: r.TypeCity,
		DateCreated: date_created,
	}
	return &address, nil
}

func (u *Address) InsertAddress() (code int, err error) {
	mConn := mymongo.Conn()
	defer mConn.Close()
	
	c := mConn.DB("").C("address_user")
	err = c.Insert(u)

	if err != nil {
		code = common.ErrDatabase
	} else {
		code = 0
	}

	return
}

func (u *Address) FindByEmail(email string) (code int, err error) {
	mConn := mymongo.Conn()
	defer mConn.Close()
	
	c := mConn.DB("").C("address_user")
	err = c.Find(bson.M{"email": email}).One(u)

	if err != nil {
		if err == mgo.ErrNotFound {
			code = common.ErrNotFound
		} else {
			code = common.ErrDatabase
		}
	} else {
		code = 0
	}
	return
}

func (u *Address) UpdateAddress(email, name_city, type_city string, date_updated int64) (code int, err error) {
	mConn := mymongo.Conn()
	defer mConn.Close()

	c := mConn.DB("").C("address_user")
	err = c.Update(bson.M{"email": email}, bson.M{"$set": bson.M{"name_city": name_city, "type_city": type_city, "dateupdated": date_updated}})

	if err != nil {
		if err == mgo.ErrNotFound {
			code = common.ErrNotFound
		} else {
			code = common.ErrDatabase
		}
	} else {
		code = 0
	}
	return
}

func (u *Address) DeleteAddress(id string) (code int, err error) {
	mConn := mymongo.Conn()
	defer mConn.Close()

	c := mConn.DB("").C("address_user")
	err = c.Remove(bson.M{"_id": bson.ObjectIdHex(id)})

	if err != nil {
		if err == mgo.ErrNotFound {
			code = common.ErrNotFound
		} else {
			code = common.ErrDatabase
		}
	} else {
		code = 0
	}
	return
}