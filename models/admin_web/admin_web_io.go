package models

import(
	// "gopkg.in/mgo.v2/bson"
)

type RockstarLoginForm struct {
	Email    				string `form:"email"					valid:"Required;Mobile"`				
	Password 				string `form:"password"`
}

type RockstarLoginInfo struct {
	Code    	 				int   				`json:"code"`
	WebAdminInfo 				*AdminWebStruct 	`json:"WebAdminInfo"`
}

type CreateAdminUserForm struct {
	Email 	 				string `form:"email" 	 				valid:"Required"`
	Password 				string `form:"password" 				valid:"Required"`
	CreateBy 				string `form:"createBy"					valid:"Required"`
}

type ActivationUserAdminWithCodeForm struct {
	Email 					string 		`form:"email" 					valid:"Required"`
	Activation_code 		string 		`form:"activation_code"  		valid:"Required"`
}

type ActivateUserAdminForm struct {
	Email 					string 		`form:"email" 					valid:"Required"`
}

type ActivateUserAdminResponse struct {
	Code 					int 		`json:"code"`
	Message 				string 		`json:"message"`
}

type DeactiveUserAdminForm struct {
	Email 					string 		`form:"email"					valid:"Required"`
}

type DeleteAdminUserByIdForm struct {
	Id        				string 						`form:"id"`
}