package models

import(
	"time"
	"crypto/rand"
	"fmt"
	"io"
	"golang.org/x/crypto/scrypt"

	"new_sosmed/models/mymongo"
	common "new_sosmed/models/common"
	"new_sosmed/library"
	"gopkg.in/mgo.v2"
	"gopkg.in/mgo.v2/bson"
)

type AdminWebStruct struct {
	Email 					string 				`bson:"_id" 					json:"email, omitempty"`
	Password 				string 				`bson:"password" 				json:"password, omitempty"`
	Salt 					string 				`bson:"salt" 					json:"salt, omitempty"`
	RegDate 				int64 				`bson:"reg_date"				json:"reg_date, omitempty"`
	Status 					int 				`bson:"status" 					json:"status, omitempty"`
	CreateBy 			 	string 				`bson:"createBy" 				json:"createBy, omitempty"`
	Activation_code 		string 				`bson:"activation_code" 		json:"activation_code"`
	Forgot_password_code 	string 				`bson:"forgot_password_code" 	json:"forgot_password_code, omitempty"`
	Forgot_password_create 	int64 				`bson:"forgot_password_create" 	json:"forgot_password_create, omitempty"`
}

const pwHashBytes = 64

func generateSalt() (salt string, err error) {
	buf := make([]byte, pwHashBytes)
	if _, err := io.ReadFull(rand.Reader, buf); err != nil {
		return "", err
	}
	return fmt.Sprintf("%x", buf), nil
}

func generatePassHash(password string, salt string) (hash string, err error) {
	h, err := scrypt.Key([]byte(password), []byte(salt), 16384, 8, 1, pwHashBytes)
	if err != nil {
		return "", err
	}

	return fmt.Sprintf("%x", h), nil
}

func NewAdminUser(r *CreateAdminUserForm, t time.Time) (u *AdminWebStruct, err error) {
	salt, err := generateSalt()
	if err != nil {
		return nil, err
	}

	hash, err := generatePassHash(r.Password, salt) 
	if err != nil {
		return nil, err
	}

	reg_date := time.Now().UnixNano() / int64(time.Millisecond)

	userAdmin := AdminWebStruct{
		Email : r.Email,
		Password: hash,
		Salt:     salt,
		RegDate:  reg_date,
		Activation_code : library.RandomString(40),
		Status: 0,
		CreateBy : r.CreateBy,
	}

	return &userAdmin, nil
}

func (u *AdminWebStruct) InsertAdminUser() (code int, err error) {
	mConn := mymongo.Conn()
	defer mConn.Close()

	c := mConn.DB("").C("admin_web")
	err = c.Insert(u)

	if err != nil {
		if mgo.IsDup(err) {
			code = common.ErrDupRows
		} else {
			code = common.ErrDatabase
		}
	} else {
		code = 0
	}
	return
}

func (u *AdminWebStruct) FindAdminWebByID(id string) (code int, err error) {
	mConn := mymongo.Conn()
	defer mConn.Close()

	c := mConn.DB("").C("admin_web")
	err = c.Find(bson.M{"_id": id, "status": 1}).One(u)

	if err != nil {
		if err == mgo.ErrNotFound {
			code = common.ErrNotFound
		} else {
			code = common.ErrDatabase
		}
	} else {
		code = 0
	}
	return
}

func (u *AdminWebStruct) CheckPass(pass string) (ok bool, err error) {
	hash, err := generatePassHash(pass, u.Salt)
	if err != nil {
		return false, err
	}

	return u.Password == hash, nil
}

func (u *AdminWebStruct) ClearPass() {
	u.Password = ""
	u.Salt = ""
}

func (u * AdminWebStruct) DeleteAdminUser(id string) (code int, err error) {
	mConn := mymongo.Conn()
	defer mConn.Close()

	c := mConn.DB("").C("admin_web")
	err = c.Remove(bson.M{"_id": id})

	if err != nil {
		if err == mgo.ErrNotFound {
			code = common.ErrNotFound
		} else {
			code = common.ErrDatabase
		}
	} else {
		code = 0
	}
	return
}

func ActivationAdminUserWithCode(id, activation_code string) (code int, err error) {
	mConn := mymongo.Conn()
	defer mConn.Close()

	c := mConn.DB("").C("admin_web")
	u := AdminWebStruct{}
	err = c.Find(bson.M{"_id": id, "activation_code": activation_code}).One(&u)

	if err != nil {
		if err == mgo.ErrNotFound {
			return common.ErrNotFound, err
		}
		return common.ErrDatabase, err
	}

	err = c.Update(bson.M{"_id": id, "status": 0}, bson.M{"$set": bson.M{"status": 1}})
	if err != nil {
		if err == mgo.ErrNotFound {
			return common.ErrNotFound, err
		}

		return common.ErrDatabase, err
	}

	return 0, nil
}

func ActivateUser(id string) (code int, err error) {
	mConn := mymongo.Conn()
	defer mConn.Close()

	c := mConn.DB("").C("admin_web")
	u := AdminWebStruct{}
	err = c.Find(bson.M{"_id": id}).One(&u)

	if err != nil {
		if err == mgo.ErrNotFound {
			return common.ErrNotFound, err
		}
		return common.ErrDatabase, err
	}

	err = c.Update(bson.M{"_id": id, "status": 0}, bson.M{"$set": bson.M{"status": 1}})
	if err != nil {
		if err == mgo.ErrNotFound {
			return common.ErrNotFound, err
		}

		return common.ErrDatabase, err
	}

	return 0, nil
}

func DeactivationAdminUser(id string) (code int, err error) {
	mConn := mymongo.Conn()
	defer mConn.Close()

	c := mConn.DB("").C("admin_web")
	u := AdminWebStruct{}
	err = c.Find(bson.M{"_id": id}).One(&u)

	if err != nil {
		if err == mgo.ErrNotFound {
			return common.ErrNotFound, err
		}
		return common.ErrDatabase, err
	}

	err = c.Update(bson.M{"_id": id, "status": 1}, bson.M{"$set": bson.M{"status": 0}})
	if err != nil {
		if err == mgo.ErrNotFound {
			return common.ErrNotFound, err
		}

		return common.ErrDatabase, err
	}

	return 0, nil
}