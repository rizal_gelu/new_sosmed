package models

import(
)

type CreateAllowancePostForm struct {
	CompanyId 	 			string 						`form:"company_id"`
	NameAllowance 			string 						`form:"name_allowance"`
	Description 			string 						`form:"description"`
	Nominal 				int64 						`form:"nominal"`
	CreateBy 				string 						`form:"create_by"` // Input email
	UpdateBy 				string 						`form:"update_by"` // Input Email
	DateCreated  			int64 						`form:"date_created"`
	DateUpdated 			int64 						`form:"date_update"`
}

type UpdateAllowanceCompanyForm struct {
	Id 						string 						`form:"id"`
	CompanyId 	 			string 						`form:"company_id"`
	NameAllowance 			string 						`form:"name_allowance"`
	Description 			string 						`form:"description"`
	Nominal 				int64 						`form:"nominal"`
	CreateBy 				string 						`form:"create_by"` // Input email
	UpdateBy 				string 						`form:"update_by"` // Input Email
	DateCreated  			int64 						`form:"date_created"`
	DateUpdated 			int64 						`form:"date_update"`
}

type GetAllowanceByIdForm struct {
	Id 						string 						`form:"id"`
}

type GetAllowanceByCompanyId struct {
	CompanyId 				string 						`form:"company_id"`
}

type AllowanceInfo struct {
	Code    	 			int   			`json:"code"`
	Message 				string 			`json:"message"`
	Data 		 			*Allowance 		`json:"data"`
}

type AllowanceByCompanyIDInfo struct {
	Code 					int 						`json:"code"`
	Message 				string 						`json:"message"`
	Data 		 			*[]Allowance  				`json:"data"`
}