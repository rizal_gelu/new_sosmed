package models

import (
	// "fmt"
	"time"
	"new_sosmed/models/mymongo"
	common "new_sosmed/models/common"

	"gopkg.in/mgo.v2"
	"gopkg.in/mgo.v2/bson"
)

type Allowance struct {
	Id 						bson.ObjectId 				`bson:"_id,omitempty"`
	CompanyId 	 			string 						`json:"company_id"`
	NameAllowance 			string 						`json:"name_allowance"`
	Description 			string 						`json:"description"`
	Nominal 				int64 						`json:"nominal"`
	CreateBy 				string 						`json:"create_by"` // Input email
	UpdateBy 				string 						`json:"update_by"` // Input Email
	DateCreated  			int64 						`json:"date_created"`
	DateUpdated 			int64 						`json:"date_update"`
}

func NewAllowancePost(r *CreateAllowancePostForm) (u *Allowance, err error) {
	date_created := time.Now().UnixNano() / int64(time.Millisecond)

	allowance := Allowance {
		CompanyId: r.CompanyId,
		NameAllowance: r.NameAllowance,
		Description: r.Description,
		Nominal: r.Nominal,
		CreateBy: r.CreateBy,
		DateCreated: date_created,
	}

	return &allowance, nil
}

func (u *Allowance) InsertAllowance() (code int, err error) {
	mConn := mymongo.Conn()
	defer mConn.Close()

	c := mConn.DB("").C("allowance")
	err = c.Insert(u)

	if err != nil {
		code = common.ErrDatabase
	} else {
		code = 0
	}

	return
}

func (u *Allowance) GetAllowanceById(id string) (code int, err error) {
	mConn := mymongo.Conn()
	defer mConn.Close()

	c := mConn.DB("").C("allowance")
	err = c.Find(bson.M{"_id": bson.ObjectIdHex(id)}).One(u)

	if err != nil {
		if err == mgo.ErrNotFound {
			code = common.ErrNotFound
		} else {
			code = common.ErrDatabase
		}
	} else {
		code = 0
	}
	return
}

func (u *Allowance) GetAllowanceByCompanyId(company_id string) ([]Allowance, error) {
	mConn := mymongo.Conn()
	defer mConn.Close()

	item := []Allowance{}
	c := mConn.DB("").C("allowance")
	err := c.Find(bson.M{"companyid": company_id}).All(&item)
	
	return item, err
}

// func (u *Company) UpdateCompany(id, name_company, format_date, format_time, report_range, deduct_every string, date_updated int64) (code int, err error) {
// 	mConn := mymongo.Conn()
// 	defer mConn.Close()

// 	c := mConn.DB("").C("company")
// 	err = c.Find(bson.M{"_id": bson.ObjectIdHex(id)}).One(u)

// 	if err != nil {
// 		if err == mgo.ErrNotFound {
// 			return common.ErrNotFound, err
// 		}
// 		return common.ErrDatabase, err
// 	}

// 	err = c.Update(bson.M{"_id": bson.ObjectIdHex(id)}, bson.M{"$set": bson.M{"name_company": name_company, "format_date": format_date, "format_time": format_time, "report_range": report_range, "deduct_every": deduct_every, "date_updated": date_updated}})
// 	if err != nil {
// 		if err == mgo.ErrNotFound {
// 			return common.ErrNotFound, err
// 		}

// 		return common.ErrDatabase, err
// 	}

// 	return 0, nil
// }