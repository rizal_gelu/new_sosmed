package models

import(
	// "gopkg.in/mgo.v2/bson"
)

type BioPostForm struct {
	Email    				string `form:"email"					valid:"Required;Mobile"`				
	Bio 					string `form:"bio"`
}

type BioGetForm struct {
	Email 					string `form:"email"`
}

type BioInfo struct {
	Code 					int 						`json:"code"`
	Message 				string 						`json:"message"`
	Bio 		 			*Bio 						`json:"data_bio"`
}