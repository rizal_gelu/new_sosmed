package models

import(
	// "fmt"
	"time"
	"new_sosmed/models/mymongo"
	common "new_sosmed/models/common"
	// profiles "new_sosmed/models/profiles"
	// comments "new_sosmed/models/comments"
	"gopkg.in/mgo.v2"
	"gopkg.in/mgo.v2/bson"
)

type Bio struct {
	Id 						bson.ObjectId		`bson:"_id,omitempty"`
	Email 					string 				`bson:"email" json:"email"`
	Bio 		 			string 				`json:"bio"`
	DateCreated				int64 				`json:"date_created"`
	DateUpdated				int64 				`json:"date_updated"`
}

func NewBioPost(r *BioPostForm) (u *Bio, err error) {
	date_created := time.Now().UnixNano() / int64(time.Millisecond)

	bio := Bio{
		Email: r.Email,
		Bio: r.Bio,
		DateCreated: date_created,
	}
	return &bio, nil
}

func (u *Bio) Insert() (code int, err error) {
	mConn := mymongo.Conn()
	defer mConn.Close()
	
	c := mConn.DB("").C("bio")
	err = c.Insert(u)

	if err != nil {
		code = common.ErrDatabase
	} else {
		code = 0
	}

	return
}

// func  GetAllTimelines() ([]Bio, error) {
// 	mConn := mymongo.Conn()
// 	defer mConn.Close()

// 	c := mConn.DB("").C("timelines") 

// 	var timeline []Timeline
// 	err := c.Find(bson.M{}).All(&timeline)
// 	return timeline, err
// }

func (u *Bio) FindByID(email string) (code int, err error) {
	mConn := mymongo.Conn()
	defer mConn.Close()
	
	c := mConn.DB("").C("bio")
	err = c.Find(bson.M{"email": email}).One(u)

	if err != nil {
		if err == mgo.ErrNotFound {
			code = common.ErrNotFound
		} else {
			code = common.ErrDatabase
		}
	} else {
		code = 0
	}
	return
}

// func (u *Timeline) FindByEmail(email string) ([]Timeline, error) {
// 	mConn := mymongo.Conn()
// 	defer mConn.Close()

// 	item := []Timeline{}
// 	c := mConn.DB("").C("timelines")
// 	err := c.Find(bson.M{"email": email}).All(&item)
	
// 	return item, err
// }

func (u *Bio) UpdateBio(email, bio string, date_updated int64) (code int, err error) {
	mConn := mymongo.Conn()
	defer mConn.Close()

	c := mConn.DB("").C("timelines")
	err = c.Update(bson.M{"email": email}, bson.M{"$set": bson.M{"bio": bio, "dateupdated": date_updated}})

	if err != nil {
		if err == mgo.ErrNotFound {
			code = common.ErrNotFound
		} else {
			code = common.ErrDatabase
		}
	} else {
		code = 0
	}
	return
}

func (u * Bio) DeleteBio(id string) (code int, err error) {
	mConn := mymongo.Conn()
	defer mConn.Close()

	c := mConn.DB("").C("timelines")
	err = c.Remove(bson.M{"_id": bson.ObjectIdHex(id)})

	if err != nil {
		if err == mgo.ErrNotFound {
			code = common.ErrNotFound
		} else {
			code = common.ErrDatabase
		}
	} else {
		code = 0
	}
	return
}