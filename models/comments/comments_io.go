package models

import(
	// "gopkg.in/mgo.v2/bson"
)

type CommentsPostForm struct {
	Email 					string 		`form:"email"`
	Id_timeline 			string 		`form:"id_timeline"`
	File 					string 		`form:"file"`
	DateCreated  			string 		`form:"date_created"`
	Comment 				string 		`form:"comment"`
}

type UpdateCommentForm struct {
	Id 						string 		`form:"id"`
	Id_timeline 			string 		`form:"id_timeline"`
	File 					string 		`form:"file"`
	DateUpdated 			int64 		`form:"date_updated"`
	Comment 				string 		`form:"comment"`
}

type DeleteCommentForm struct {
	Id 						string 		`form:"id"`
}

type ComentsReturnResponse struct {
	Code    	 			int   			`json:"code"`
	Message 				string 			`json:"message"`
	CommentInfo 			*Comments 		`json:"data_comment"`
}