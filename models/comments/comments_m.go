package models

import (
	// "fmt"
	"time"
	"new_sosmed/models/mymongo"
	common "new_sosmed/models/common"

	"gopkg.in/mgo.v2"
	"gopkg.in/mgo.v2/bson"
)

type Comments struct {
	Id 					bson.ObjectId 			`bson:"_id,omitempty"`
	Email				string					`json:"email"`
	Id_timeline		 	string 					`json:"id_timeline" bson:"id_timeline" valid:"alphanum,printableascii"`
	DateCreated			int64 					`json:"date_created"`
	DateUpdated 		int64 					`json:"date_updated"`
	Comment 			string 					`json:"comments"`
	File 				string 					`json:"file"`
}

func NewCommentsPost(r *CommentsPostForm, file string) (u *Comments, err error) {
	date_created := time.Now().UnixNano() / int64(time.Millisecond)

	comments := Comments {
		Id_timeline: r.Id_timeline,
		Email: r.Email,
		Comment: r.Comment,
		File: file,
		DateCreated: date_created,
	}

	return &comments, nil
}

func (u *Comments) InsertComment() (code int, err error) {
	mConn := mymongo.Conn()
	defer mConn.Close()

	c := mConn.DB("").C("comments")
	err = c.Insert(u)

	if err != nil {
		code = common.ErrDatabase
	} else {
		code = 0
	}

	return
}

func (u *Comments) GetCommentByIdTimeline(id_timeline string) ([]Comments, error) {
	mConn := mymongo.Conn()
	defer mConn.Close()

	item := []Comments{}
	c := mConn.DB("").C("comments")
	err := c.Find(bson.M{"id_timeline": id_timeline}).All(&item)
	
	return item, err
}

func (u *Comments) GetCommentById(id string) (code int, err error) {
	mConn := mymongo.Conn()
	defer mConn.Close()
	// fmt.Println(bson.ObjectIdHex(id))
	c := mConn.DB("").C("comments")
	err = c.Find(bson.M{"_id": bson.ObjectIdHex(id)}).One(u)

	if err != nil {
		if err == mgo.ErrNotFound {
			code = common.ErrNotFound
		} else {
			code = common.ErrDatabase
		}
	} else {
		code = 0
	}
	return
}

func (u *Comments) UpdateComment(id, id_timeline, file, comment string, date_updated int64) (code int, err error) {
	mConn := mymongo.Conn()
	defer mConn.Close()

	c := mConn.DB("").C("comments")
	err = c.Update(bson.M{"_id": bson.ObjectIdHex(id)}, bson.M{"$set": bson.M{"id_timeline": id_timeline, "file": file, "comment": comment, "dateupdated": date_updated}})

	if err != nil {
		if err == mgo.ErrNotFound {
			code = common.ErrNotFound
		} else {
			code = common.ErrDatabase
		}
	} else {
		code = 0
	}
	return
}

func (u *Comments) DeleteComment(id string) (code int, err error) {
	mConn := mymongo.Conn()
	defer mConn.Close()

	c := mConn.DB("").C("comments")
	err = c.Remove(bson.M{"_id": bson.ObjectIdHex(id)})

	if err != nil {
		if err == mgo.ErrNotFound {
			code = common.ErrNotFound
		} else {
			code = common.ErrDatabase
		}
	} else {
		code = 0
	}
	return
}