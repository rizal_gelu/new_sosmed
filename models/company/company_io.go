package models

import(
	// "gopkg.in/mgo.v2/bson"
)

type CreateCompanyPostForm struct {
	Email 					string 						`form:"email"`
	NameCompany 			string 						`form:"name_company"`
	FormatDate 				string 						`form:"format_date"`
	FormatTime 				string 						`form:"format_time"`
	ReportRange				string 						`form:"report_range"`
	DeductEvery 			string 						`form:"deduct_every"`
	Location 				string 						`form:"location"`
	DateCreated  			int64 						`form:"date_created"`
	DateUpdated 			int64 						`form:"date_update"`
}

type UpdateSettingCompanyForm struct {
	Id 						string 						`form:"id"`
	NameCompany 			string 						`form:"name_company"`
	FormatDate 				string 						`form:"format_date"`
	FormatTime 				string 						`form:"format_time"`
	ReportRange				string 						`form:"report_range"`
	DeductEvery 			string 						`form:"deduct_every"`
	Location 				string 						`form:"location"`
	DateUpdated 			int64 						`form:"date_update"`
}

type GetCompanyByIdForm struct {
	Id 						string 						`form:"id"`
}

type CompanySingleInfo struct {
	Code 					int 			`json:"code"`
	Message 				string 			`json:"message"`
	Data 					*Company 		`json:"data"`
}

type CompanyInfo struct {
	Code    	 			int   			`json:"code"`
	Message 				string 			`json:"message"`
	Data 		 			*Company 		`json:"data"`
}

// type DeleteCommentForm struct {
// 	Id 						string 		`form:"id"`
// }

// type ComentsReturnResponse struct {
	// Code    	 			int   			`json:"code"`
	// Message 				string 			`json:"message"`
	// CommentInfo 			*Comments 		`json:"data_comment"`
// }