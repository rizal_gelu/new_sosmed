package models

import (
	// "fmt"
	"time"
	"new_sosmed/models/mymongo"
	common "new_sosmed/models/common"

	"gopkg.in/mgo.v2"
	"gopkg.in/mgo.v2/bson"
)

type Company struct {
	Id 						bson.ObjectId 				`bson:"_id,omitempty"`
	Email 					string 						`json:"email"`
	NameCompany 			string 						`json:"name_company"`
	FormatDate 				string 						`json:"format_date"`
	FormatTime 				string 						`json:"format_time"`
	ReportRange				string 						`json:"report_range"`
	DeductEvery 			string 						`json:"deduct_every"`
	Location				string 						`json:"location"`
	DateCreated  			int64 						`json:"date_created"`
	DateUpdated 			int64 						`json:"date_update"`
}

func NewCompanyPost(r *CreateCompanyPostForm) (u *Company, err error) {
	date_created := time.Now().UnixNano() / int64(time.Millisecond)

	company := Company {
		Email: r.Email,
		NameCompany: r.NameCompany,
		FormatDate: r.FormatDate,
		FormatTime: r.FormatTime,
		ReportRange: r.ReportRange,
		Location: r.Location,
		DeductEvery: r.DeductEvery,
		DateCreated: date_created,
	}

	return &company, nil
}

func (u *Company) InsertCompany() (code int, err error) {
	mConn := mymongo.Conn()
	defer mConn.Close()

	c := mConn.DB("").C("company")
	err = c.Insert(u)

	if err != nil {
		code = common.ErrDatabase
	} else {
		code = 0
	}

	return
}

func (u *Company) GetCompanyById(id string) (code int, err error) {
	mConn := mymongo.Conn()
	defer mConn.Close()

	c := mConn.DB("").C("company")
	err = c.Find(bson.M{"_id": bson.ObjectIdHex(id)}).One(u)

	if err != nil {
		if err == mgo.ErrNotFound {
			code = common.ErrNotFound
		} else {
			code = common.ErrDatabase
		}
	} else {
		code = 0
	}
	return
}

func (u *Company) UpdateCompany(id, name_company, format_date, format_time, report_range, deduct_every string, date_updated int64) (code int, err error) {
	mConn := mymongo.Conn()
	defer mConn.Close()

	c := mConn.DB("").C("company")
	err = c.Find(bson.M{"_id": bson.ObjectIdHex(id)}).One(u)

	if err != nil {
		if err == mgo.ErrNotFound {
			return common.ErrNotFound, err
		}
		return common.ErrDatabase, err
	}

	err = c.Update(bson.M{"_id": bson.ObjectIdHex(id)}, bson.M{"$set": bson.M{"name_company": name_company, "format_date": format_date, "format_time": format_time, "report_range": report_range, "deduct_every": deduct_every, "date_updated": date_updated}})
	if err != nil {
		if err == mgo.ErrNotFound {
			return common.ErrNotFound, err
		}

		return common.ErrDatabase, err
	}

	return 0, nil
}

// func (u *Comments) DeleteComment(id string) (code int, err error) {
// 	mConn := mymongo.Conn()
// 	defer mConn.Close()

// 	c := mConn.DB("").C("comments")
// 	err = c.Remove(bson.M{"_id": bson.ObjectIdHex(id)})

// 	if err != nil {
// 		if err == mgo.ErrNotFound {
// 			code = common.ErrNotFound
// 		} else {
// 			code = common.ErrDatabase
// 		}
// 	} else {
// 		code = 0
// 	}
// 	return
// }