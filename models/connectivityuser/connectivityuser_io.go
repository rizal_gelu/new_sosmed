package models

import (

)

type FollowsPostForm struct {
	EmailForm 				string 								`form:"email_from"				valid:"Required"`
	EmailTo 				string 								`form:"email_to"				valid:"Required"`
	Status 					int 								`form:"status"					valid:"Required"`
	Blocked 				int 								`form:"blocked" 				valid:"Required"`
}

type UnfollowPostForm struct {
	EmailForm 				string 								`form:"email_from"				valid:"Required"`
	EmailTo 				string 								`form:"email_to"				valid:"Required"`
}

type CountFollowingPostForm struct {
	EmailForm 				string 								`form:"emailfrom"				valid:"Required"`		
}

type ListFollowingForm struct {
	EmailForm 				string 								`form:"email_from"`
}

type GetListFollowingResponse struct {
	Code 					int 								`json:"code"`
	Message 				string 								`json:"message"`
	Data 					[]map[string]interface{} 			`json:"list_following"`
}

type GetListFollowersResponse struct {
	Code 					int 								`json:"code"`
	Message 				string 								`json:"message"`
	Data 					[]map[string]interface{} 			`json:"list_followers"`
}

type ListFollowersForm struct {
	EmailTo 				string 								`form:"email_to"`
}

type ResponseFollow struct {
	Code 					int 								`json:"code"`
	Message 				string 								`json:"message"`
}

type ResponseUnFollow struct {
	Code 					int 								`json:"code"`
	Message 				string 								`json:"message"`
}

type BlockedUserForm struct {
	EmailForm 				string 								`form:"email_from" 				valid:"Required"`
	EmailTo 				string 								`form:"email_to"				valid:"Required"`
}

type UnBlockedUserForm struct {
	EmailTo 				string 								`form:"email_to" 				valid:"Required"`
	EmailFrom 				string 								`form:"email_from" 				valid:"Required"`
}

type ResponseBlockedUser struct {
	Code 					int 								`json:"code"`
	Message 				string 								`json:"message"`
}

type ResponseUnBlockedUser struct {
	Code 					int 								`json:"code"`
	Message 				string 								`json:"message"`
}