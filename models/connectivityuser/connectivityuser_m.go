package models

import (
	// "fmt"
	"time"
	"new_sosmed/models/mymongo"
	common "new_sosmed/models/common"

	"gopkg.in/mgo.v2"
	"gopkg.in/mgo.v2/bson"
)

type Follows struct {
	Id 				bson.ObjectId 				`bson:"_id,omitempty"`
	EmailFrom 		string 						`json:"email_from"`
	EmailTo 		string 						`json:"email_to"`
	DateCreated 	int64 						`json:"date_created"`
	Status			int 						`json:"status"`
	Blocked 		int 						`json:"blocked"`
	DateUpdated 	int64 						`json:"date_updated"`
}

func NewFollowPost(r *FollowsPostForm) (u *Follows, err error) {
	date_created := time.Now().UnixNano() / int64(time.Millisecond)

	follows := Follows {
		EmailFrom : r.EmailForm,
		EmailTo : r.EmailTo,
		DateCreated : date_created,
		Status : r.Status,
		Blocked : r.Blocked,
	}

	return &follows, nil
}

func (u *Follows) Insert() (code int, err error) {
	mConn := mymongo.Conn()
	defer mConn.Close()

	c := mConn.DB("").C("follows")
		err = c.Insert(u)

	if err != nil {
		if mgo.IsDup(err) {
			code = common.ErrDupRows
		} else {
			code = common.ErrDatabase
		}
	} else {
		code = 0
	}
	return
}

func (u *Follows) UnFollow(email_from, email_to string) (code int, err error) {
	mConn := mymongo.Conn()
	defer mConn.Close()

	c := mConn.DB("").C("follows")
	err = c.Remove(bson.M{"emailfrom": email_from, "emailto": email_to})

	if err != nil {
		if err == mgo.ErrNotFound {
			code = common.ErrNotFound
		} else {
			code = common.ErrDatabase
		}
	} else {
		code = 0
	}
	return
}

func (u *Follows) CheckingStatusFollow(email_from, email_to string) (code int, err error) {
	mConn := mymongo.Conn()
	defer mConn.Close()
	c := mConn.DB("").C("follows")

	err = c.Find(bson.M{"emailfrom": email_from, "emailto": email_to}).One(u)
	if err != nil {
		if err == mgo.ErrNotFound {
			code = common.ErrNotFound
		} else {
			code = common.ErrDatabase
		}
	} else {
		code = 0
	}
	return
}

func (u *Follows) GetListFollowing(emailfrom string) ([]Follows, error) {
	mConn := mymongo.Conn()
	defer mConn.Close()

	item := []Follows{}
	c := mConn.DB("").C("follows")

	err := c.Find(bson.M{"emailfrom": emailfrom}).All(&item)
	return item, err
}

func (u *Follows) GetListFollowers(emailto string) ([]Follows, error) {
	mConn := mymongo.Conn()
	defer mConn.Close()

	item := []Follows{}
	c := mConn.DB("").C("follows")
	
	err := c.Find(bson.M{"emailto": emailto}).All(&item)
	return item, err
}

func (u *Follows) BlockedUser() (code int, err error) {
	mConn := mymongo.Conn()
	defer mConn.Close()

	c := mConn.DB("").C("follows")
	err = c.Insert(u)

	if err != nil {
		if mgo.IsDup(err) {
			code = common.ErrDupRows
		} else {
			code = common.ErrDatabase
		}
	} else {
		code = 0
	}
	return
}

func (u *Follows) UnBlockUser(emailfrom, emailto string) (code int, err error) {
	mConn := mymongo.Conn()
	defer mConn.Close()

	c := mConn.DB("").C("follows")
	checkPosition := c.Find(bson.M{"emailfrom": emailfrom, "emailto": emailto}).One(u)
	if checkPosition != nil {

		// loopSingleData := make([]map[string]interface{}, 0)
		// for _, v := range checkPosition {
		// 	loopData := make(map[string]interface{})
		// 	loopData["emailto"] 	= v.EmailTo
		// 	loopData["emailfrom"] 	= v.EmailFrom

		// 	loopSingleData = append(loopSingleData, loopData)
		// }
		
		// fmt.Println(loopSingleData)
	} else {
		err = c.Insert(u)
		if err != nil {
			if mgo.IsDup(err) {
				code = common.ErrDupRows
			} else {
				code = common.ErrDatabase
			}
		}

		code = 0
	}
	return
}
