package models

import(
	// "gopkg.in/mgo.v2/bson"
)

type CreateDivisinPostForm struct {
	CompanyId 	 			string 						`form:"company_id"`
	NameDivision 			string 						`form:"name_division"`
	Description 			string 						`form:"description"`
	CreateBy 				string 						`form:"create_by"` // Input email
	UpdateBy 				string 						`form:"update_by"` // Input Email
	DateCreated  			int64 						`form:"date_created"`
	DateUpdated 			int64 						`form:"date_update"`
}

type UpdateSettingCompanyForm struct {
	Id 						string 						`form:"id"`
	NameCompany 			string 						`form:"name_company"`
	FormatDate 				string 						`form:"format_date"`
	FormatTime 				string 						`form:"format_time"`
	ReportRange				string 						`form:"report_range"`
	DeductEvery 			string 						`form:"deduct_every"`
	Location 				string 						`form:"location"`
	DateUpdated 			int64 						`form:"date_update"`
}

type GetDevisionByIdForm struct {
	Id 						string 						`form:"id"`
}

type GetDevisionByCompanyIdForm struct {
	CompanyId 				string 						`form:"company_id"`
}

type DivisionInfo struct {
	Code    	 			int   			`json:"code"`
	Message 				string 			`json:"message"`
	Data 		 			*Devision 		`json:"data"`
}

type DivisionByCompanyIDInfo struct {
	Code 					int 						`json:"code"`
	Message 				string 						`json:"message"`
	Data 		 			*[]Devision  				`json:"data"`
}

// type DeleteCommentForm struct {
// 	Id 						string 		`form:"id"`
// }

// type ComentsReturnResponse struct {
	// Code    	 			int   			`json:"code"`
	// Message 				string 			`json:"message"`
	// CommentInfo 			*Comments 		`json:"data_comment"`
// }