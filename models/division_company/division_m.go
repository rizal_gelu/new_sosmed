package models

import (
	// "fmt"
	"time"
	"new_sosmed/models/mymongo"
	common "new_sosmed/models/common"

	"gopkg.in/mgo.v2"
	"gopkg.in/mgo.v2/bson"
)

type Devision struct {
	Id 						bson.ObjectId 				`bson:"_id,omitempty"`
	CompanyId 	 			string 						`json:"company_id"`
	NameDivision 			string 						`json:"name_division"`
	Description 			string 						`json:"description"`
	CreateBy 				string 						`json:"create_by"` // Input email
	UpdateBy 				string 						`json:"update_by"` // Input Email
	DateCreated  			int64 						`json:"date_created"`
	DateUpdated 			int64 						`json:"date_update"`
}

func NewDivisionPost(r *CreateDivisinPostForm) (u *Devision, err error) {
	date_created := time.Now().UnixNano() / int64(time.Millisecond)

	division := Devision {
		CompanyId: r.CompanyId,
		NameDivision: r.NameDivision,
		Description: r.Description,
		CreateBy: r.CreateBy,
		DateCreated: date_created,
	}

	return &division, nil
}

func (u *Devision) InsertDevision() (code int, err error) {
	mConn := mymongo.Conn()
	defer mConn.Close()

	c := mConn.DB("").C("division")
	err = c.Insert(u)

	if err != nil {
		code = common.ErrDatabase
	} else {
		code = 0
	}

	return
}

func (u *Devision) GetDevisionById(id string) (code int, err error) {
	mConn := mymongo.Conn()
	defer mConn.Close()

	c := mConn.DB("").C("division")
	err = c.Find(bson.M{"_id": bson.ObjectIdHex(id)}).One(u)

	if err != nil {
		if err == mgo.ErrNotFound {
			code = common.ErrNotFound
		} else {
			code = common.ErrDatabase
		}
	} else {
		code = 0
	}
	return
}

func (u *Devision) GetDevisionByCompanyId(company_id string) ([]Devision, error) {
	mConn := mymongo.Conn()
	defer mConn.Close()

	item := []Devision{}
	c := mConn.DB("").C("division")
	err := c.Find(bson.M{"companyid": company_id}).All(&item)
	
	return item, err
}

// func (u *Company) UpdateCompany(id, name_company, format_date, format_time, report_range, deduct_every string, date_updated int64) (code int, err error) {
// 	mConn := mymongo.Conn()
// 	defer mConn.Close()

// 	c := mConn.DB("").C("company")
// 	err = c.Find(bson.M{"_id": bson.ObjectIdHex(id)}).One(u)

// 	if err != nil {
// 		if err == mgo.ErrNotFound {
// 			return common.ErrNotFound, err
// 		}
// 		return common.ErrDatabase, err
// 	}

// 	err = c.Update(bson.M{"_id": bson.ObjectIdHex(id)}, bson.M{"$set": bson.M{"name_company": name_company, "format_date": format_date, "format_time": format_time, "report_range": report_range, "deduct_every": deduct_every, "date_updated": date_updated}})
// 	if err != nil {
// 		if err == mgo.ErrNotFound {
// 			return common.ErrNotFound, err
// 		}

// 		return common.ErrDatabase, err
// 	}

// 	return 0, nil
// }