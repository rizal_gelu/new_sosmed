package models

import(
	// "gopkg.in/mgo.v2/bson"
)

type PostLikeTLForm struct {
	Email 					string 		`form:"email"`
	Id_timeline 			string 		`form:"id_timeline"`
	DateCreated  			string 		`form:"date_created"`
}

type UnLikeTLform struct {
	Email 					string 		`form:"email"`
	Id_timeline 			string 		`form:"id_timeline"`
}

type GetLikeForm struct {
	Id_timeline 			string  	`form:"id_timeline"`
}

type LikeInfo struct {
	Code    	 			int   			`json:"code"`
	Message 				string 			`json:"message"`
	LikeInfo 				*[]Like 		`json:"data"`
}