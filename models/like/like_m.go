package models

import (
	"time"
	"new_sosmed/models/mymongo"
	common "new_sosmed/models/common"

	"gopkg.in/mgo.v2"
	"gopkg.in/mgo.v2/bson"
)

type Like struct {
	Id 						bson.ObjectId 				`bson:"_id,omitempty"`
	Id_timeline 			string 						`json:"id_timeline"`
	Email 					string 						`json:"email"`
	DateCreated  			int64 						`json:"date_created"`
	DateUpdated 			int64 						`json:"date_update"`
}

func NewLikesPost(r *PostLikeTLForm) (u *Like, err error) {
	date_created := time.Now().UnixNano() / int64(time.Millisecond)

	like := Like {
		Id_timeline: r.Id_timeline,
		Email: r.Email,
		DateCreated: date_created,
	}

	return &like, nil
}

func (u *Like) InsertLike() (code int, err error) {
	mConn := mymongo.Conn()
	defer mConn.Close()

	c := mConn.DB("").C("likes_tl")
	err = c.Insert(u)

	if err != nil {
		code = common.ErrDatabase
	} else {
		code = 0
	}

	return
}

func (u *Like) DeleteLike(id_timeline, email string) (code int, err error) {
	mConn := mymongo.Conn()
	defer mConn.Close()

	c := mConn.DB("").C("likes_tl")
	err = c.Remove(bson.M{"id_timeline": id_timeline,"email": email})

	if err != nil {
		if err == mgo.ErrNotFound {
			code = common.ErrNotFound
		} else {
			code = common.ErrDatabase
		}
	} else {
		code = 0
	}
	return
}

func (u *Like) GetLikeTLByIdTL(id_timeline string) ([]Like, error) {
	mConn := mymongo.Conn()
	defer mConn.Close()

	c := mConn.DB("").C("likes_tl") 

	var timeline []Like
	err := c.Find(bson.M{"id_timeline": id_timeline}).All(&timeline)
	return timeline, err
}

// func (u *Comments) GetCommentByIdTimeline(id_timeline string) ([]Comments, error) {
// 	mConn := mymongo.Conn()
// 	defer mConn.Close()

// 	item := []Comments{}
// 	c := mConn.DB("").C("comments")
// 	err := c.Find(bson.M{"id_timeline": id_timeline}).All(&item)
	
// 	return item, err
// }

// func (u *Comments) GetCommentById(id string) (code int, err error) {
// 	mConn := mymongo.Conn()
// 	defer mConn.Close()
// 	// fmt.Println(bson.ObjectIdHex(id))
// 	c := mConn.DB("").C("comments")
// 	err = c.Find(bson.M{"_id": bson.ObjectIdHex(id)}).One(u)

// 	if err != nil {
// 		if err == mgo.ErrNotFound {
// 			code = common.ErrNotFound
// 		} else {
// 			code = common.ErrDatabase
// 		}
// 	} else {
// 		code = 0
// 	}
// 	return
// }

// func (u *Comments) UpdateComment(id, id_timeline, file, comment string, date_updated int64) (code int, err error) {
// 	mConn := mymongo.Conn()
// 	defer mConn.Close()

// 	c := mConn.DB("").C("comments")
// 	err = c.Update(bson.M{"_id": bson.ObjectIdHex(id)}, bson.M{"$set": bson.M{"id_timeline": id_timeline, "file": file, "comment": comment, "dateupdated": date_updated}})

// 	if err != nil {
// 		if err == mgo.ErrNotFound {
// 			code = common.ErrNotFound
// 		} else {
// 			code = common.ErrDatabase
// 		}
// 	} else {
// 		code = 0
// 	}
// 	return
// }

// func (u *Comments) DeleteComment(id string) (code int, err error) {
// 	mConn := mymongo.Conn()
// 	defer mConn.Close()

// 	c := mConn.DB("").C("comments")
// 	err = c.Remove(bson.M{"_id": bson.ObjectIdHex(id)})

// 	if err != nil {
// 		if err == mgo.ErrNotFound {
// 			code = common.ErrNotFound
// 		} else {
// 			code = common.ErrDatabase
// 		}
// 	} else {
// 		code = 0
// 	}
// 	return
// }