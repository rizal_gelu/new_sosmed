package models

import(
	// "gopkg.in/mgo.v2/bson"
)

type OrderPostForm struct {
	Email 					string 		`form:"email"`
	Id_product 				string 		`form:"id_timeline"`
	Title 					string 		`form:"title"`
	Description				string 		`form:"description"`
	Qty 					int 		`form:"qty"`
	ItemPrice 				int 		`form:"item_price"`
	TotalPrice 				int 		`form:"total_price"`
	DateCreated  			string 		`form:"date_created"`
}

type OrderInfo struct {
	Code 					int 		`json:"code"`
	Message 				string 		`json:"message"`
	Data 					*Order 		`json:"data"`
}

type CartPostForm struct {
	CompanyName 			string 		`json:"company_name"`
	Email 					string 		`json:"email"`
	Handphone 				string 		`json:"handphone"`
	TotalPrice 				int 		`json:""`
}

// type ComentsReturnResponse struct {
// 	Code    	 			int   			`json:"code"`
// 	Message 				string 			`json:"message"`
// 	CommentInfo 			*Order 		`json:"data_comment"`
// }