package models

import (
	// "fmt"
	"time"
	"new_sosmed/models/mymongo"
	common "new_sosmed/models/common"

	// "gopkg.in/mgo.v2"
	"gopkg.in/mgo.v2/bson"
)

type Order struct {
	Id 						bson.ObjectId 				`bson:"_id,omitempty"`
	Email 					string 						`json:"email"`
	Id_product 				string 						`json:"id_product"`
	Title					string 						`json:"title"`
	Description 			string 						`json:"description"`
	ItemPrice 				int 						`json:"price_item"`
	Qty 					int 						`json:"qty"`
	TotalPrice				int 						`json:"total_price"`
	DateCreated  			int64 						`json:"date_created"`
	DateUpdated 			int64 						`json:"date_update"`
}

func NewOrderPost(r *OrderPostForm) (u *Order, err error) {
	date_created := time.Now().UnixNano() / int64(time.Millisecond)

	order := Order {
		Email: r.Email,
		Id_product: r.Id_product,
		Title: r.Title,
		Description: r.Description,
		ItemPrice: r.ItemPrice,
		Qty: r.Qty,
		TotalPrice: r.TotalPrice,
		DateCreated: date_created,
	}

	return &order, nil
}

func (u *Order) InsertOrder() (code int, err error) {
	mConn := mymongo.Conn()
	defer mConn.Close()

	c := mConn.DB("").C("order")
	err = c.Insert(u)

	if err != nil {
		code = common.ErrDatabase
	} else {
		code = 0
	}

	return
}
