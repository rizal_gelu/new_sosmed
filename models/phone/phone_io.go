package models

import(
	// "gopkg.in/mgo.v2/bson"
)

type PhonePostForm struct {
	Email    				string `form:"email"					valid:"Required;Mobile"`				
	Phone 					string `form:"phone"`
}

type PhoneGetForm struct {
	Email 					string `form:"email"`
}

type PhoneInfo struct {
	Code 					int 						`json:"code"`
	Message 				string 						`json:"message"`
	Data 		 			*Phone 						`json:"data"`
}