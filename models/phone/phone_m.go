package models

import(
	"time"
	"new_sosmed/models/mymongo"
	common "new_sosmed/models/common"
	"gopkg.in/mgo.v2"
	"gopkg.in/mgo.v2/bson"
)

type Phone struct {
	Id 						bson.ObjectId		`bson:"_id,omitempty"`
	Email 					string 				`bson:"email" json:"email"`
	Phone 		 			string 				`json:"phone"`
	DateCreated				int64 				`json:"date_created"`
	DateUpdated				int64 				`json:"date_updated"`
}

func NewPhonePost(r *PhonePostForm) (u *Phone, err error) {
	date_created := time.Now().UnixNano() / int64(time.Millisecond)

	phone := Phone{
		Email: r.Email,
		Phone: r.Phone,
		DateCreated: date_created,
	}
	return &phone, nil
}

func (u *Phone) InsertPhone() (code int, err error) {
	mConn := mymongo.Conn()
	defer mConn.Close()
	
	c := mConn.DB("").C("phone")
	err = c.Insert(u)

	if err != nil {
		code = common.ErrDatabase
	} else {
		code = 0
	}

	return
}

func (u *Phone) FindPhoneByEmail(email string) (code int, err error) {
	mConn := mymongo.Conn()
	defer mConn.Close()
	
	c := mConn.DB("").C("phone")
	err = c.Find(bson.M{"email": email}).One(u)

	if err != nil {
		if err == mgo.ErrNotFound {
			code = common.ErrNotFound
		} else {
			code = common.ErrDatabase
		}
	} else {
		code = 0
	}
	return
}

func (u *Phone) UpdatePhone(email, phone string, date_updated int64) (code int, err error) {
	mConn := mymongo.Conn()
	defer mConn.Close()

	c := mConn.DB("").C("phone")
	err = c.Update(bson.M{"email": email}, bson.M{"$set": bson.M{"phone": phone, "dateupdated": date_updated}})

	if err != nil {
		if err == mgo.ErrNotFound {
			code = common.ErrNotFound
		} else {
			code = common.ErrDatabase
		}
	} else {
		code = 0
	}
	return
}

func (u *Phone) DeletePhone(id string) (code int, err error) {
	mConn := mymongo.Conn()
	defer mConn.Close()

	c := mConn.DB("").C("phone")
	err = c.Remove(bson.M{"_id": bson.ObjectIdHex(id)})

	if err != nil {
		if err == mgo.ErrNotFound {
			code = common.ErrNotFound
		} else {
			code = common.ErrDatabase
		}
	} else {
		code = 0
	}
	return
}