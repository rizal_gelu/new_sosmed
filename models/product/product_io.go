package models

import (
	// "time"
)

type AddProductForm struct {
	NameProduct 	 				string 		`form:"name_product" 	 			valid:"Required"`
	DescriptionProduct     			string 		`form:"description_product"     	valid:"Required"`
	ImageProduct 					string 		`form:"image_product" 				valid:"Required"`
	Price 					 		int64 		`form:"price"						valid:"Required"`
}

type EditProductForm struct {
	NameProduct 	 				string 		`form:"name_product" 	 			valid:"Required"`
	DescriptionProduct     			string 		`form:"description_product"     	valid:"Required"`
	ImageProduct 					string 		`form:"image_product" 				valid:"Required"`
	Price 					 		int 		`form:"price"						valid:"Required"`
}

type DeleteProductForm struct {
	Id        				string 						`form:"id"`
}

//form user biasa 
type GetAllProductForm struct {
	Code    	 			int   			`json:"code"`
	Message 				string 			`json:"message"`
	Data 		 			*[]Product 		`json:"data"`
}