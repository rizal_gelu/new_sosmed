package models

import(
	// "fmt"
	"time"
	"new_sosmed/models/mymongo"
	common "new_sosmed/models/common"
	"gopkg.in/mgo.v2/bson"
)

type Product struct {
	Id 								bson.ObjectId		`bson:"_id,omitempty"`
	NameProduct 					string 				`form:"name_product"`
	DescriptionProduct 				string 				`form:"description_product"`
	DateCreated 					int64 				`form:"date_start"`
	DateUpdated 					int64 				`form:"date_end"`
	ImageProduct 					string 				`form:"image_product"`
	Price 							int64  				`form:"price"`
}

func NewProductPost(r *AddProductForm, image_product string) (u *Product, err error) {
	date_created := time.Now().UnixNano() / int64(time.Millisecond)

	if image_product != "" {
		product := Product{
			NameProduct: r.NameProduct,
			DescriptionProduct: r.DescriptionProduct,
			ImageProduct: image_product,
			DateCreated: date_created,
		}
		return &product, nil
	} else {
		product := Product{
			NameProduct: r.NameProduct,
			DescriptionProduct: r.DescriptionProduct,
			DateCreated: date_created,
		}
		return &product, nil
	}
}

func (u *Product) InsertProduct() (code int, err error) {
	mConn := mymongo.Conn()
	defer mConn.Close()
	
	c := mConn.DB("").C("products")
	err = c.Insert(u)

	if err != nil {
		code = common.ErrDatabase
	} else {
		code = 0
	}

	return
}

// User Biasa
func GetAllProductList() ([]Product, error) {
	mConn := mymongo.Conn()
	defer mConn.Close()

	c := mConn.DB("").C("products") 

	var product []Product
	err := c.Find(bson.M{}).All(&product)
	return product, err
}