package models

import (
	// "time"
)

type CreateEducationCollegeForm struct {
	Email 					string 				`form:"email"`
	College_name 			string 				`form:"college_name"`
	Faculty_major 			string 				`form:"faculty_major"`
	Graduated 				string 				`form:"graduated"`
	Year_graduate			string 				`form:"year_graduate"`
	Description 			string 				`form:"description"`
}

type ResponsePostEducationCollegeInfo struct {
	Code 					int 				`json:"code"`
	Message 				string 				`json:"message"`
}

type GetEducationCollegeByEmailForm struct {
	Email 					string 				`form:"email"`
}

type ResponseGetEducationCollegeInfo struct {
	Code 					int 				`json:"code"`
	Message 				string 				`json:"Email"`
	Data 					*EducationCollege 	`json:"data_education_college"`
}

type UpdateEducationCollegeForm struct {
	Email 					string 				`form:"email"`
	College_name 			string 				`form:"college_name"`
	Faculty_major 			string 				`form:"faculty_major"` 
	Graduated				string 				`form:"graduated"`
	Year_graduate			string 				`form:"year_graduate"`
	Description 			string 				`form:"description"` 						
}

type ResponseUpdateEducationCollegeInfo struct {
	Code 					int 				`json:"code"`
	Message 				string 				`json:"message"`
	Data 					*EducationCollege 	`json:"data_update_education_college"`
}

type DeleteEducationCollegeForm struct {
	Email 					string 				`form:"email"`
}

type ResponseDeleteEducationCollegeForm struct {
	Code 					int 				`json:"code"`
	Message 				string 				`json:"message"`
}