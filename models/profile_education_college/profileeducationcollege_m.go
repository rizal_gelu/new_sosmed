package models

import (
	// "time"
	mymongo "new_sosmed/models/mymongo"
	common "new_sosmed/models/common"
	"gopkg.in/mgo.v2"
	"gopkg.in/mgo.v2/bson"
)

type EducationCollege struct {
	Email 					string 				`bson:"_id"`
	College_name 			string 				`json:"college_name"`
	Faculty_major 			string 				`json:"faculty_major"`
	Graduated 				string 				`json:"graduated"`
	Year_graduate			string 				`form:"year_graduate"`
	Description 			string 				`json:"description"`
}

func NewEducationCollege(c *CreateEducationCollegeForm) (h *EducationCollege, err error) {
	college := EducationCollege {
		Email: c.Email,
		College_name: c.College_name,
		Faculty_major: c.Faculty_major,
		Graduated: c.Graduated,
		Year_graduate: c.Year_graduate,
		Description: c.Description,
	}

	return &college, nil
}

func (u *EducationCollege) Insert() (code int, err error) {
	mConn := mymongo.Conn()
	defer mConn.Close()

	c := mConn.DB("").C("education_college")
	err = c.Insert(u)

	if err != nil {
		if mgo.IsDup(err) {
			code = common.ErrDupRows
		} else {
			code = common.ErrDatabase
		}
	} else {
		code = 0
	}

	return
}

func (u *EducationCollege) FindEducationCollegeByEmail(email string) (code int, err error) {
	mConn := mymongo.Conn()
	defer mConn.Close()

	c := mConn.DB("").C("education_college")
	err = c.Find(bson.M{"_id": email}).One(u)

	if err != nil {
		if err == mgo.ErrNotFound {
			code = common.ErrNotFound
		} else {
			code = common.ErrDatabase
		}
	} else {
		code = 0
	}

	return
}

func (u *EducationCollege) UpdateEducationCollege(email, college_name, faculty_major, graduated, year_graduate, description string) (code int, err error) {
	mConn := mymongo.Conn()
	defer mConn.Close()

	c:= mConn.DB("").C("education_college")
	err = c.Find(bson.M{"_id": email}).One(u)

	if err != nil {
		if err == mgo.ErrNotFound {
			return common.ErrNotFound, err
		} 
		return common.ErrDatabase, err
	}

	err = c.Update(bson.M{"_id": email}, bson.M{"$set": bson.M{"college_name": college_name, "faculty_major": faculty_major, "graduated": graduated, "year_graduate": year_graduate, "description": description}})
	if err != nil {
		if err == mgo.ErrNotFound {
			return common.ErrNotFound, err
		}
		return common.ErrDatabase, err 
	}

	return 0, nil
}

func (u *EducationCollege) DeleteEducationCollage(email string) (code int, err error) {
	mConn := mymongo.Conn()
	defer mConn.Close()
	
	c := mConn.DB("").C("education_college")
	err = c.Remove(bson.M{"_id": email})

	if err != nil {
		if err == mgo.ErrNotFound {
			code = common.ErrNotFound
		} else {
			code = common.ErrDatabase
		}
	} else {
		code = 0
	}

	return
}