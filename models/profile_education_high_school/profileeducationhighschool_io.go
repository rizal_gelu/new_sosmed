package models

import (
	// "time"
)

type CreateEducationHighSchoolProfileForm struct {
	Email 					string 				`form:"email"`
	High_school_name 		string 				`form:"high_school_name"`
	Year_graduate  			string 				`form:"year_graduate"`
	Graduated 				string 				`form:"graduated"`
	Description 			string 				`form:"description"`
}

type GetEducationHighSchoolForm struct {
	Email 					string 				`form:"email"`
}

type ResponseGetEducationHighSchoolInfo struct {
	Code 					int 					`json:"code"`
	Message 				string 					`json:"message"`
	Data 					*EducationHighSchool 	`json:"data_education_high_school"`
}

type UpdateEducationHighSchoolForm struct {
	Email 					string 				`form:"email"`
	High_school_name 		string 				`form:"high_school_name"`
	Year_graduate  			string 				`form:"year_graduate"`
	Graduated 				string 				`form:"graduated"`
	Description 			string 				`form:"description"`
}

type ResponseUpdateEducationHighSchoolInfo struct {
	Code 					int 					`json:"code"`
	Message 				string 					`json:"message"`
	Data 					*EducationHighSchool 	`json:"data_update_education_high_school"`
}

type DeleteEducationHighSchoolForm struct {
	Email 					string 				`form:"email"`
}

type ResponseDeleteEducationHighSchool struct {
	Code 					int 				`json:"code"`
	Message 				string 				`json:"message"`
}

