package models

import (
	// "time"
	mymongo "new_sosmed/models/mymongo"
	common "new_sosmed/models/common"
	"gopkg.in/mgo.v2"
	"gopkg.in/mgo.v2/bson"
)

type EducationHighSchool struct {
	Email 				string 				`bson:"_id"`
	High_school_name 	string 				`json:"education_high_school_name, omitempty"`
	Year_graduate 		string 				`json:"year_graduate"`
	Graduated 			string 				`json:"graduated"`
	Description 		string 				`json:"description"`
}

func NewEducationHighSchool(c *CreateEducationHighSchoolProfileForm) (h *EducationHighSchool, err error) {
	high_school := EducationHighSchool {
		Email: c.Email,
		High_school_name: c.High_school_name,
		Year_graduate: c.Year_graduate,
		Graduated: c.Graduated,
		Description: c.Description,
	}

	return &high_school, nil
}

func (u *EducationHighSchool) Insert() (code int, err error) {
	mConn := mymongo.Conn()
	defer mConn.Close()

	c := mConn.DB("").C("education_high_school")
	err = c.Insert(u)

	if err != nil {
		if mgo.IsDup(err) {
			code = common.ErrDupRows
		} else {
			code = common.ErrDatabase
		}
	} else {
		code = 0
	}

	return
}

func (u *EducationHighSchool) FindEducationHighSchoolByEmail(email string) (code int, err error) {
	mConn := mymongo.Conn()
	defer mConn.Close()

	c := mConn.DB("").C("education_high_school")
	err = c.Find(bson.M{"_id": email}).One(u)

	if err != nil {
		if err == mgo.ErrNotFound {
			code = common.ErrNotFound
		} else {
			code = common.ErrDatabase
		}
	} else {
		code = 0
	}

	return
}

func (u *EducationHighSchool) UpdateEducationHighSchool(email, high_school_name, year_graduate, graduated, description string) (code int, err error) {
	mConn := mymongo.Conn()
	defer mConn.Close()

	c := mConn.DB("").C("education_high_school")
	err = c.Find(bson.M{"_id": email}).One(u)

	if err != nil {
		if err == mgo.ErrNotFound {
			return common.ErrNotFound, err
		}
		return common.ErrDatabase, err
	}

	err = c.Update(bson.M{"_id": email}, bson.M{"$set": bson.M{"high_school_name": high_school_name, "year_graduate": year_graduate, "graduated": graduated, "description": description}})
	if err != nil {
		if err == mgo.ErrNotFound {
			return common.ErrNotFound, err
		}
		return common.ErrDatabase, err
	}
	return 0, nil
}

func (u *EducationHighSchool) DeleteEducationHighSchool(email string) (code int, err error) {
	mConn := mymongo.Conn()
	defer mConn.Close()

	c := mConn.DB("").C("education_high_school")
	err = c.Remove(bson.M{"_id": email})

	if err != nil {
		if err == mgo.ErrNotFound {
			code = common.ErrNotFound
		} else {
			code = common.ErrDatabase
		}
	} else {
		code = 0
	}
	return
}