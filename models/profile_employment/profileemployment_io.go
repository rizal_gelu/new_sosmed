package models

import (
	"time"
)

type CreateEmploymentProfileForm struct {
	Email 					string 			`form:"email"`
	Company					string 			`form:"company"`
	Position 				string 			`form:"position"`
	City					string 			`form:"city"`
	Date_start 				time.Time 		`form:"date_start"`
	Date_end 				time.Time 		`form:"date_end"`
	Project_name 			string 			`form:"project_name"`
	Description 			string 			`form:"description"`
}

type GetEmploymentByEmailForm struct {
	Email 					string 			`form:"email"`
}

type ResponseGetEmploymentInfo struct {
	Code 					int 			`json:"code"`
	Message 				string 			`json:"message"`
	Data 					*EmploymentUser `json:"data_employment_user"`
}

type UpdateEmploymentForm struct {
	Email 					string 			`form:"email"`
	Company 				string 			`form:"company"`
	Position 				string 			`form:"position"`
	City 					string 			`form:"city"`
	Date_start 				string 			`form:"date_start"`
	Date_end 				string 			`form:"date_end"`
	Project_name 			string 			`form:"project_name"`
	Description 			string 			`form:"description"`
}

type ResponseUpdateEmploymentInfo struct {
	Code 					int 			`json:"code"`
	Message 				string 			`json:"message"`
	Data 					*EmploymentUser `json:"data_update_employment"`
}

type DeleteEmploymentForm struct {
	Email 					string 			`form:"email"`
}

type ResponseDeleteEmployeeInfo struct {
	Code 					int 			`json:"code"`
	Message 				string 			`json:"message"`
}