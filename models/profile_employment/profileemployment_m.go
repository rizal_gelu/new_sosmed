package models

import(
	"time"
	mymongo "new_sosmed/models/mymongo"
	common "new_sosmed/models/common"
	"gopkg.in/mgo.v2"
	"gopkg.in/mgo.v2/bson"
)

type EmploymentUser struct {
	Email 					string 			`bson:"_id"`
	Company 				string 			`json:"company, omitempty"`
	Position 				string 			`json:"position, omitempty"`
	City 					string 			`json:"city, omitempty"`
	Date_start 				time.Time 		`json:"date_start, omitempty"`
	Date_end 				time.Time 		`json:"date_end, omitempty"`
	Project_name 			string 			`json:"project_name, omitempty"`
	Description 			string 			`json:"description, omitempty"`
}

func NewEmploymentUser(c *CreateEmploymentProfileForm) (p *EmploymentUser, err error) {
	employment := EmploymentUser {
		Email: c.Email,
		Company: c.Company,
		Position: c.Position,
		City: c.City,
		Date_start: c.Date_start,
		Date_end: c.Date_end,
		Project_name: c.Project_name,
		Description: c.Description,
	}

	return &employment, nil
}

func (u *EmploymentUser) Insert() (code int, err error) {
	mConn := mymongo.Conn()
	defer mConn.Close()

	c := mConn.DB("").C("employment_user")
	err = c.Insert(u)

	if err != nil {
		if mgo.IsDup(err) {
			code = common.ErrDupRows
		} else {
			code = common.ErrDatabase
		}
	} else {
		code = 0
	}
	return
}

func (u *EmploymentUser) FindByEmail(email string) (code int, err error) {
	mConn := mymongo.Conn()
	defer mConn.Close()

	c := mConn.DB("").C("employment_user")
	err = c.Find(bson.M{"_id": email}).One(u)

	if err != nil {
		if err == mgo.ErrNotFound {
			code = common.ErrNotFound
		} else {
			code = common.ErrDatabase
		}
	} else {
		code = 0
	}
	return
}

func (u *EmploymentUser) Update(email, company, position, city, date_start, date_end, project_name, description string) (code int, err error) {
	mConn := mymongo.Conn()
	defer mConn.Close()

	c := mConn.DB("").C("employment_user")
	err = c.Find(bson.M{"_id": email}).One(u)

	if err != nil {
		if err == mgo.ErrNotFound {
			return common.ErrNotFound, err
		}
		return common.ErrDatabase, err
	}

	err = c.Update(bson.M{"_id": email}, bson.M{"$set": bson.M{"company": company, "position": position, "city": city, "date_start": date_start, "date_end": date_end, "project_name": project_name, "description": description}})
	if err != nil {
		if err == mgo.ErrNotFound {
			return common.ErrNotFound, err
		} 

		return common.ErrDatabase, err
	}

	return 0, nil
}

func (u *EmploymentUser) DeleteEploymentUser(email string) (code int, err error) {
	mConn := mymongo.Conn()
	defer mConn.Close()

	c := mConn.DB("").C("employment_user")
	err = c.Remove(bson.M{"_id": email})

	if err != nil {
		if err == mgo.ErrNotFound {
			code = common.ErrNotFound
		} else {
			code = common.ErrDatabase
		}
	} else {
		code = 0
	}
	return
}