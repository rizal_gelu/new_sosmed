package models

import (
	"time"
)

type CreateResidenceForm struct {
	Email 					string 				 	`form:"email"`
	City 					string 					`form:"city"`
	Address 				string 					`form:"address"`
	Date_create 			time.Time 				`form:"date_create"`
	Date_update 			time.Time 				`form:"date_update"`
}

type ResponsePostResidence struct {
	Code 					int 					`json:"code"`
	Message 				string 					`json:"message"`
}

type GetResidenceByEmailForm struct {
	Email 					string 					`form:"email"`
}

type ResponseGetResidenceInfo struct {
	Code 					int 					`json:"code"`
	Message 				string 					`json:"message"`
	Data 					*Residence 				`json:"data_get_residence"`
}

type UpdateResidenceForm struct {
	Email 					string 					`form:"email"`
	City 					string 					`form:"city"`
	Address 				string 					`form:"address"`
}

type ResponseUpdateResidenceInfo struct {
	Code 					int 					`json:"code"`
	Message 				string 					`json:"message"`
	Data 					*Residence 				`json:"data_update_residence"`
}

type DeleteResidenceForm struct {
	Email 					string 					`form:"email"`
}

type ResponseDeleteResidenceInfo struct {
	Code 					int 					`json:"code"`
	Message 				string 					`json:"message"`
}