package models

import (
	"time"
	mymongo "new_sosmed/models/mymongo"
	common "new_sosmed/models/common"
	"gopkg.in/mgo.v2"
	"gopkg.in/mgo.v2/bson"
)

type Residence struct {
	Email 					string 				`bson:"_id"`
	City 					string 				`json:"city"`
	Address 				string 				`json:"address"`
	Date_create 			time.Time 			`json:"date_create"`
	Date_update 			time.Time 			`json:"date_update"`
}

func NewResidence(c *CreateResidenceForm) (r *Residence, err error) {
	// reg_date := time.Now().UnixNano() / int64(time.Millisecond)
	date_create := time.Now()
	residence := Residence {
		Email: c.Email,
		City: c.City,
		Address: c.Address,
		Date_create: date_create,
	}

	return &residence, nil
}

func (r *Residence) Insert() (code int, err error) {
	mConn := mymongo.Conn()
	defer mConn.Close()

	c := mConn.DB("").C("residence")
	err = c.Insert(r)

	if err != nil {
		if mgo.IsDup(err) {
			code = common.ErrDupRows
		} else {
			code = common.ErrDatabase
		}
	} else {
		code = 0
	}

	return
}

func (r *Residence) FindResidenceByEmail(email string) (code int, err error) {
	mConn := mymongo.Conn()
	defer mConn.Close()

	c := mConn.DB("").C("residence")
	err = c.Find(bson.M{"_id": email}).One(r)

	if err != nil {
		if err == mgo.ErrNotFound{
			code = common.ErrNotFound
		} else {
			code = common.ErrDatabase
		}
	} else {
		code = 0
	}

	return
}

func (r *Residence) UpdateResidence(email, city, address string) (code int, err error) {
	mConn := mymongo.Conn()
	defer mConn.Close()

	c := mConn.DB("").C("residence")
	err = c.Find(bson.M{"_id": email}).One(r)

	date_update := time.Now()

	if err != nil {
		if err == mgo.ErrNotFound {
			return common.ErrNotFound, err
		}
		return common.ErrDatabase, err
	}

	err = c.Update(bson.M{"_id": email}, bson.M{"$set": bson.M{"city": city, "address": address, "date_update": date_update}})
	if err != nil {
		if err == mgo.ErrNotFound {
			return common.ErrNotFound, err
		}
		return common.ErrDatabase, err
	}
	return 0, nil
}

func (r *Residence) DeleteResidence(email string) (code int, err error) {
	mConn := mymongo.Conn()
	defer mConn.Close()

	c := mConn.DB("").C("residence")
	err = c.Remove(bson.M{"_id": email})

	if err != nil {
		if err == mgo.ErrNotFound {
			code = common.ErrNotFound
		} else {
			code = common.ErrDatabase
		}
	} else {
		code = 0
	}

	return
}