package models

import (
	"time"
)
	
type CreateProfileForm struct {
	Email 	 				string 		`form:"email" 	 				valid:"Required"`
	First_name     			string 		`form:"first_name"     			valid:"Required"`
	Last_name 				string 		`form:"last_name" 				valid:"Required"`
	Private_account 		int 		`form:"private_account"			valid:"Required"`
	Date_of_birth 			time.Time  	`form:"Date_of_birth"			valid:"Required"`
}

type UpdateProfileForm struct {
	Email 	 				string 		`form:"email" 	 				valid:"Required"`
	First_name 				string 		`form:"first_name" 				valid:"Required"`
	Last_name 				string 		`form:"last_name"				valid:"Required"`
	Date_of_birth 			string 		`form:"date_of_birth"			valid:"Required"`
	Avatar 					string 		`form:"avatar"					valid:"Required"`	
}

type FindProfileByEmailForm struct {
	Email 					string 		`form:"email"					valid:"Required"`
}

type SingleProfileInfo struct {
	Code    	 			int   		`json:"code"`
	Message 				string 		`json:"message"`
	UserInfo 				*Profile 	`json:"profile"`
}

type PrivateAccountActiveForm struct {
	Email 					string 		`form:"email" 					valid:"Required"`
}

type PrivateAccountDeActiveForm struct {
	Email 					string 		`form:"email"					valid:"Required"`
}

type PrivateAccountActiveResponse struct {
	Code 					int 		`json:"code"`
	Message 				string 		`json:"message"`
}

type PrivateAccountDeActiveResponse struct {
	Code 					int 		`json:"code"`
	Message 				string 		`json:"message"`
}