package models

import (
	// "fmt"
	"time"
	mymongo "new_sosmed/models/mymongo"
	common "new_sosmed/models/common"
	"gopkg.in/mgo.v2"
	"gopkg.in/mgo.v2/bson"
)

type Profile struct {
	// ID 						string 		`bson:"_id" 					json:"_id, omitempty"`
	First_name 				string 		`json:"first_name, omitempty"`
	Last_name 				string 		`json:"last_name, omitempty"`
	Avatar 					string 		`json:"avatar, omitempty"`
	Email 					string 		`bson:"_id"`
	Private_account 		int 		`json:"private_account"`
	Email_secondary			string 		`json:"email_secondary, omitempty"`
	Date_of_birth 			time.Time 	`json:"date_of_birth, omitempty"`
}

func NewProfile(c *CreateProfileForm) (p *Profile, err error) {
	profile := Profile{
		// ID: library.Uuid(),
		First_name: c.First_name,
		Last_name: c.Last_name,
		Email: c.Email,
		Private_account: 0,
		Date_of_birth: c.Date_of_birth,
	}

	return &profile, nil
}

func (u *Profile) Insert() (code int, err error) {
	mConn := mymongo.Conn()
	defer mConn.Close()

	c := mConn.DB("").C("profiles")
	err = c.Insert(u)

	if err != nil {
		if mgo.IsDup(err) {
			code = common.ErrDupRows
		} else {
			code = common.ErrDatabase
		}
	} else {
		code = 0
	}

	return
}

func (u *Profile) FindByEmail(id string) (code int, err error) {
	mConn := mymongo.Conn()
	defer mConn.Close()

	// u := Profile{}
	c := mConn.DB("").C("profiles")
	err = c.Find(bson.M{"_id": id}).One(u)

	if err != nil {
		if err == mgo.ErrNotFound {
			code = common.ErrNotFound
		} else {
			code = common.ErrDatabase
		}
	} else {
		code = 0
	}
	return
}

func (u *Profile) Update(email, first_name, last_name, date_of_birth, avatar string) (code int, err error) {
	mConn := mymongo.Conn()
	defer mConn.Close()

	c := mConn.DB("").C("profiles")
	err = c.Find(bson.M{"_id": email}).One(u)

	if err != nil {
		if err == mgo.ErrNotFound {
			return common.ErrNotFound, err
		}
		return common.ErrDatabase, err
	}

	err = c.Update(bson.M{"_id": email}, bson.M{"$set": bson.M{"first_name": first_name, "last_name": last_name, "date_of_birth": date_of_birth, "avatar": avatar}})
	if err != nil {
		if err == mgo.ErrNotFound {
			return common.ErrNotFound, err
		}

		return common.ErrDatabase, err
	}

	return 0, nil
}


func (u *Profile) PrivateAccountActive(email string) (code int, err error) {
	mConn := mymongo.Conn()
	defer mConn.Close()

	c := mConn.DB("").C("profiles")
	err = c.Find(bson.M{"_id": email}).One(u)

	if err != nil {
		if err == mgo.ErrNotFound {
			return common.ErrNotFound, err
		}
		return common.ErrDatabase, err
	}

	err = c.Update(bson.M{"_id": email}, bson.M{"$set": bson.M{"private_account": 1}})
	if err != nil {
		if err == mgo.ErrNotFound {
			return common.ErrNotFound, err
		}

		return common.ErrDatabase, err
	}

	return 0, nil
}

func (u *Profile) PrivateAccountDeActive(email string) (code int, err error) {
	mConn := mymongo.Conn()
	defer mConn.Close()

	c := mConn.DB("").C("profiles")
	err = c.Find(bson.M{"_id": email}).One(u)

	if err != nil {
		if err == mgo.ErrNotFound {
			return common.ErrNotFound, err
		}
		return common.ErrDatabase, err
	}

	err = c.Update(bson.M{"_id": email}, bson.M{"$set": bson.M{"private_account": 0}})
	if err != nil {
		if err == mgo.ErrNotFound {
			return common.ErrNotFound, err 
		}
		return common.ErrDatabase, err
	}

	return 0, nil
}

func (u *Profile) DeleteAccount(email string) (code int, err error) {
	mConn := mymongo.Conn()
	defer mConn.Close()

	c := mConn.DB("").C("profiles")
	err = c.Remove(bson.M{"_id": email})

	if err != nil {
		if err == mgo.ErrNotFound {
			code = common.ErrNotFound
		} else {
			code = common.ErrDatabase
		}
	} else {
		code = 0
	}
	return
}