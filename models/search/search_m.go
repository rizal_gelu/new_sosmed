package models

import(
	// "fmt"
	"time"
	"new_sosmed/models/mymongo"
	common "new_sosmed/models/common"
	// profiles "new_sosmed/models/profiles"
	// comments "new_sosmed/models/comments"
	// "gopkg.in/mgo.v2"
	"gopkg.in/mgo.v2/bson"
)

type Product struct {
	Id 								bson.ObjectId		`bson:"_id,omitempty"`
	NameProduct 					string 				`form:"name_product"`
	DescriptionProduct 				string 				`form:"description_product"`
	DateCreated 					int64 				`form:"date_start"`
	DateUpdated 					int64 				`form:"date_end"`
	ImageProduct 					string 				`form:"image_product"`
	Price 							string 				`form:"price"`
}

// func  GetAllTimelines() ([]Timeline, error) {
// 	mConn := mymongo.Conn()
// 	defer mConn.Close()

// 	c := mConn.DB("").C("timelines") 

// 	var timeline []Timeline
// 	err := c.Find(bson.M{}).All(&timeline)
// 	return timeline, err
// }

// func (u *Timeline) FindByID(id string) (code int, err error) {
// 	mConn := mymongo.Conn()
// 	defer mConn.Close()
	
// 	c := mConn.DB("").C("timelines")
// 	err = c.Find(bson.M{"_id": bson.ObjectIdHex(id)}).One(u)

// 	if err != nil {
// 		if err == mgo.ErrNotFound {
// 			code = common.ErrNotFound
// 		} else {
// 			code = common.ErrDatabase
// 		}
// 	} else {
// 		code = 0
// 	}
// 	return
// }

// func (u *Timeline) FindByEmail(email string) ([]Timeline, error) {
// 	mConn := mymongo.Conn()
// 	defer mConn.Close()

// 	item := []Timeline{}
// 	c := mConn.DB("").C("timelines")
// 	err := c.Find(bson.M{"email": email}).All(&item)
	
// 	return item, err
// }

// func (u *Timeline) UpdateTimeline(id, comments, file string, date_updated int64) (code int, err error) {
// 	mConn := mymongo.Conn()
// 	defer mConn.Close()

// 	c := mConn.DB("").C("timelines")
// 	err = c.Update(bson.M{"_id": bson.ObjectIdHex(id)}, bson.M{"$set": bson.M{"comments": comments, "file": file, "dateupdated": date_updated}})

// 	if err != nil {
// 		if err == mgo.ErrNotFound {
// 			code = common.ErrNotFound
// 		} else {
// 			code = common.ErrDatabase
// 		}
// 	} else {
// 		code = 0
// 	}
// 	return
// }

// func (u * Timeline) DeleteTimeline(id string) (code int, err error) {
// 	mConn := mymongo.Conn()
// 	defer mConn.Close()

// 	c := mConn.DB("").C("timelines")
// 	err = c.Remove(bson.M{"_id": bson.ObjectIdHex(id)})

// 	if err != nil {
// 		if err == mgo.ErrNotFound {
// 			code = common.ErrNotFound
// 		} else {
// 			code = common.ErrDatabase
// 		}
// 	} else {
// 		code = 0
// 	}
// 	return
// }