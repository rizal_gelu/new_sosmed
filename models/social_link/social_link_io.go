package models

import(
	// "gopkg.in/mgo.v2/bson"
)

type SocialLinkPostForm struct {
	Email    				string `form:"email"					valid:"Required;Mobile"`				
	Name_social_media 		string `form:"name_social_media"`
	Username_social_media 	string `form:"username_social_media"`
}

type SocialLinkGetForm struct {
	Email 					string `form:"email"`
}

type SocialLinkInfo struct {
	Code 					int 						`json:"code"`
	Message 				string 						`json:"message"`
	Data 		 			*SocialLink 				`json:"data"`
}

type SocialLinkUser struct {
	Code 					int 						`json:"code"`
	Message 				string 						`json:"message"`
	Data 		 			[]map[string]interface{}  	`json:"data"`
}