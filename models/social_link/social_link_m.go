package models

import(
	"time"
	"new_sosmed/models/mymongo"
	common "new_sosmed/models/common"
	"gopkg.in/mgo.v2"
	"gopkg.in/mgo.v2/bson"
)

type SocialLink struct {
	Id 						bson.ObjectId		`bson:"_id,omitempty"`
	Email 					string 				`bson:"email" json:"email"`
	Name_social_media 		string 				`json:"name_social_media"`
	Username_social_media 	string 				`json:"username_social_media"`
	DateCreated				int64 				`json:"date_created"`
	DateUpdated				int64 				`json:"date_updated"`
}

func NewSocialLinkPost(r *SocialLinkPostForm) (u *SocialLink, err error) {
	date_created := time.Now().UnixNano() / int64(time.Millisecond)

	socialLink := SocialLink{
		Email: r.Email,
		Name_social_media: r.Name_social_media,
		Username_social_media: r.Username_social_media,
		DateCreated: date_created,
	}
	return &socialLink, nil
}

func (u *SocialLink) Insert() (code int, err error) {
	mConn := mymongo.Conn()
	defer mConn.Close()
	
	c := mConn.DB("").C("social_media_user")
	err = c.Insert(u)

	if err != nil {
		code = common.ErrDatabase
	} else {
		code = 0
	}

	return
}

func (u *SocialLink) FindByID(id string) (code int, err error) {
	mConn := mymongo.Conn()
	defer mConn.Close()
	
	c := mConn.DB("").C("social_media_user")
	err = c.Find(bson.M{"_id": bson.ObjectIdHex(id)}).One(u)

	if err != nil {
		if err == mgo.ErrNotFound {
			code = common.ErrNotFound
		} else {
			code = common.ErrDatabase
		}
	} else {
		code = 0
	}
	return
}

func (u *SocialLink) FindByEmail(email string) ([]SocialLink, error) {
	mConn := mymongo.Conn()
	defer mConn.Close()

	item := []SocialLink{}
	c := mConn.DB("").C("social_media_user")
	err := c.Find(bson.M{"email": email}).All(&item)
	
	return item, err
}

func (u *SocialLink) UpdateSocialMedia(id, email, name_social_media, username_social_media string, date_updated int64) (code int, err error) {
	mConn := mymongo.Conn()
	defer mConn.Close()

	c := mConn.DB("").C("timelines")
	err = c.Update(bson.M{"_id": bson.ObjectIdHex(id)}, bson.M{"$set": bson.M{"name_social_media": name_social_media, "username_social_media": username_social_media, "dateupdated": date_updated}})

	if err != nil {
		if err == mgo.ErrNotFound {
			code = common.ErrNotFound
		} else {
			code = common.ErrDatabase
		}
	} else {
		code = 0
	}
	return
}

func (u * SocialLink) DeleteSocialLink(id string) (code int, err error) {
	mConn := mymongo.Conn()
	defer mConn.Close()

	c := mConn.DB("").C("timelines")
	err = c.Remove(bson.M{"_id": bson.ObjectIdHex(id)})

	if err != nil {
		if err == mgo.ErrNotFound {
			code = common.ErrNotFound
		} else {
			code = common.ErrDatabase
		}
	} else {
		code = 0
	}
	return
}