package models

import(
	// "gopkg.in/mgo.v2/bson"
)

type TaskBoardPostForm struct {
	Title_task 				string 				`form:"title_task"`			
	Company_id		 		string 				`form:"company_id"`
	Description_task 	 	string 				`form:"description_task"`
	Deadline				string 				`form:"deadline"`
	Image_task 				string 				`form:"image_task"`
	CreateBy    			string 				`form:"create_by"`
	UpdateBy 				string 				`form:"update_by"`
	DateCreated				int64 				`form:"date_created"`
	DateUpdated				int64 				`form:"date_updated"`
}

type TaskBoardGetForm struct {
	Id 					string `form:"id"`
}

type TaskBoardGetByCompanyIdForm struct {
	Company_id 			string `form:"company_id"`
}

type TaskBoardInfo struct {
	Code 					int 						`json:"code"`
	Message 				string 						`json:"message"`
	Data 		 			*TaskBoard  				`json:"data"`
}

type TaskBoardByCompanyIDInfo struct {
	Code 					int 						`json:"code"`
	Message 				string 						`json:"message"`
	Data 		 			*[]TaskBoard  				`json:"data_taskboard"`
}