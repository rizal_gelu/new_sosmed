package models

import(
	"time"
	"new_sosmed/models/mymongo"
	common "new_sosmed/models/common"
	"gopkg.in/mgo.v2"
	"gopkg.in/mgo.v2/bson"
)

type TaskBoard struct {
	Id 						bson.ObjectId		`bson:"_id,omitempty"`
	Title_task 				string 				`json:"title_task"`			
	Company_id		 		string 				`json:"company_id"`
	Description_task 	 	string 				`json:"description_task"`
	Deadline				string 				`json:"deadline"`
	Image_task 				string 				`json:"image_task"`
	CreateBy    			string 				`json:"create_by"`
	UpdateBy 				string 				`json:"update_by"`
	DateCreated				int64 				`json:"date_created"`
	DateUpdated				int64 				`json:"date_updated"`
}

func NewTaskBoardPost(r *TaskBoardPostForm) (u *TaskBoard, err error) {
	date_created := time.Now().UnixNano() / int64(time.Millisecond)

	taskboard := TaskBoard{
		Title_task: r.Title_task,
		Company_id: r.Company_id,
		Description_task: r.Description_task,
		CreateBy: r.CreateBy,
		Deadline: r.Deadline,
		DateCreated: date_created,
	}
	return &taskboard, nil
}

func (u *TaskBoard) InsertTaskBoard() (code int, err error) {
	mConn := mymongo.Conn()
	defer mConn.Close()
	
	c := mConn.DB("").C("task_board")
	err = c.Insert(u)

	if err != nil {
		code = common.ErrDatabase
	} else {
		code = 0
	}

	return
}

func (u *TaskBoard) FindTaskBoardByID(id string) (code int, err error) {
	mConn := mymongo.Conn()
	defer mConn.Close()
	
	c := mConn.DB("").C("task_board")
	err = c.Find(bson.M{"_id": bson.ObjectIdHex(id)}).One(u)

	if err != nil {
		if err == mgo.ErrNotFound {
			code = common.ErrNotFound
		} else {
			code = common.ErrDatabase
		}
	} else {
		code = 0
	}
	return
}

func (u *TaskBoard) FindByCompanyId(company_id string) ([]TaskBoard, error) {
	mConn := mymongo.Conn()
	defer mConn.Close()

	item := []TaskBoard{}
	c := mConn.DB("").C("task_board")
	err := c.Find(bson.M{"company_id": company_id}).All(&item)
	
	return item, err
}

// func (u *SocialLink) UpdateSocialMedia(id, email, name_social_media, username_social_media string, date_updated int64) (code int, err error) {
// 	mConn := mymongo.Conn()
// 	defer mConn.Close()

// 	c := mConn.DB("").C("timelines")
// 	err = c.Update(bson.M{"_id": bson.ObjectIdHex(id)}, bson.M{"$set": bson.M{"name_social_media": name_social_media, "username_social_media": username_social_media, "dateupdated": date_updated}})

// 	if err != nil {
// 		if err == mgo.ErrNotFound {
// 			code = common.ErrNotFound
// 		} else {
// 			code = common.ErrDatabase
// 		}
// 	} else {
// 		code = 0
// 	}
// 	return
// }

// func (u * SocialLink) DeleteSocialLink(id string) (code int, err error) {
// 	mConn := mymongo.Conn()
// 	defer mConn.Close()

// 	c := mConn.DB("").C("timelines")
// 	err = c.Remove(bson.M{"_id": bson.ObjectIdHex(id)})

// 	if err != nil {
// 		if err == mgo.ErrNotFound {
// 			code = common.ErrNotFound
// 		} else {
// 			code = common.ErrDatabase
// 		}
// 	} else {
// 		code = 0
// 	}
// 	return
// }