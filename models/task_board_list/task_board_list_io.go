package models

import(
	// "gopkg.in/mgo.v2/bson"
)

type TaskBoardListPostForm struct {
	Title_list_task 		string 				`form:"title_list_task"`			
	Task_id 			 	string 				`form:"task_id"`
	Company_id 				string 				`form:"company_id"`
	Description_list_task 	string 				`form:"description_list_task"`
	Priority				string 				`form:"priority"`
	Due_date 				string 				`form:"due_date"`
	CreateBy    			string 				`form:"create_by"`
	UpdateBy 				string 				`form:"update_by"`
	DateCreated				int64 				`form:"date_created"`
	DateUpdated				int64 				`form:"date_updated"`
}

type TaskBoardListGetForm struct {
	Id 					string `form:"id"`
}

type TaskBoardGetByCompanyIdForm struct {
	Company_id 			string `form:"company_id"`
}

type TaskBoardListInfo struct {
	Code 					int 						`json:"code"`
	Message 				string 						`json:"message"`
	Data 		 			*TaskBoardList  			`json:"data"`
}

type TaskBoardByCompanyIDInfo struct {
	Code 					int 						`json:"code"`
	Message 				string 						`json:"message"`
	Data 		 			*[]TaskBoardList  			`json:"data_taskboard_list"`
}