package models

import(
	"time"
	"new_sosmed/models/mymongo"
	common "new_sosmed/models/common"
	"gopkg.in/mgo.v2"
	"gopkg.in/mgo.v2/bson"
)

type TaskBoardList struct {
	Id 						bson.ObjectId		`bson:"_id,omitempty"`
	Title_list_task 		string 				`json:"title_list_task"`	
	Company_id 				string 				`json:"company_id"`		
	Task_id 		 		string 				`json:"task_id"`
	Description_list_task 	string 				`json:"description_list_task"`
	Priority				string 				`json:"priority"`
	Due_date 				string 				`json:"due_date"`
	CreateBy    			string 				`json:"create_by"`
	UpdateBy 				string 				`json:"update_by"`
	DateCreated				int64 				`json:"date_created"`
	DateUpdated				int64 				`json:"date_updated"`
}

func NewTaskBoardListPost(r *TaskBoardListPostForm) (u *TaskBoardList, err error) {
	date_created := time.Now().UnixNano() / int64(time.Millisecond)

	taskBoardlist := TaskBoardList{
		Title_list_task: r.Title_list_task,
		Task_id: r.Task_id,
		Company_id: r.Company_id,
		Description_list_task: r.Description_list_task,
		Priority: r.Priority,
		CreateBy: r.CreateBy,
		Due_date: r.Due_date,
		DateCreated: date_created,
	}
	return &taskBoardlist, nil
}

func (u *TaskBoardList) InsertTaskBoardList() (code int, err error) {
	mConn := mymongo.Conn()
	defer mConn.Close()
	
	c := mConn.DB("").C("task_board_list")
	err = c.Insert(u)

	if err != nil {
		code = common.ErrDatabase
	} else {
		code = 0
	}

	return
}

func (u *TaskBoardList) FindTaskBoardListByID(id string) (code int, err error) {
	mConn := mymongo.Conn()
	defer mConn.Close()
	
	c := mConn.DB("").C("task_board_list")
	err = c.Find(bson.M{"_id": bson.ObjectIdHex(id)}).One(u)

	if err != nil {
		if err == mgo.ErrNotFound {
			code = common.ErrNotFound
		} else {
			code = common.ErrDatabase
		}
	} else {
		code = 0
	}
	return
}

func (u *TaskBoardList) FindTaskBoardListByCompanyId(company_id string) ([]TaskBoardList, error) {
	mConn := mymongo.Conn()
	defer mConn.Close()

	item := []TaskBoardList{}
	c := mConn.DB("").C("task_board_list")
	err := c.Find(bson.M{"company_id": company_id}).All(&item)
	
	return item, err
}

// func (u *SocialLink) UpdateSocialMedia(id, email, name_social_media, username_social_media string, date_updated int64) (code int, err error) {
// 	mConn := mymongo.Conn()
// 	defer mConn.Close()

// 	c := mConn.DB("").C("timelines")
// 	err = c.Update(bson.M{"_id": bson.ObjectIdHex(id)}, bson.M{"$set": bson.M{"name_social_media": name_social_media, "username_social_media": username_social_media, "dateupdated": date_updated}})

// 	if err != nil {
// 		if err == mgo.ErrNotFound {
// 			code = common.ErrNotFound
// 		} else {
// 			code = common.ErrDatabase
// 		}
// 	} else {
// 		code = 0
// 	}
// 	return
// }

// func (u * SocialLink) DeleteSocialLink(id string) (code int, err error) {
// 	mConn := mymongo.Conn()
// 	defer mConn.Close()

// 	c := mConn.DB("").C("timelines")
// 	err = c.Remove(bson.M{"_id": bson.ObjectIdHex(id)})

// 	if err != nil {
// 		if err == mgo.ErrNotFound {
// 			code = common.ErrNotFound
// 		} else {
// 			code = common.ErrDatabase
// 		}
// 	} else {
// 		code = 0
// 	}
// 	return
// }