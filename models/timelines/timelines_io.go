package models

import(
	// "gopkg.in/mgo.v2/bson"
)

type TimelinePostForm struct {
	Id 						string 						`form"id"`
	Email 					string 						`form:"email"`
	Description 			string 						`form:"description"`
	File 					string 						`form:"file"`
	DateCreated  			string 						`form:"date_created"`
	DateUpdated 			string 						`form:"date_update"`
}

type TimelineInfo struct { // Get All Timeline 
	Code    	 			int   						`json:"code"`
	Message 				string 						`json:"message"`
	TimelineInfo 			[]map[string]interface{} 	`json:"data_timeline"`
}

type TimelineSingleInfo struct { // Get Singel Timeline By ID (Just 1 timeline)
	Code 					int 						`json:"code"`
	Message 				string 						`json:"message"`
	SingleTimeline 			*Timeline 					`json:"data"`
}

type GetTimelineByIdInfo struct {
	Code 					int 						`json:"code"`
	Message 				string 						`json:"message"`
	SingleTimeline 			map[string]interface{} 		`json:"data"`
}

type TimelineGetAllByEmailInfo struct { // Response Get Timeline By Email (All Timelines by One User)
	Code 					int 						`json:"code"`
	Message 				string 						`json:"message"`
	GetAllTimelineByEmail 	[]map[string]interface{} 	`json:"data_timeline_user"`
}

type TimelineGetSingleForm struct { // Form Get Single Timeline (Just Single)
	Id 						string 						`form:"id"`
}

type GetTimelineAllByEmailForm struct { // Form Get Timeline By Email User (All Timeline by One User)
	Email 					string 						`form:"email"`
}

type TimelineUpdateForm struct {
	Id 						string 						`form:"id"`
	Description 			string 						`form:"description"`
	File 					string 						`form:"file"`
	DateUpdated 			string 						`form:"date_update"`
}

// type RepostTiemlineform struct {
// 	Id 						string 						`form:"id"`
// 	Email 					string 						`form:"email"`
// 	Description 			string 						`form:"description"`
// 	File 						string 						`form:"id"`
// 	Id 						string 						`form:"id"`
// 	Id 						string 						`form:"id"`
// }

type DeleteTimelineByIdForm struct {
	Id        				string 						`form:"id"`
}

type PostLikeTLForm struct {
	Id 						string 						`form:"id"`
}