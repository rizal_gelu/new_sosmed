package models

import(
	// "fmt"
	"time"
	"new_sosmed/models/mymongo"
	common "new_sosmed/models/common"
	// profiles "new_sosmed/models/profiles"
	// comments "new_sosmed/models/comments"
	"gopkg.in/mgo.v2"
	"gopkg.in/mgo.v2/bson"
	// likes "new_sosmed/models/like"
)

type Timeline struct {
	Id 						bson.ObjectId		`bson:"_id,omitempty"`
	Email 					string 				`bson:"email" json:"email"`
	Description 			string 				`json:"description"`
	File 					string 				`json:"file"`
	DateCreated				int64 				`json:"date_created"`
	DateUpdated				int64 				`json:"date_updated"`
}

func NewTimelinePost(r *TimelinePostForm, file string) (u *Timeline, err error) {
	date_created := time.Now().UnixNano() / int64(time.Millisecond)

	if file != "" {
		timeline := Timeline{
			Email: r.Email,
			Description: r.Description,
			File: file,
			DateCreated: date_created,
			// Like: 0,
		}
		return &timeline, nil
	} else {
		timeline := Timeline{
			Email: r.Email,
			Description: r.Description,
			DateCreated: date_created,
			// Like: 0,
		}
		return &timeline, nil
	}
}

func (u *Timeline) Insert() (code int, err error) {
	mConn := mymongo.Conn()
	defer mConn.Close()
	
	c := mConn.DB("").C("timelines")
	err = c.Insert(u)

	if err != nil {
		code = common.ErrDatabase
	} else {
		code = 0
	}

	return
}

func  GetAllTimelines() ([]Timeline, error) {
	mConn := mymongo.Conn()
	defer mConn.Close()

	c := mConn.DB("").C("timelines") 

	var timeline []Timeline
	err := c.Find(bson.M{}).All(&timeline)
	return timeline, err
}

func (u *Timeline) FindByID(id string) (code int, err error) {
	mConn := mymongo.Conn()
	defer mConn.Close()
	
	c := mConn.DB("").C("timelines")
	err = c.Find(bson.M{"_id": bson.ObjectIdHex(id)}).One(u)

	if err != nil {
		if err == mgo.ErrNotFound {
			code = common.ErrNotFound
		} else {
			code = common.ErrDatabase
		}
	} else {
		code = 0
	}
	return
}

func (u *Timeline) FindByEmail(email string) ([]Timeline, error) {
	mConn := mymongo.Conn()
	defer mConn.Close()

	item := []Timeline{}
	c := mConn.DB("").C("timelines")
	err := c.Find(bson.M{"email": email}).All(&item)
	
	return item, err
}

func (u *Timeline) UpdateTimeline(id, comments, file string, date_updated int64) (code int, err error) {
	mConn := mymongo.Conn()
	defer mConn.Close()

	c := mConn.DB("").C("timelines")
	err = c.Update(bson.M{"_id": bson.ObjectIdHex(id)}, bson.M{"$set": bson.M{"comments": comments, "file": file, "dateupdated": date_updated}})

	if err != nil {
		if err == mgo.ErrNotFound {
			code = common.ErrNotFound
		} else {
			code = common.ErrDatabase
		}
	} else {
		code = 0
	}
	return
}

func (u *Timeline) DeleteTimeline(id string) (code int, err error) {
	mConn := mymongo.Conn()
	defer mConn.Close()

	c := mConn.DB("").C("timelines")
	err = c.Remove(bson.M{"_id": bson.ObjectIdHex(id)})

	if err != nil {
		if err == mgo.ErrNotFound {
			code = common.ErrNotFound
		} else {
			code = common.ErrDatabase
		}
	} else {
		code = 0
	}
	return
}