package models

// RegisterForm definiton.
type RegisterForm struct {
	Email 	 				string `form:"email" 	 				valid:"Required;Mobile"`
	Password 				string `form:"password" 				valid:"Required"`
}

// LoginForm definiton.
type LoginForm struct {
	Email    				string `form:"email"					valid:"Required;Mobile"`				
	Password 				string `form:"password"`
}

// LoginInfo definiton.
type LoginInfo struct {
	Code    	 			int   `json:"code"`
	Data 					*User `json:"data"`
}

// LogoutForm defintion.
type LogoutForm struct {
	Email 					string `form:"phone" 					valid:"Required;Mobile"`
}

// PasswdForm definition.
type Change_PasswordForm struct {
	Email   				string `form:"email"        			valid:"Required;Mobile"`
	OldPass 				string `form:"old_password" 			valid:"Required"`
	NewPass 				string `form:"new_password" 			valid:"Required"`
}

// UploadsForm definiton.
type UploadsForm struct {
	Email string `form:"phone" valid:"Required;Mobile"`
}

type ActivationUserForm struct {
	Email 					string `form:"email" 					valid:"Required;Mobile"`
	Activation_code 		string `form:"activation_code"  		valid:"Required"`
}

type Forgot_password struct {
	Email 					string `form:"email"					valid:"Required;Mobile"`
}

type Forgot_pwd_confirm struct {
	Email 					string `form:"email"					valid:"Required;Mobile"`
	Forgot_pwd_code  		string `form:"forgot_password_code"`
}

type Reset_password struct {
	Email 					string `form:"email"					valid:"Required;Mobile"`
	Forgot_password_code	string `form:"forgot_password_code" 	valid:"Required"`
	NewPass 				string `form:"new_password" 			valid:"Required"`
}