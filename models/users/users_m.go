package models

import (
	"crypto/rand"
	"fmt"
	"io"
	"time"

	common "new_sosmed/models/common"
	mymongo "new_sosmed/models/mymongo"
	"new_sosmed/library"

	"golang.org/x/crypto/scrypt"
	"gopkg.in/mgo.v2"
	"gopkg.in/mgo.v2/bson"
)

type User struct {
	// ID 						bson.ObjectId 		`bson:"_id" 					json:"_id, omitempty"`
	Email 					string 				`bson:"_id" 					json:"email, omitempty"`
	Password 				string 				`bson:"password" 				json:"password, omitempty"`
	Salt 					string 				`bson:"salt" 					json:"salt, omitempty"`
	RegDate 				int64 				`bson:"reg_date"				json:"reg_date, omitempty"`
	Status 					int 				`bson:"status" 					json:"status, omitempty"`
	Activation_code 		string 				`bson:"activation_code" 		json:"activation_code"`
	Forgot_password_code 	string 				`bson:"forgot_password_code" 	json:"forgot_password_code, omitempty"`
	Forgot_password_create 	int64 				`bson:"forgot_password_create" 	json:"forgot_password_create, omitempty"`
	// Phone					string 				`bson:"phone"					json:"phone"`
	// Avatar 					string 				`bson:"avatar"					json:"avatar"`
	// Bio 					string 				`bson:"bio"						json:"bio"`
	// Province 				string 				`bson:"province"				json:"province"`
	// City 					string 				`bson:"city"					json:"city"`
	// College 				string 				`bson:"college"					json:"college"`
	// Major					string 				`bson:"major"					json:"major"` // jurusan kuliah
	// HighSchool 				string 				`bson:"high_school"				json:"high_school"`
}

const pwHashBytes = 64

func generateSalt() (salt string, err error) {
	buf := make([]byte, pwHashBytes)
	if _, err := io.ReadFull(rand.Reader, buf); err != nil {
		return "", err
	}
	return fmt.Sprintf("%x", buf), nil
}

func generatePassHash(password string, salt string) (hash string, err error) {
	h, err := scrypt.Key([]byte(password), []byte(salt), 16384, 8, 1, pwHashBytes)
	if err != nil {
		return "", err
	}

	return fmt.Sprintf("%x", h), nil
}

func NewUser(r *RegisterForm, t time.Time) (u *User, err error) {
	salt, err := generateSalt()
	if err != nil {
		return nil, err
	}

	hash, err := generatePassHash(r.Password, salt) 
	if err != nil {
		return nil, err
	}

	reg_date := time.Now().UnixNano() / int64(time.Millisecond)

	user := User{
		Email : r.Email,
		Password: hash,
		Salt:     salt,
		RegDate:  reg_date,
		Activation_code : library.RandomString(40),
		Status: 0,
		// Phone: "",
		// City: "",
		// Province: "",
		// Bio: "",
		// College: "",
		// Major: "",
		// HighSchool: "",
	}

	return &user, nil
}

func (u *User) Insert() (code int, err error) {
	mConn := mymongo.Conn()
	defer mConn.Close()

	c := mConn.DB("").C("users")
	err = c.Insert(u)

	if err != nil {
		if mgo.IsDup(err) {
			code = common.ErrDupRows
		} else {
			code = common.ErrDatabase
		}
	} else {
		code = 0
	}
	return
}

func (u *User) FindByID(id string) (code int, err error) {
	mConn := mymongo.Conn()
	defer mConn.Close()

	c := mConn.DB("").C("users")
	err = c.Find(bson.M{"_id": id, "status": 1}).One(u)

	if err != nil {
		if err == mgo.ErrNotFound {
			code = common.ErrNotFound
		} else {
			code = common.ErrDatabase
		}
	} else {
		code = 0
	}
	return
}

func (u *User) CheckPass(pass string) (ok bool, err error) {
	hash, err := generatePassHash(pass, u.Salt)
	if err != nil {
		return false, err
	}

	return u.Password == hash, nil
}

func (u *User) ClearPass() {
	u.Password = ""
	u.Salt = ""
}

func ChangePass(id, oldPass, newPass string) (code int, err error) {
	mConn := mymongo.Conn()
	defer mConn.Close()

	c := mConn.DB("").C("users") 
	u := User{}
	
	err = c.Find(bson.M{"_id": id}).One(&u)

	if err != nil {
		if err == mgo.ErrNotFound {
			return common.ErrNotFound, err
		} 

		return common.ErrDatabase, err
	}

	oldHash, err := generatePassHash(oldPass, u.Salt)
	if err != nil {
		return common.ErrSystem, err
	}

	newSalt, err := generateSalt()
	if err != nil {
		return common.ErrSystem, err
	}

	newHash, err := generatePassHash(newPass, newSalt)
	if err != nil {
		return common.ErrSystem, err
	}

	err = c.Update(bson.M{"_id": id, "password": oldHash}, bson.M{"$set": bson.M{"password": newHash, "salt": newSalt}})
	if err != nil {
		if err == mgo.ErrNotFound {
			return common.ErrNotFound, err
		}

		return common.ErrDatabase, err
	}

	return 0, nil
}

func ActivateUser(id, activation_code string) (code int, err error) {
	mConn := mymongo.Conn()
	defer mConn.Close()

	c := mConn.DB("").C("users")
	u := User{}
	err = c.Find(bson.M{"_id": id, "activation_code": activation_code}).One(&u)

	if err != nil {
		if err == mgo.ErrNotFound {
			return common.ErrNotFound, err
		}
		return common.ErrDatabase, err
	}

	err = c.Update(bson.M{"_id": id, "status": 0}, bson.M{"$set": bson.M{"status": 1}})
	if err != nil {
		if err == mgo.ErrNotFound {
			return common.ErrNotFound, err
		}

		return common.ErrDatabase, err
	}

	return 0, nil
}

func ForgotPassword(id string) (code int, err error) {
	mConn := mymongo.Conn()
	defer mConn.Close()

	c := mConn.DB("").C("users")
	u := User{} 
	
	err = c.Find(bson.M{"_id": id}).One(&u)

	if err != nil {
		if err == mgo.ErrNotFound {
			return common.ErrNotFound, err
		}
		return common.ErrDatabase, err
	}

	if u.Status != 1 {
		return common.ErrUserNotActive, err
	} else {
		forgot_password_code 	:= library.RandomString(40)
		forgot_password_create 	:=  time.Now().UnixNano() / int64(time.Millisecond)
		err = c.Update(bson.M{"_id": id, "forgot_password_code": u.Forgot_password_code, "forgot_password_create": u.Forgot_password_create}, bson.M{"$set": bson.M{"forgot_password_code": forgot_password_code, "forgot_password_create": forgot_password_create}})
		if err != nil {
			if err == mgo.ErrNotFound {
				return common.ErrNotFound, err
			}

			return common.ErrDatabase, err
		}
		return 0, nil
	}
}

func ForgotPasswordConfirm(id, forgot_password_code string) (code int, err error) {
	mConn := mymongo.Conn()
	defer mConn.Close()

	c := mConn.DB("").C("users")
	u := User{}

	err = c.Find(bson.M{"_id": id, "forgot_password_code": forgot_password_code, "status": 1}).One(&u)

	if err != nil {
		if err == mgo.ErrNotFound {
			code = common.ErrNotFound
		} else {
			code = common.ErrDatabase
		}
	} else {
		code = 0
	}
	return
} 

func ResetPassword(id, forgot_password_code, newPass string) (code int, err error) {
	mConn := mymongo.Conn()
	defer mConn.Close()

	c := mConn.DB("").C("users")
	u := User{}
	err = c.Find(bson.M{"_id": id, "forgot_password_code": forgot_password_code}).One(&u)

	if err != nil {
		if err == mgo.ErrNotFound {
			return common.ErrNotFound, err
		}
		return common.ErrDatabase, err
	}

	newSalt, err := generateSalt()
	if err != nil {
		return common.ErrSystem, err
	}

	newHash, err := generatePassHash(newPass, newSalt)
	if err != nil {
		return common.ErrSystem, err
	}

	err = c.Update(bson.M{"_id": id}, bson.M{"$set": bson.M{"password": newHash, "salt": newSalt}})
	if err != nil {
		if err == mgo.ErrNotFound {
			return common.ErrNotFound, err
		}

		return common.ErrDatabase, err
	}

	return 0, nil
}