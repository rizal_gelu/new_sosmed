package models

// RegisterForm definiton.
type AddWorkplaceForm struct {
	Email 	 				string `form:"email" 	 				valid:"Required;Mobile"`
	NameCompany 			string `form:"name_company"				valid:"Required"`
	Job_title 				string `form:"job_title" 				valid:"Required"`
	Location 				string `form:"location"					valid:"Required"`
	Description				string `form:"description"`
}

type WorkplaceInfo struct {
	Code 					int 						`json:"code"`
	Message 				string 						`json:"message"`
	Data 		 			*WorkPlace 					`json:"data"`
}

type GetWorkplaceForm struct {
	Email 					string `form:"email"`
}
