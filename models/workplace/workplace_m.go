package models

import (
	"time"

	common "new_sosmed/models/common"
	mymongo "new_sosmed/models/mymongo"

	"gopkg.in/mgo.v2"
	"gopkg.in/mgo.v2/bson"
)

type WorkPlace struct {
	Email 					string 				`bson:"_id" 					json:"email, omitempty"`
	NameCompany 			string 				`bson:"name_company"			json:"name_company, omitempty"`
	Job_title 				string 				`bson:"job_title" 				json:"job_title, omitempty"`
	Location 				string 				`bson:"location" 				json:"location, omitempty"`
	Description 			string 				`bson:"description"				json:"description, omitempty"`
	DateCreated				int64 				`json:"date_created"`
	DateUpdated				int64 				`json:"date_updated"`
}

func NewWorkPlace(r *AddWorkplaceForm) (u *WorkPlace, err error) {
	reg_date := time.Now().UnixNano() / int64(time.Millisecond)

	workplace := WorkPlace{
		Email : r.Email,
		NameCompany: r.NameCompany,
		Job_title: r.Job_title,
		Location: r.Location,
		Description : r.Description,
		DateCreated: reg_date,
	}

	return &workplace, nil
}

func (u *WorkPlace) Insert() (code int, err error) {
	mConn := mymongo.Conn()
	defer mConn.Close()

	c := mConn.DB("").C("workplace")
	err = c.Insert(u)

	if err != nil {
		if mgo.IsDup(err) {
			code = common.ErrDupRows
		} else {
			code = common.ErrDatabase
		}
	} else {
		code = 0
	}
	return
}

func (u *WorkPlace) FindByID(email string) (code int, err error) {
	mConn := mymongo.Conn()
	defer mConn.Close()

	c := mConn.DB("").C("workplace")
	err = c.Find(bson.M{"email": email}).One(u)

	if err != nil {
		if err == mgo.ErrNotFound {
			code = common.ErrNotFound
		} else {
			code = common.ErrDatabase
		}
	} else {
		code = 0
	}
	return
}