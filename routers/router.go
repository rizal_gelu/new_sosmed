// @APIVersion 1.0.0
// @Title beego Test API
// @Description beego has a very cool tools to autogenerate documents for your API
// @Contact astaxie@gmail.com
// @TermsOfServiceUrl http://beego.me/
// @License Apache 2.0
// @LicenseUrl http://www.apache.org/licenses/LICENSE-2.0.html
package routers

import (
	"new_sosmed/controllers"

	"github.com/astaxie/beego"
)


func init() {
	// web.run()
	beego.Router("/", &controllers.MainController{})
	ns := beego.NewNamespace("/v1",
			beego.NSNamespace("/users",
				beego.NSRouter("/register", &controllers.UserController{}, "post:Register"),
				beego.NSRouter("/login", &controllers.UserController{}, "post:Login"),
				beego.NSRouter("/logout", &controllers.UserController{}, "post:Logout"),
				beego.NSRouter("/update_profile", &controllers.UserController{}, "post:Update_Profile"),
				beego.NSRouter("/get_profile:id", &controllers.UserController{}, "get:Find_Profile"),
				beego.NSRouter("/change_password", &controllers.UserController{}, "post:Change_Password"),
				beego.NSRouter("/activation_user", &controllers.UserController{}, "post:Activation_user"),
				beego.NSRouter("/forgot_password", &controllers.UserController{}, "post:Forgot_password"),
				beego.NSRouter("/forgot_password_confirm", &controllers.UserController{}, "post:ForgotPasswordConfirm"),
				beego.NSRouter("/reset_password", &controllers.UserController{}, "post:Reset_password"),
				beego.NSRouter("/private_account_activate", &controllers.UserController{}, "post:PrivateAccountActive"),
				beego.NSRouter("/private_account_deactivate", &controllers.UserController{}, "post:PrivateAccountDeActive"),
				// beego.NSRouter("/delete_account", &controllers.UserController{}, "delete:DeleteAccount"),
				beego.NSRouter("/post_employment_user", &controllers.UserController{}, "post:PostEmployment"),
				beego.NSRouter("/get_employment_user:id", &controllers.UserController{}, "get:GetEmploymentByEmail"),
				beego.NSRouter("/update_employment_user:id", &controllers.UserController{}, "put:UpdateEmployment"),
				beego.NSRouter("/delete_employment_user:id", &controllers.UserController{}, "delete:DeleteEmployment"),
				beego.NSRouter("/post_education_high_school", &controllers.UserController{}, "post:PostEducationHighSchool"),
				beego.NSRouter("/get_education_high_school:id", &controllers.UserController{}, "get:GetEducationHighSchoolByEmail"),
				beego.NSRouter("/update_education_high_school:id", &controllers.UserController{}, "put:UpdateEducationHighSchool"),
				beego.NSRouter("/delete_education_high_school:id", &controllers.UserController{}, "delete:DeleteEducationHighSchool"),
				beego.NSRouter("/post_education_college", &controllers.UserController{}, "post:PostEducationCollege"),
				beego.NSRouter("/get_education_college", &controllers.UserController{}, "get:GetEducationCollegeByEmail"),
				beego.NSRouter("/update_education_college", &controllers.UserController{}, "put:UpdateEducationCollege"),
				beego.NSRouter("/delete_education_college", &controllers.UserController{}, "delete:DeleteEducationCollege"),
				beego.NSRouter("/post_residence", &controllers.UserController{}, "post:PostResidence"),
				beego.NSRouter("/get_residence", &controllers.UserController{}, "get:GetResidenceByEmail"),
				beego.NSRouter("/update_residence", &controllers.UserController{}, "put:UpdateResidence"),
				beego.NSRouter("/delete_residence", &controllers.UserController{}, "delete:DeleteResidence"),
				beego.NSRouter("/post_bio", &controllers.UserController{}, "post:Post_bio"),
				beego.NSRouter("/get_bio", &controllers.UserController{}, "get:Get_bio"),
				beego.NSRouter("/post_workplace", &controllers.UserController{}, "post:Add_workplace"),
				beego.NSRouter("/get_workplace", &controllers.UserController{}, "get:Get_Workplace"),
				beego.NSRouter("/post_phone", &controllers.UserController{}, "post:Add_phone"),
				beego.NSRouter("/get_phone", &controllers.UserController{}, "get:Get_phone"),
				beego.NSRouter("/post_address", &controllers.UserController{}, "post:Post_address"),
				beego.NSRouter("/get_address", &controllers.UserController{}, "get:Get_address"),
				beego.NSRouter("/get_product", &controllers.ProductController{}, "get:GetAllProduct"),
				// beego.NSRouter("/order", &controllers.),
				// beego.NSRouter("/cart", &controllers.)
			),
			beego.NSNamespace("/company",
				beego.NSRouter("/create_company", &controllers.CompanyController{}, "post:CreateCompany"),
				beego.NSRouter("/get_company:id", &controllers.CompanyController{}, "get:GetCompanyById"),
				beego.NSRouter("/update_company:id", &controllers.CompanyController{}, "put:Update_company"),
				beego.NSRouter("/post_job", &controllers.CompanyController{}, "post:Post_job"),
				beego.NSRouter("/get_job_by_id:id", &controllers.CompanyController{}, "get:Get_job_by_id"),
				beego.NSRouter("/get_job_by_company_id:company_id", &controllers.CompanyController{}, "get:Get_job_by_company_id"),
				beego.NSRouter("/post_division", &controllers.CompanyController{}, "post:Post_division"),
				beego.NSRouter("/get_division:id", &controllers.CompanyController{}, "get:Get_devision"),
				beego.NSRouter("/get_division_by_company_id:company_id", &controllers.CompanyController{}, "get:Get_devision_by_company_id"),
				beego.NSRouter("/post_allowance", &controllers.CompanyController{}, "post:Post_allowance"),
				beego.NSRouter("/get_allowance:id", &controllers.CompanyController{}, "get:Get_allowance"),
				beego.NSRouter("/get_allowance_by_company_id:company_id", &controllers.CompanyController{}, "get:Get_allowance_by_company_id"),
				beego.NSRouter("/post_task_job", &controllers.CompanyController{}, "post:Post_job_module"),
				beego.NSRouter("/get_task_job:id", &controllers.CompanyController{}, "get:Get_job_module_by_id"),

			),
			beego.NSNamespace("/order",
				beego.NSRouter("/ordering", &controllers.OrderControllers{}, "post:Ordering"),
				
			),
			beego.NSNamespace("/timelines",
				beego.NSRouter("/get_all", &controllers.TimelineController{}, "get:GetAll"),
				beego.NSRouter("/get_timeline_by_id:id", &controllers.TimelineController{}, "get:GetTimelineById"),
				beego.NSRouter("/update_timeline:id", &controllers.TimelineController{}, "put:TimelineUpdate"),
				beego.NSRouter("/post_timeline", &controllers.TimelineController{}, "post:PostTimeline"),
				beego.NSRouter("/get_timeline_by_email:email", &controllers.TimelineController{}, "get:GetTimelineByEmail"),
				beego.NSRouter("/delete_timeline:id", &controllers.TimelineController{}, "delete:DeleteTimelineById"),
				// beego.NSRouter("/repost:id", &controllers.TimelineController{}, "post:RepostTimeline"),
				beego.NSRouter("/post_comment", &controllers.TimelineController{}, "post:PostComment"),
				beego.NSRouter("/update_comment:id", &controllers.TimelineController{}, "put:UpdateComment"),
				beego.NSRouter("/delete_comment:id", &controllers.TimelineController{}, "delete:DeleteComment"),
				beego.NSRouter("/post_like_tl:id", &controllers.TimelineController{}, "post:PostLikeTL"),
				beego.NSRouter("/unlike_tl:id", &controllers.TimelineController{}, "delete:UnlikeTL"),
				beego.NSRouter("/get_like_tl:id", &controllers.TimelineController{}, "get:GetLikeTL"),
			),
			beego.NSNamespace("/connectivity",
				beego.NSRouter("/follow", &controllers.ConnectivityUserController{}, "post:FollowPost"),
				beego.NSRouter("/unfollow", &controllers.ConnectivityUserController{}, "delete:UnFollowPost"),
				beego.NSRouter("/list_following", &controllers.ConnectivityUserController{}, "get:ListFollowing"),
				beego.NSRouter("/list_followers", &controllers.ConnectivityUserController{}, "get:ListFollowers"),
				beego.NSRouter("/block_user", &controllers.ConnectivityUserController{}, "post:BlockedUser"),
				beego.NSRouter("/unblock_user", &controllers.ConnectivityUserController{}, "post:UnBlockedUser"),
			),
			// beego.NSNamespace("/friendship",
			// 	beego.NSRouter("/follow", &controllers.FriendshipController{}, "post:Follow"),
			// 	beego.NSRouter("/unfollow", &controllers.FriendshipController{}, "post:Unfollow"),
			// 	beego.NSRouter("/get_following_list:id", &controllers.FriendshipController{}, "get:GetFollowingUser"),
			// 	beego.NSRouter("/get_followers_list:id", &controllers.FriendshipController{}, "get:GetFollowersUser"),
			// 	beego.NSRouter("/block_user:id", &controllers.FriendshipController{}, "post:BlockUser"),
			// 	beego.NSRouter("/unblock_user:id", &controllers.FriendshipController{}, "post:UnblockUser"),
			// 	beego.NSRouter("/report_user:id", &controllers.FriendshipController{}, "post:RepoetUser"),
			// ),
			// beego.NSNamespace("/notification")
		beego.NSNamespace("/rockstar",
			beego.NSRouter("/login", &controllers.AdminWeb{}, "post:LoginRockStar"),
			beego.NSRouter("/create_user_admin", &controllers.AdminWeb{}, "post:CreateAdminUser"),
			beego.NSRouter("/delete_user_admin", &controllers.AdminWeb{}, "delete:DeleteAdminUser"),
			beego.NSRouter("/activation_user_admin_with_code", &controllers.AdminWeb{}, "post:ActivationAdminUserWithCode"),
			beego.NSRouter("/activate_user_admin", &controllers.AdminWeb{}, "post:ActivateAdminUser"),
			beego.NSRouter("/deactivate_user_admin", &controllers.AdminWeb{}, "post:DeactiveAdminUser"),
			beego.NSRouter("/add_product", &controllers.AdminWeb{}, "post:AddProduct"),
			// beego
		),
	)
	beego.AddNamespace(ns)
}